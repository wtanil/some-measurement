//
//  ValueEditorTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 10/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class ValueEditorTests: XCTestCase {
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var unit: RecordUnit!
    var record: Record!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        persistenceController.persist(for: viewContext)
    }
    
    override func tearDown() {
        record = nil
        unit = nil
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - setAmount(String)
    func testSetAmount_valid_quiIsUpdated() {
        // Arrange
        let valueEditor = ValueEditor(persistenceController: persistenceController, measurement: record, validator: Validator())
        // Act
        valueEditor.setAmount("100")
        // Assert
        XCTAssertEqual(100.0, valueEditor.value.value)
    }
    
    func testSetAmount_notValid_nothingHappened() {
        // Arrange
        let valueEditor = ValueEditor(persistenceController: persistenceController, measurement: record, validator: Validator())
        // Act
        valueEditor.setAmount("asd")
        // Assert
        XCTAssertEqual(0, valueEditor.value.value)
    }

    // MARK: - isValueValid: Bool
    func testIsValueValid_valid_true() {
        // Arrange
        let value = Value.make(in: viewContext, record: record, unit: unit, amount: 2727)
        persistenceController.persist(for: viewContext)
        let valueEditorCreate = ValueEditor(persistenceController: persistenceController, measurement: record, validator: Validator())
        let valueEditorUpdate = ValueEditor(persistenceController: persistenceController, measurement: record, value: value, validator: Validator())
        // Act
        let isValueValidCreate = valueEditorCreate.isValueValid
        let isValueValidUpdate = valueEditorUpdate.isValueValid
        // Assert
        XCTAssertTrue(isValueValidCreate)
        XCTAssertTrue(isValueValidUpdate)
    }
    
    // MARK: - isFormValid: Bool
    func testIsFormValid_valid_true() {
        // Arrange
        let value = Value.make(in: viewContext, record: record, unit: unit, amount: 2727)
        persistenceController.persist(for: viewContext)
        let valueEditorCreate = ValueEditor(persistenceController: persistenceController, measurement: record, validator: Validator())
        let valueEditorUpdate = ValueEditor(persistenceController: persistenceController, measurement: record, value: value, validator: Validator())
        // Act
        let isFormValidCreate = valueEditorCreate.isFormValid
        let isFormValidUpdate = valueEditorUpdate.isFormValid
        // Assert
        XCTAssertTrue(isFormValidCreate)
        XCTAssertTrue(isFormValidUpdate)
    }
}
