//
//  MeasurementHomeTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 04/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class MeasurementHomeTests: XCTestCase {
    var measurementHome: MeasurementHome!
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var record: Record!
    var unit: RecordUnit!
    var unitNone: RecordUnit!
    var value: Value!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        unitNone = RecordUnit.make(in: viewContext, isNone: true, title: "none")
        record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2)
        value = Value.make(in: viewContext, record: record, unit: unit, amount: 2727)
        persistenceController.persist(for: viewContext)
        measurementHome = MeasurementHome(persistenceController: persistenceController)
//        measurementHome.environment(\.managedObjectContext, viewContext)
    }
    
    override func tearDown() {
        measurementHome = nil
        unit = nil
        unitNone = nil
        record = nil
        value = nil
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - Helper
    private func format(_ doubleValue: Double) -> String {
        return NumberFormatter.numberFormatterDecimal.string(from: NSNumber(value: doubleValue))!
    }
    
    private func format(_ date: Date) -> String {
        return DateFormatter.dateFormatterMediumMedium.string(from: date)
    }
    
    // MARK: - shouldQuickUpdate(Record) -> Bool
    func testShouldQuickUpdate_zero_false() {
        // Arrange
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 0)
        // Act
        let shouldQuickUpdate = measurementHome.shouldQuickUpdate(for: record)
        // Assert
        XCTAssertFalse(shouldQuickUpdate)
    }
    
    func testShouldQuickUpdate_positiveNotZero_true() {
        // Arrange
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        // Act
        let shouldQuickUpdate = measurementHome.shouldQuickUpdate(for: record)
        // Assert
        XCTAssertTrue(shouldQuickUpdate)
    }
    
    func testShouldQuickUpdate_negativeNotZero_true() {
        // Arrange
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: -1)
        // Act
        let shouldQuickUpdate = measurementHome.shouldQuickUpdate(for: record)
        // Assert
        XCTAssertTrue(shouldQuickUpdate)
    }
    
    // MARK: - lastValueString(Record) -> String
    func testLastValueString_withValues_valid() {
        // Arrange
        let values = Value.populate(5, in: viewContext, record: record, unit: unit)
        record.addToValues(NSSet(array: values))
        // Act
        let expectedString: String = "4 unit"
        let lastValueString = measurementHome.lastValueString(for: record)
        // Assert
        XCTAssertEqual(expectedString, lastValueString)
    }
    
    func testLastValueString_withoutValue_none() {
        // Arrange
        let noValueRecord = Record.make(in: viewContext, unit: unit, title: "Record No Value", note: "notes", quickUpdateInterval: 2)
        // Act
        let expectedString: String = "-"
        let lastValueString = measurementHome.lastValueString(for: noValueRecord)
        // Assert
        XCTAssertEqual(expectedString, lastValueString)
    }
    
    // MARK: - lastValueDateString(Record) -> String
    func testLastValueDateString_withValue_valid() {
        // Arrange
        let values = Value.populate(5, in: viewContext, record: record, unit: unit)
        record.addToValues(NSSet(array: values))
        let lastValue = values.last!
        let lastValueDate = lastValue.date!
        // Act
        let expectedString: String = format(lastValueDate)
        let dateString = measurementHome.lastValueDateString(for: record)
        // Assert
        XCTAssertEqual(expectedString, dateString)
    }
    
    func testLastValueDateString_withoutValue_none() {
        // Arrange
        let noValueRecord = Record.make(in: viewContext, unit: unit, title: "Record No Value", note: "notes", quickUpdateInterval: 2)
        // Act
        let expectedString: String = "-"
        let dateString = measurementHome.lastValueDateString(for: noValueRecord)
        // Assert
        XCTAssertEqual(expectedString, dateString)
    }
    
    // MARK: - quickUpdateIntervalString(Record) -> String
    func testQuickUpdateIntervalString_valid_valid() {
        // Arrange
        // Act
        let quickUpdateIntervalString = measurementHome.quickUpdateIntervalString(for: record)
        let expectedString = format(2)
        // Assert
        XCTAssertEqual(expectedString, quickUpdateIntervalString)
    }
    
    // MARK: - lastValueUnitString(Record) -> String
    func testLastValueUnitString_withValue_valid() {
        // Arrange
        let values = Value.populate(5, in: viewContext, record: record, unit: unit)
        record.addToValues(NSSet(array: values))
        // Act
        let lastValueUnitString = measurementHome.lastValueUnitString(for: record)
        // Assert
        XCTAssertEqual("unit", lastValueUnitString)
    }
    
    func testLastValueUnitString_withoutValue_none() {
        // Arrange
        let emptyValuerecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2)
        // Act
        let lastValueUnitString = measurementHome.lastValueUnitString(for: emptyValuerecord)
        // Assert
        XCTAssertEqual("", lastValueUnitString)
    }
    
    // MARK: - createValue(Record, Double, RecordUnit) -> Value
    func testCreateValue_valid_newValue() {
        // Arrange
        // Act
        let newValue = measurementHome.createValue(measurement: record, amount: 2727, unit: unit)
        let expectedValue = record.lastValue
        // Assert
        XCTAssertEqual(expectedValue, newValue)
        XCTAssertEqual(2727, newValue.value)
        XCTAssertEqual(unit, newValue.unit!)
    }
    
    // MARK: - contextMenuButtonAction(Record, ContextMenuMode)
    func testContextMenuButtonAction_withValue_newValue() {
        // Arrange
        // Act
        measurementHome.contextMenuButtonAction(for: record, mode: .decrement)
        let decrementValue = record.lastValue!
        measurementHome.contextMenuButtonAction(for: record, mode: .increment)
        let incrementValue = record.lastValue!
        // Assert
        XCTAssertEqual(2725, decrementValue.value)
        XCTAssertEqual(2727, incrementValue.value)
        XCTAssertEqual(unit, decrementValue.unit!)
        XCTAssertEqual(unit, incrementValue.unit!)
    }
    
    func testContextMenuButtonAction_withoutValue_newValue() {
        // Arrange
        let decrementRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        let incrementRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        // Act
        measurementHome.contextMenuButtonAction(for: decrementRecord, mode: .decrement)
        let decrementValue = decrementRecord.lastValue!
        measurementHome.contextMenuButtonAction(for: incrementRecord, mode: .increment)
        let incrementValue = incrementRecord.lastValue!
        // Assert
        XCTAssertEqual(-1, decrementValue.value)
        XCTAssertEqual(1, incrementValue.value)
        XCTAssertEqual(unit, decrementValue.unit!)
        XCTAssertEqual(unit, incrementValue.unit!)
    }
}
