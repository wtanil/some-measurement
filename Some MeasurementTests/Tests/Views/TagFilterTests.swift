//
//  TagFilterTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 14/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData
import SwiftUI

class TagFilterTests: XCTestCase {

    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!

    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
    }
    
    override func tearDown() {
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
        
    }
    
    // MARK: - Helper
    
    //    var bindingValue = false
    //    let binding = Binding(get: { bindingValue }, set: { bindingValue = $0 })
    
    // MARK: - updateSelectedTags(Tag)
    func testUpdateSelectedTags_newTagExists_removed() {
        // Arrange
        var tags = Tag.populate(5, in: viewContext)
        let tagsBinding = Binding(get: { tags }, set: { tags = $0 })
        let tagFilter = TagFilter(selectedTags: tagsBinding)
        let newTag = tags.first!
        // Act
        tagFilter.updateSelectedTags(with: newTag)
        // Assert
        XCTAssertEqual(4, tags.count)
        XCTAssertFalse(tags.contains(newTag))
    }
    
    func testUpdateSelectedTags_newTagDoesntExist_appended() {
        // Arrange
        var tags = Tag.populate(5, in: viewContext)
        let tagsBinding = Binding(get: { tags }, set: { tags = $0 })
        let tagFilter = TagFilter(selectedTags: tagsBinding)
        let newTag = Tag.make(in: viewContext, name: "newtag")
        // Act
        tagFilter.updateSelectedTags(with: newTag)
        // Assert
        XCTAssertEqual(6, tags.count)
        XCTAssertEqual("newtag", tags.first!.name!)
    }
}
