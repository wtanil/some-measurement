//
//  TagEditorTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 11/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class TagEditorTests: XCTestCase {
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var unit: RecordUnit!
    var record: Record!
    var tag: Tag!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        tag = Tag.make(in: viewContext, name: "tag")
        persistenceController.persist(for: viewContext)
    }
    
    override func tearDown() {
        record = nil
        unit = nil
        tag = nil
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - shouldSave: Bool
    func testShouldSave_createValid_true() {
        // Arrange
        let tagEditor = TagEditor(persistenceController: persistenceController)
        tagEditor.tag.name = "new tag"
        // Act
        let shouldSave = tagEditor.shouldSave
        // Assert
        XCTAssertTrue(shouldSave)
    }
    
    func testShouldSave_createNotValid_false() {
        // Arrange
        let tagEditor = TagEditor(persistenceController: persistenceController)
        tagEditor.tag.name = "tag"
        // Act
        let shouldSave = tagEditor.shouldSave
        // Assert
        XCTAssertFalse(shouldSave)
        
    }
    
    func testShouldSave_updateValid_true() {
        // Arrange
        let tagEditor = TagEditor(persistenceController: persistenceController, tag: tag)
        tagEditor.tag.name = "new tag"
        // Act
        let shouldSave = tagEditor.shouldSave
        // Assert
        XCTAssertTrue(shouldSave)
    }
    
    func testShouldSave_updateNotValid_false() {
        // Arrange
        let newTag = Tag.make(in: viewContext, name: "new tag")
        persistenceController.persist(newTag)
        let tagEditor = TagEditor(persistenceController: persistenceController, tag: tag)
        tagEditor.tag.name = "new tag"
        // Act
        let shouldSave = tagEditor.shouldSave
        // Assert
        XCTAssertFalse(shouldSave)
    }
    
    
}
