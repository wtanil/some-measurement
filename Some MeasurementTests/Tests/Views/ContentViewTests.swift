//
//  ContentViewTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 04/03/22.
//

import XCTest
@testable import Some_Measurement

class ContentViewTests: XCTestCase {
    var contentView: ContentView!

    override func setUp() {
        contentView = ContentView()
        contentView.environment(\.managedObjectContext, PersistenceController.shared.container.viewContext)
    }
    
    override func tearDown() {
        contentView = nil
        PersistenceController.shared.resetStore()
    }
    
    // MARK: - setIsNotFirstTime(Bool)
    func testSetIsNotFirstTime_true_true() {
        // Arrange
        // Act
        contentView.setIsNotFirstTime(as: true)
        // Assert
        XCTAssertTrue(contentView.isNotFirstTime)
    }
    
    func testSetIsNotFirstTime_false_false() {
        // Arrange
        // Act
        contentView.setIsNotFirstTime(as: false)
        // Assert
        XCTAssertFalse(contentView.isNotFirstTime)
    }
}
