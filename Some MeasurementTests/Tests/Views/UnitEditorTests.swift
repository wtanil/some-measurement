//
//  UnitEditorTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 14/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class UnitEditorTests: XCTestCase {
    
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var unit: RecordUnit!

    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        persistenceController.persist(unit)
    }

    override func tearDown() {
        unit = nil
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - shouldSave: Bool
    func testShouldSave_createValid_true() {
        // Arrange
        let unitEditor = UnitEditor(persistenceController: persistenceController)
        unitEditor.unit.title = "newunit"
        // Act
        let shouldSave = unitEditor.shouldSave
        // Assert
        XCTAssertTrue(shouldSave)
    }
    
    func testShouldSave_createNotValid_false() {
        // Arrange
        let unitEditor = UnitEditor(persistenceController: persistenceController)
        unitEditor.unit.title = "unit"
        // Act
        let shouldSave = unitEditor.shouldSave
        // Assert
        XCTAssertFalse(shouldSave)
        
    }
    
    func testShouldSave_updateValid_true() {
        // Arrange
        let unitEditor = UnitEditor(persistenceController: persistenceController, unit: unit)
        unitEditor.unit.title = "newtag"
        // Act
        let shouldSave = unitEditor.shouldSave
        // Assert
        XCTAssertTrue(shouldSave)
    }
    
    func testShouldSave_updateNotValid_false() {
        // Arrange
        let newTag = RecordUnit.make(in: viewContext, isNone: false, title: "newtag")
        persistenceController.persist(newTag)
        let unitEditor = UnitEditor(persistenceController: persistenceController, unit: unit)
        unitEditor.unit.title = "newtag"
        // Act
        let shouldSave = unitEditor.shouldSave
        // Assert
        XCTAssertFalse(shouldSave)
    }
    
    // MARK: - replaceUnit
    
    // MARK: - deleteUnit


}
