//
//  MeasurementDetailTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 09/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class MeasurementDetailTests: XCTestCase {

    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var record: Record!
    var unit: RecordUnit!
    var measurementDetail: MeasurementDetail!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2)
        persistenceController.persist(for: viewContext)
        measurementDetail = MeasurementDetail(persistenceController: persistenceController, measurement: record)
    }
    
    override func tearDown() {
        measurementDetail = nil
        record = nil
        unit = nil
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - Helper
    private func format(_ doubleValue: Double) -> String {
        return NumberFormatter.numberFormatterDecimal.string(from: NSNumber(value: doubleValue))!
    }
    
    // MARK: - lastValue: String
    func testLastValue_valid_valid() {
        // Arrange
        // Act
        let lastValueString = measurementDetail.lastValue
        // Assert
        XCTAssertEqual("None", lastValueString)
        
    }
    
    func testLastValue_withoutLastValue_nonLabel() {
        // Arrange
        let _ = Value.make(in: viewContext, record: record, unit: unit, amount: 2727)
        // Act
        let lastValueString = measurementDetail.lastValue
        let expectedLastValueString = "\(format(2727)) unit"
        // Assert
        XCTAssertEqual(expectedLastValueString, lastValueString)
    }
    
    // MARK: - deleteMeasurement
    
}
