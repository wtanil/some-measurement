//
//  MeasurementEditorTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 07/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class MeasurementEditorTests: XCTestCase {
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var unit: RecordUnit!
    var unitNone: RecordUnit!
    var record: Record!

    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        unitNone = RecordUnit.make(in: viewContext, isNone: true, title: "none")
        record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2)
        persistenceController.persist(for: viewContext)
    }
    
    override func tearDown() {
        unit = nil
        unitNone = nil
        record = nil
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - Helper
    func makeMeasurementEditor() -> MeasurementEditor {
        return MeasurementEditor(persistenceController: persistenceController, validator: Validator())
    }
    
    func makeMeasurementEditor(record: Record) -> MeasurementEditor {
        return MeasurementEditor(persistenceController: persistenceController, measurement: record, validator: Validator())
    }
    
    // MARK: - setQUI(String)
    func testSetQUI_valid_quiIsUpdated() {
        // Arrange
        let measurementEditor = makeMeasurementEditor()
        // Act
        measurementEditor.setQUI("100")
        // Assert
        XCTAssertEqual(100.0, measurementEditor.measurement.quickUpdateInterval)
    }
    
    func testSetQUI_notValid_nothingHappened() {
        // Arrange
        let measurementEditor = makeMeasurementEditor()
        // Act
        measurementEditor.setQUI("asd")
        // Assert
        XCTAssertEqual(0, measurementEditor.measurement.quickUpdateInterval)
    }

    // MARK: - isQuickUpdateIntervalValid: Bool
    func testIsQUIValid_valid_true() {
        // Arrange
        record.quickUpdateInterval = 1
        let measurementEditor = makeMeasurementEditor()
        measurementEditor.measurement.quickUpdateInterval = 1
        // Act
        let isQUIValid = measurementEditor.isQuickUpdateIntervalValid
        // Assert
        XCTAssertTrue(isQUIValid)
    }
    
    // MARK: - isFormValid: Bool
    func testIsFormValid_valid_true() {
        // Arrange
        let measurementEditorWithRecord = makeMeasurementEditor(record: record)
        // Act
        let isFormValid = measurementEditorWithRecord.isFormValid
        // Assert
        XCTAssertTrue(isFormValid)
    }
    
    func testIsFormValid_notValid_false() {
        // Arrange
        let measurementEditor = makeMeasurementEditor()
        // Act
        let isFormValid = measurementEditor.isFormValid
        // Assert
        XCTAssertFalse(isFormValid)
    }
    
    // MARK: - shouldSave: Bool
    func testShouldSave_create_true() {
        // Arrange
        let measurementEditor = makeMeasurementEditor()
        measurementEditor.measurement.title = "New Record"
        // Act
        let shouldSave = measurementEditor.shouldSave
        // Assert
        XCTAssertTrue(shouldSave)
    }
    
    func testShouldSave_createNotValid_false() {
        // Arrange
        let measurementEditor = makeMeasurementEditor()
        measurementEditor.measurement.title = "Record"
        // Act
        let shouldSave = measurementEditor.shouldSave
        // Assert
        XCTAssertFalse(shouldSave)
    }
    
    func testShouldSave_edit_true() {
        // Arrange
        let measurementEditor = makeMeasurementEditor(record: record)
        measurementEditor.measurement.title = "ASD"
        // Act
        let shouldSave = measurementEditor.shouldSave
        // Assert
        XCTAssertTrue(shouldSave)
    }
    
    func testShouldSave_editSameTitle_true() {
        // Arrange
        let measurementEditor = makeMeasurementEditor(record: record)
        measurementEditor.measurement.quickUpdateInterval = 10
        // Act
        let shouldSave = measurementEditor.shouldSave
        // Assert
        XCTAssertTrue(shouldSave)
    }
    
    func testShouldSave_editNotValid_false() {
        // Arrange
        let newRecord = Record.make(in: viewContext, unit: unit, title: "New Record", note: "notes", quickUpdateInterval: 1)
        persistenceController.persist(newRecord)
        let measurementEditor = makeMeasurementEditor(record: record)
        measurementEditor.measurement.title = "New Record"
        // Act
        let shouldSave = measurementEditor.shouldSave
        // Assert
        XCTAssertFalse(shouldSave)
    }
    
    // MARK: - save
}
