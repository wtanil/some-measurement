//
//  ValueInlineEditorTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 10/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class ValueInlineEditorTests: XCTestCase {

    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var valueInlineEditor: ValueInlineEditor!
    var unit: RecordUnit!
    var record: Record!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        valueInlineEditor = ValueInlineEditor(persistenceController: persistenceController, measurement: record, defaultUnit: unit, validator: Validator())
    }
    
    override func tearDown() {
        valueInlineEditor = nil
        record = nil
        unit = nil
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - selectedUnitLabel: String
    func testSelectedUnitLabel_valid_unit() {
        // Arrange
        // Act
        let selectedUnitLabel = valueInlineEditor.selectedUnitLabel
        // Assert
        XCTAssertEqual("unit", selectedUnitLabel)
    }
    
    // MARK: - createValue(Record, Double, RecordUnit) -> Value
    func testCreateValue() {
        // Arrange
        
        // Act
        let newValue = valueInlineEditor.createValue(measurement: record, amount: 2727, unit: unit)
        let expectedValue = record.lastValue
        // Assert
        XCTAssertEqual(expectedValue, newValue)
        XCTAssertEqual(2727, newValue.value)
        XCTAssertEqual(unit, newValue.unit!)
    }

}
