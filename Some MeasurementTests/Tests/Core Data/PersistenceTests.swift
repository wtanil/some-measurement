//
//  PersistenceTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 02/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class PersistenceTests: XCTestCase {
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
    }
    
    override func tearDown() {
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - hasUnitNone(NSManagedObjectContext) -> Bool
    func testHasUnitNone_exists_true() {
        // Arrange
        let _ = RecordUnit.createUnitNone(in: viewContext)
        persistenceController.persist(for: viewContext)
        // Act
        let hasUnitNone = persistenceController.hasUnitNone(in: viewContext)
        // Assert
        XCTAssertTrue(hasUnitNone)
    }
    
    func testHasUnitNone_noUnitNone_false() {
        // Arrange
        // Act
        let hasUnitNone = persistenceController.hasUnitNone(in: viewContext)
        // Assert
        XCTAssertFalse(hasUnitNone)
    }
    
}
