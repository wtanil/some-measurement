//
//  ValueTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 25/02/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class ValueTests: XCTestCase {

    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var record: Record!
    var unit: RecordUnit!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        record = Record.make(in: viewContext, unit: unit, title: "Record", note: "Test Description", quickUpdateInterval: 2727)
    }
    
    override func tearDown() {
        viewContext = nil
        record = nil
        unit = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - Helper
    private func format(_ doubleValue: Double) -> String {
        return NumberFormatter.numberFormatterDecimal.string(from: NSNumber(value: doubleValue))!
    }
    
    private func format(_ date: Date) -> String {
        return DateFormatter.dateFormatterMediumMedium.string(from: date)
    }
    
    // MARK: - displayValue: String
    func testDisplayValue_valid_valid() {
        // Arrange
        let positiveNumber: Double = 2727
        let numberZero: Double = 0
        let negativeNumber: Double = -2727
        let positiveValue = Value.make(in: viewContext, record: record, unit: unit, amount: positiveNumber)
        let zeroValue = Value.make(in: viewContext, record: record, unit: unit, amount: numberZero)
        let negativeValue = Value.make(in: viewContext, record: record, unit: unit, amount: negativeNumber)
        let expectedPositiveString = format(positiveNumber)
        let expectedZeroString = format(numberZero)
        let expectedNegativeString = format(negativeNumber)
        // Act
        let positiveDisplayValue = positiveValue.displayValue
        let zeroDisplayValue = zeroValue.displayValue
        let negativeDisplayValue = negativeValue.displayValue
        // Assert
        XCTAssertEqual(expectedPositiveString, positiveDisplayValue, "Failed test on positive number")
        XCTAssertEqual(expectedZeroString, zeroDisplayValue, "Failed test on 0")
        XCTAssertEqual(expectedNegativeString, negativeDisplayValue, "Failed test on negative number")
    }
    
    // MARK: - displayValueWithUnit: String
    func testDisplayValueWithUnit_valid_valid() {
        // Arrange
        let value = Value.make(in: viewContext, record: record, unit: unit, amount: 2727)
        // Act
        let displayValueWithUnit = value.displayValueWithUnit
        let expectedValueWithUnit = "\(format(value.value)) \(value.unit!.displayTitle)"
        // Assert
        XCTAssertEqual(expectedValueWithUnit, displayValueWithUnit)
    }
    
    func testDisplayValueWithUnit_unitNil_withoutUnit() {
        // Arrange
        let value = Value.make(in: viewContext, record: record, unit: nil, amount: 2727)
        // Act
        let displayValueWithUnit = value.displayValueWithUnit
        let expectedValueWithUnit = "\(format(value.value))"
        // Assert
        XCTAssertEqual(expectedValueWithUnit, displayValueWithUnit)
    }
    
    // MARK: - displayDate: String
    func testDisplayDate_valid_valid() {
        // Arrange
        let date = Date()
        let value = Value.make(in: viewContext, record: record, unit: unit, amount: 2727)
        value.date = date
        let createDate = value.createDate!
        value.updateDate = date
        // Act
        let expectedDisplayDate = format(date)
        let displayDate = value.displayDate
        let expectedDisplayUpdateDate = format(date)
        let displayUpdateDate = value.displayUpdateDate
        let expectedDisplayCreateDate = format(createDate)
        let displayCreateDate = value.displayCreateDate
        // Assert
        XCTAssertEqual(expectedDisplayDate, displayDate)
        XCTAssertEqual(expectedDisplayUpdateDate, displayUpdateDate)
        XCTAssertEqual(expectedDisplayCreateDate, displayCreateDate)
    }
    
    func testDisplayDate_nilDate_unknownDate() {
        // Arrange
        let date: Date? = nil
        let value = Value.make(in: viewContext, record: record, unit: unit, amount: 2727)
        value.date = date
        value.createDate = date
        value.updateDate = date
        // Act
        let expectedDisplayDate = "Unknown date"
        let displayDate = value.displayDate
        let displayUpdateDate = value.displayUpdateDate
        let displayCreateDate = value.displayCreateDate
        // Assert
        XCTAssertEqual(expectedDisplayDate, displayDate)
        XCTAssertEqual(expectedDisplayDate, displayUpdateDate)
        XCTAssertEqual(expectedDisplayDate, displayCreateDate)
    }
    
    // MARK: - replaceAllUnits(RecordUnit, RecordUnit, NSManagedObjectContext) -> Int
    func testReplaceAllUnits_allReplaced_valid() {
        // Arrange
        let values = Value.populate(5, in: viewContext, record: record, unit: unit)
        let newUnit = RecordUnit.make(in: viewContext, isNone: false, title: "none")
        let oldUnit: RecordUnit = (values.first?.unit)!
        persistenceController.persist(for: viewContext)
        // Act
        let counts = Value.replaceAllUnits(from: oldUnit, to: newUnit, in: viewContext)
        persistenceController.persist(for: viewContext)
        // Assert
        XCTAssertEqual(5, counts)
        for value in values {
            XCTAssertIdentical(newUnit, value.unit!)
        }
    }
    
    func testReplaceAllUnits_someReplaced_valid() {
        // Arrange
        let values = Value.populate(5, in: viewContext, record: record, unit: unit)
        let newUnit = RecordUnit.make(in: viewContext, isNone: false, title: "none")
        let oldUnit: RecordUnit = (values.first?.unit)!
        let anotherUnit = RecordUnit.make(in: viewContext, isNone: false, title: "another")
        values[0].unit = anotherUnit
        values[1].unit = anotherUnit
        persistenceController.persist(for: viewContext)
        // Act
        let counts = Value.replaceAllUnits(from: oldUnit, to: newUnit, in: viewContext)
        persistenceController.persist(for: viewContext)
        // Assert
        XCTAssertEqual(3, counts)
        XCTAssertIdentical(anotherUnit, values[0].unit!)
        XCTAssertIdentical(anotherUnit, values[1].unit!)
        XCTAssertIdentical(newUnit, values[2].unit!)
        XCTAssertIdentical(newUnit, values[3].unit!)
        XCTAssertIdentical(newUnit, values[4].unit!)
    }
    
    func testReplaceAllUnits_noneReplaced_valid() {
        // Arrange
        let _ = Value.populate(5, in: viewContext, record: record, unit: unit)
        let newUnit = RecordUnit.make(in: viewContext, isNone: false, title: "none")
        let anotherUnit = RecordUnit.make(in: viewContext, isNone: false, title: "another")
        persistenceController.persist(for: viewContext)
        // Act
        let counts = Value.replaceAllUnits(from: anotherUnit, to: newUnit, in: viewContext)
        persistenceController.persist(for: viewContext)
        // Assert
        XCTAssertEqual(0, counts)
    }
    
}
