//
//  RecordUnitTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 01/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class RecordUnitTests: XCTestCase {
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
    }
    
    override func tearDown() {
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - Helper

    // MARK: - displayTitle: String
    func testDisplayTitle_value_value() {
        // Arrange
        let validUnit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        let nilUnit = RecordUnit.make(in: viewContext, isNone: false, title: nil)
        let noneUnit = RecordUnit.make(in: viewContext, isNone: true, title: "none")
        // Act
        let displayTitle = validUnit.displayTitle
        let nilDisplayTitle = nilUnit.displayTitle
        let noneDisplayTitle = noneUnit.displayTitle
        // Assert
        XCTAssertEqual("unit", displayTitle)
        XCTAssertEqual("Unknown unit", nilDisplayTitle)
        XCTAssertEqual("", noneDisplayTitle)
    }
    
    // MARK: - displayListTitle: String
    func testDisplayListTitle_value_value() {
        // Arrange
        let validUnit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        let nilUnit = RecordUnit.make(in: viewContext, isNone: false, title: nil)
        let noneUnit = RecordUnit.make(in: viewContext, isNone: true, title: "none")
        // Act
        let displayListTitle = validUnit.displayListTitle
        let nilDisplayListTitle = nilUnit.displayListTitle
        let noneDisplayListTitle = noneUnit.displayListTitle
        // Assert
        XCTAssertEqual("unit", displayListTitle)
        XCTAssertEqual("Unknown unit", nilDisplayListTitle)
        XCTAssertEqual("none", noneDisplayListTitle)
    }

    // MARK: - isTitleValid: Bool
    func testIsTitleValid_valid_true() {
        // Arrange
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        // Act
        let isTitleValid = unit.isTitleValid
        // Assert
        XCTAssertTrue(isTitleValid)
    }
    
    func testIsTitleValid_empty_false() {
        // Arrange
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "")
        // Act
        let isTitleValid = unit.isTitleValid
        // Assert
        XCTAssertFalse(isTitleValid)
    }
    
    func testIsTitleValid_nil_false() {
        // Arrange
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: nil)
        // Act
        let isTitleValid = unit.isTitleValid
        // Assert
        XCTAssertFalse(isTitleValid)
    }
    
    // MARK: - isValid(NSManagedObjectContext) -> Bool
    func testIsValid_valid_true() {
        // Arrange
        let _ = Tag.populate(5, in: viewContext)
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
        persistenceController.persist(for: viewContext)
        // Act
        let isValid = unit.isValid(in: viewContext)
        // Assert
        XCTAssertTrue(isValid)
    }
    
    func testIsValid_titleIsEmpty_false() {
        // Arrange
        let _ = Tag.populate(5, in: viewContext)
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "")
        persistenceController.persist(for: viewContext)
        // Act
        let isValid = unit.isValid(in: viewContext)
        // Assert
        XCTAssertFalse(isValid)
    }
    
    func testIsValid_notUnique_false() {
        // Arrange
        let units = RecordUnit.populate(5, in: viewContext)
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: units.first!.title)
        persistenceController.persist(for: viewContext)
        // Act
        let isValid = unit.isValid(in: viewContext)
        // Assert
        XCTAssertFalse(isValid)
    }
    
    // MARK: - isUnique(NSManagedObjectContext) -> Bool
    func testIsUnique_valid_true() {
        // Arrange
        let _ = Tag.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "unitnew")
        // Act
        let isUnique = unit.isUnique(in: viewContext)
        // Assert
        XCTAssertTrue(isUnique)
    }
    
    func testIsUnique_notUnique_false() {
        // Arrange
        let units = RecordUnit.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: units.first!.title)
        // Act
        let isUnique = unit.isUnique(in: viewContext)
        // Assert
        XCTAssertFalse(isUnique)
    }
    
    func testIsUnique_caseSensitive_true() {
        // Arrange
        let units = RecordUnit.populate(5, in: viewContext)
        units.first!.title = "UnitName"
        persistenceController.persist(for: viewContext)
        let unit = RecordUnit.make(in: viewContext, isNone: false, title: "unitname")
        // Act
        let isUnique = unit.isUnique(in: viewContext)
        // Assert
        XCTAssertTrue(isUnique)
    }
    
    // MARK: - createUnitNone(NSManagedObjectContext) -> RecordUnit & getUnitNone(NSManagedObjectContext) -> RecordUnit {
    func testUnitNone_valid_valid() {
        // Arrange
        // Act
        let createdUnitNone = RecordUnit.createUnitNone(in: viewContext)
        let fetchedUnitNone = RecordUnit.getUnitNone(in: viewContext)
        // Assert
        XCTAssertIdentical(createdUnitNone, fetchedUnitNone)
        
    }
}
