//
//  RecordTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 01/03/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class RecordTests: XCTestCase {
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    var unit: RecordUnit!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
        unit = RecordUnit.make(in: viewContext, isNone: false, title: "unit")
    }
    
    override func tearDown() {
        viewContext = nil
        unit = nil
        persistenceController.resetStore()
        persistenceController = nil
    }
    
    // MARK: - Helper
    private func format(_ doubleValue: Double) -> String {
        return NumberFormatter.numberFormatterDecimal.string(from: NSNumber(value: doubleValue))!
    }
    
    private func format(_ date: Date) -> String {
        return DateFormatter.dateFormatterMediumMedium.string(from: date)
    }

    // MARK: - displayTitle: String
    func testDisplayTitle_valid_valid() {
        // Arrange
        let validRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        let nilRecord = Record.make(in: viewContext, unit: unit, title: nil, note: "notes", quickUpdateInterval: 1)
        // Act
        let displayTitle = validRecord.displayTitle
        let nilDisplayTitle = nilRecord.displayTitle
        // Assert
        XCTAssertEqual("Record", displayTitle)
        XCTAssertEqual("Unknown title", nilDisplayTitle)
    }
    
    // MARK: - displayNote: String
    func testDisplayNote_valid_valid() {
        // Arrange
        let validRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 1)
        let nilRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: nil, quickUpdateInterval: 1)
        // Act
        let displayNote = validRecord.displayNote
        let nilDisplayNote = nilRecord.displayNote
        // Assert
        XCTAssertEqual("notes", displayNote)
        XCTAssertEqual("Unknown note", nilDisplayNote)
    }
    
    // MARK: - displayQuickUpdateInterval: String
    func testDisplayQuickUpdateInterval_valid_valid() {
        // Arrange
        let positiveNumber: Double = 2727
        let numberZero: Double = 0
        let negativeNumber: Double = -2727
        let positiveRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: positiveNumber)
        let zeroRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: numberZero)
        let negativeRecord = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: negativeNumber)
        let expectedPositiveString = format(positiveNumber)
        let expectedZeroString = format(numberZero)
        let expectedNegativeString = format(negativeNumber)
        // Act
        let positiveDisplayQUI = positiveRecord.displayQuickUpdateInterval
        let zeroDisplayQUI = zeroRecord.displayQuickUpdateInterval
        let negativeDisplayQUI = negativeRecord.displayQuickUpdateInterval
        // Assert
        XCTAssertEqual(expectedPositiveString, positiveDisplayQUI, "Failed test on positive number")
        XCTAssertEqual(expectedZeroString, zeroDisplayQUI, "Failed test on 0")
        XCTAssertEqual(expectedNegativeString, negativeDisplayQUI, "Failed test on negative number")
    }
    
    // MARK: - displayDate: String
    func testDisplayDate_valid_valid() {
        // Arrange
        let date = Date()
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        let createDate = record.createDate!
        record.updateDate = date
        // Act
        let expectedDisplayCreateDate = format(createDate)
        let displayCreateDate = record.displayCreateDate
        let expectedDisplayUpdateDate = format(date)
        let displayUpdateDate = record.displayUpdateDate
        // Assert
        XCTAssertEqual(expectedDisplayUpdateDate, displayUpdateDate)
        XCTAssertEqual(expectedDisplayCreateDate, displayCreateDate)
    }
    
    func testDisplayDate_nilDate_unknownDate() {
        // Arrange
        let date: Date? = nil
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        record.createDate = date
        record.updateDate = date
        // Act
        let expectedDisplayDate = "Unknown date"
        let displayCreateDate = record.displayCreateDate
        let displayUpdateDate = record.displayUpdateDate
        // Assert
        XCTAssertEqual(expectedDisplayDate, displayUpdateDate)
        XCTAssertEqual(expectedDisplayDate, displayCreateDate)
    }
    
    // MARK: - displayTags: String
    func testDisplayTags_valid_valid() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        let tags = Tag.populate(5, in: viewContext)
        record.tags = NSSet(array: tags)
        // Act
        let expectedDisplayTags = "tag0, tag1, tag2, tag3, tag4"
        let displayTags = record.displayTags
        // Assert
        XCTAssertEqual(expectedDisplayTags, displayTags)
    }
    
    func testDisplayTags_empty_noTags() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        record.tags = NSSet(array: [Tag]())
        // Act
        let displayTags = record.displayTags
        // Assert
        XCTAssertEqual("No tags", displayTags)
    }
    
    // nil child will always return empty set
    func testDisplayTags_nil_unknownTags() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        record.tags = nil
        // Act
        let displayTags = record.displayTags
        // Assert
        XCTAssertEqual("No tags", displayTags)
    }
    
    // MARK: - tagAsSet: Set<Tag>
    func testTagAsSet_valid_valid() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        let tags = Tag.populate(5, in: viewContext)
        record.tags = NSSet(array: tags)
        // Act
        let tagAsSet = record.tagsAsSet
        let expectedTagAsSet = Set(tags)
        // Arrange
        XCTAssertTrue(tagAsSet == expectedTagAsSet)
    }
    
    // MARK: - tagAsArray: [Tag]
    func testTagAsArray_valid_valid() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        let tags = Tag.populate(5, in: viewContext)
        record.tags = NSSet(array: tags)
        // Act
        let tagAsArray = record.tagsAsArray.sorted {$0.name! < $1.name!}
        let expectedTagAsArray = tags.sorted {$0.name! < $1.name!}
        // Arrange
        XCTAssertEqual(5, tagAsArray.count)
        XCTAssertEqual(expectedTagAsArray, tagAsArray)
    }
    
    // MARK: - valueAsArray: [Tag]
    func testValueAsArray_valid_valid() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "test note", quickUpdateInterval: 2727)
        let values = Value.populate(5, in: viewContext, record: record, unit: unit)
        // Act
        let valueAsArray = record.valuesAsArray.sorted {$0.date! < $1.date!}
        let expectedValueAsArray = values.sorted {$0.date! < $1.date!}
        // Assert
        XCTAssertEqual(5, valueAsArray.count)
        XCTAssertEqual(expectedValueAsArray, valueAsArray)
    }
    
    // MARK: - lastValue: Value?
    func testLastValue_valid_lastValue() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        let values = Value.populate(5, in: viewContext, record: record, unit: unit)
        // Act
        let expectedLastValue = values.last
        let lastValue = record.lastValue
        // Assert
        XCTAssertIdentical(expectedLastValue, lastValue)
    }
    
    func testLastValue_noValue_nil() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        // Act
        let lastValue = record.lastValue
        // Assert
        XCTAssertNil(lastValue)
    }
    
    // MARK: - computedTags: [Tag]
    func testComputedTags_valid_valid() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        var tags = Tag.populate(5, in: viewContext)
        record.tags = NSSet(array: tags)
        // Act
        var computedTags = record.computedTags.sorted { $0.name! < $1.name!}
        var expectedComputedTags = tags.sorted { $0.name! < $1.name!}
        // Assert
        XCTAssertEqual(expectedComputedTags, computedTags)
        // Act
        tags.removeFirst()
        record.computedTags = tags
        computedTags = record.computedTags.sorted { $0.name! < $1.name!}
        expectedComputedTags = tags.sorted { $0.name! < $1.name!}
        // Assert
        XCTAssertEqual(expectedComputedTags, computedTags)
    }
    
    // MARK: - isTitleValid: Bool
    func testIsTitleValid_valid_true() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        // Act
        let isTitleValid = record.isTitleValid
        // Assert
        XCTAssertTrue(isTitleValid)
    }
    
    func testIsTitleValid_empty_false() {
        // Arrange
        let record = Record.make(in: viewContext, unit: unit, title: "", note: "notes", quickUpdateInterval: 2727)
        // Act
        let isTitleValid = record.isTitleValid
        // Assert
        XCTAssertFalse(isTitleValid)
    }
    
    // MARK: - isValid(NSManagedObjectContext) -> Bool
    
    func testIsValid_valid_true() {
        // Arrange
        let _ = Record.populate(5, in: viewContext, unit: unit)
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        // Act
        let isValid = record.isValid(in: viewContext)
        // Assert
        XCTAssertTrue(isValid)
    }
    
    func testIsValid_notUnique_false() {
        // Arrange
        let records = Record.populate(5, in: viewContext, unit: unit)
        let recordNotUniquetitle = records.first!.title
        let record = Record.make(in: viewContext, unit: unit, title: recordNotUniquetitle, note: "notes", quickUpdateInterval: 2727)
        // Act
        let isValid = record.isValid(in: viewContext)
        // Assert
        XCTAssertFalse(isValid)
    }
    
    func testIsValid_emptyTitle_false() {
        // Arrange
        let _ = Record.populate(5, in: viewContext, unit: unit)
        let record = Record.make(in: viewContext, unit: unit, title: "", note: "notes", quickUpdateInterval: 2727)
        // Act
        let isValid = record.isValid(in: viewContext)
        // Assert
        XCTAssertFalse(isValid)
    }
    
    // MARK: - isUnique(NSManagedObjectContext) -> Bool
    func testIsUnique_valid_true() {
        // Arrange
        let _ = Record.populate(5, in: viewContext, unit: unit)
        let record = Record.make(in: viewContext, unit: unit, title: "Record", note: "notes", quickUpdateInterval: 2727)
        // Act
        let isUnique = record.isUnique(in: viewContext)
        // Assert
        XCTAssertTrue(isUnique)
    }
    
    func testIsUnique_notUnique_false() {
        // Arrange
        let records = Record.populate(5, in: viewContext, unit: unit)
        let recordNotUniquetitle = records.first!.title
        let record = Record.make(in: viewContext, unit: unit, title: recordNotUniquetitle, note: "notes", quickUpdateInterval: 2727)
        // Act
        let isUnique = record.isUnique(in: viewContext)
        // Assert
        XCTAssertFalse(isUnique)
    }
    
    func testIsUnique_notUniqueCaseInsensitive_false() {
        // Arrange
        let records = Record.populate(5, in: viewContext, unit: unit)
        records.first!.title = "CaseInsensitiveName Record"
        let record = Record.make(in: viewContext, unit: unit, title: "caseinsensitivename record", note: "notes", quickUpdateInterval: 2727)
        // Act
        let isUnique = record.isUnique(in: viewContext)
        // Assert
        XCTAssertFalse(isUnique)
    }
    
    // MARK: - replaceAllDefaultUnits(RecordUnit, RecordUnit, NSManagedObjectContext) -> Int
    func testReplaceAllDefaultUnits_allReplaced_valid() {
        // Arrange
        let records = Record.populate(5, in: viewContext, unit: unit)
        let newUnit = RecordUnit.make(in: viewContext, isNone: false, title: "new")
        persistenceController.persist(for: viewContext)
        let oldUnit: RecordUnit = unit
        // Act
        let counts = Record.replaceAllDefaultUnits(from: oldUnit, to: newUnit, in: viewContext)
        persistenceController.persist(for: viewContext)
        // Assert
        XCTAssertEqual(5, counts)
        for record in records {
            XCTAssertIdentical(newUnit, record.defaultUnit!)
        }
    }
    
    func testReplaceAllDefaultUnits_someReplaced_valid() {
        // Arrange
        let records = Record.populate(5, in: viewContext, unit: unit)
        let newUnit = RecordUnit.make(in: viewContext, isNone: false, title: "new")
        let anotherUnit = RecordUnit.make(in: viewContext, isNone: false, title: "another")
        records[0].defaultUnit = anotherUnit
        records[1].defaultUnit = anotherUnit
        persistenceController.persist(for: viewContext)
        let oldUnit: RecordUnit = unit
        // Act
        let counts = Record.replaceAllDefaultUnits(from: oldUnit, to: newUnit, in: viewContext)
        persistenceController.persist(for: viewContext)
        // Assert
        XCTAssertEqual(3, counts)
        XCTAssertIdentical(anotherUnit, records[0].defaultUnit!)
        XCTAssertIdentical(anotherUnit, records[1].defaultUnit!)
        XCTAssertIdentical(newUnit, records[2].defaultUnit!)
        XCTAssertIdentical(newUnit, records[3].defaultUnit!)
        XCTAssertIdentical(newUnit, records[4].defaultUnit!)
    }
    
    func testReplaceAllDefaultUnits_noneReplaced_valid() {
        // Arrange
        let _ = Record.populate(5, in: viewContext, unit: unit)
        let newUnit = RecordUnit.make(in: viewContext, isNone: false, title: "new")
        let anotherUnit = RecordUnit.make(in: viewContext, isNone: false, title: "another")
        // Act
        let counts = Record.replaceAllDefaultUnits(from: anotherUnit, to: newUnit, in: viewContext)
        // Assert
        XCTAssertEqual(0, counts)
    }
}
