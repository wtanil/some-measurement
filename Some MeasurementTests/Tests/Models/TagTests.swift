//
//  TagTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 22/02/22.
//

import XCTest
@testable import Some_Measurement
import CoreData

class TagTests: XCTestCase {
    
    var persistenceController: PersistenceController!
    var viewContext: NSManagedObjectContext!
    
    override func setUp() {
        persistenceController = PersistenceController()
        viewContext = persistenceController.container.viewContext
    }
    
    override func tearDown() {
        viewContext = nil
        persistenceController.resetStore()
        persistenceController = nil
        
    }
    
    // MARK: - displayName: String
    func testDisplayName_valid_value() {
        // Arrange
        let validTag = Tag.make(in: viewContext, name: "tag")
        let nilTag = Tag.make(in: viewContext, name: nil)
        // Act
        let validDisplayName = validTag.displayName
        let nilDisplayName = nilTag.displayName
        // Assert
        XCTAssertEqual("tag", validDisplayName)
        XCTAssertEqual("Unknown name", nilDisplayName)
    }
    
    // MARK: - isNameValid: Bool
    func testIsNameValid_notEmpty_true() {
        // Arrange
        let tag = Tag.make(in: viewContext, name: "Tag Name")
        // Act
        let isNameValid = tag.isNameValid
        // Assert
        XCTAssertTrue(isNameValid)
    }
    
    func testIsNameValid_empty_false() {
        // Arrange
        let tag = Tag.make(in: viewContext, name: "")
        // Act
        let isNameValid = tag.isNameValid
        // Assert
        XCTAssertFalse(isNameValid)
    }
    
    func testIsNameValid_nil_false() {
        // Arrange
        let tag = Tag.make(in: viewContext, name: nil)
        // Act
        let isNameValid = tag.isNameValid
        // Assert
        XCTAssertFalse(isNameValid)
    }
    
    // MARK: - isUnique(NSManagedObjectContext): Bool
    func testIsUnique_valid_true() {
        // Arrange
        let _ = Tag.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let tag = Tag.make(in: viewContext, name: "New tag name")
        // Act
        let isUnique = tag.isUnique(in: viewContext)
        // Assert
        XCTAssertTrue(isUnique)
    }
    
    func testIsUnique_notUnique_false() {
        // Arrange
        let tags = Tag.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let tag = Tag.make(in: viewContext, name: tags.first!.name)
        // Act
        let isUnique = tag.isUnique(in: viewContext)
        // Assert
        XCTAssertFalse(isUnique)
    }
    
    func testIsUnique_notUniqueCaseInsensitive_false() {
        // Arrange
        let tags = Tag.populate(5, in: viewContext)
        tags[0].name = "CaseInsensitiveName Tag"
        persistenceController.persist(for: viewContext)
        let tag = Tag.make(in: viewContext, name: "caseinsensitivename tag")
        // Act
        let isUnique = tag.isUnique(in: viewContext)
        // Assert
        XCTAssertFalse(isUnique)
    }
    
    // MARK: - isValid(NSManagedObjectContext): Bool
    func testIsValid_valid_true() {
        // Arrange
        let _ = Tag.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let tag = Tag.make(in: viewContext, name: "unique tag")
        // Act
        let isValid = tag.isValid(in: viewContext)
        // Assert
        XCTAssertTrue(isValid)
    }
    
    func testIsValid_nameIsEmpty_false() {
        // Arrange
        let _ = Tag.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let tag = Tag.make(in: viewContext, name: "")
        // Act
        let isValid = tag.isValid(in: viewContext)
        // Assert
        XCTAssertFalse(isValid)
    }
    
    func testIsValid_nameIsNil_false() {
        // Arrange
        let _ = Tag.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let tag = Tag.make(in: viewContext, name: nil)
        // Act
        let isValid = tag.isValid(in: viewContext)
        // Assert
        XCTAssertFalse(isValid)
    }
    
    func testIsValid_notUnique_false() {
        // Arrange
        let tags = Tag.populate(5, in: viewContext)
        persistenceController.persist(for: viewContext)
        let tag = Tag.make(in: viewContext, name: tags.first!.name)
        // Act
        let isValid = tag.isValid(in: viewContext)
        // Assert
        XCTAssertFalse(isValid)
    }
    
}
