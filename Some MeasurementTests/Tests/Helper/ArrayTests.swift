//
//  ArrayTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 14/03/22.
//

import XCTest
@testable import Some_Measurement

class ArrayTests: XCTestCase {
    var array: [Int]!
    
    override func setUp() {
        array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15]
    }
    
    override func tearDown() {
        array = nil
    }
    
    // MARK: - remove(Element) where Element: Equatable
    func testRemove_elementExists_elementRemoved() {
        // Arrange
        // Act
        array.remove(1)
        // Assert
        XCTAssertFalse(array.contains(1))
        XCTAssertEqual(10, array.count)
        // Act
        array.remove(10)
        // Assert
        XCTAssertFalse(array.contains(10))
        XCTAssertEqual(9, array.count)
    }
    
    func testRemove_elementDoesntExist_skip() {
        // Arrange
        // Act
        array.remove(11)
        // Assert
        XCTAssertEqual(11, array.count)
        XCTAssertFalse(array.contains(11))
    }
    
    // MARK: - appendAndSort(Element, (Element, Element) -> Bool)
    func testAppendAndSort_valid_appendedAndSorted() {
        // Arrange
        // Act
        array.appendAndSort(13, comparator: { $0 > $1 })
        // Assert
        XCTAssertEqual(12, array.count)
        XCTAssertEqual(15, array.first)
        XCTAssertEqual(1, array.last)
    }
    
    func testAppendAndSort_valid2_appendedAndSorted() {
        // Arrange
        // Act
        array.appendAndSort(0, comparator: { $0 < $1 })
        // Assert
        XCTAssertEqual(12, array.count)
        XCTAssertEqual(0, array.first)
        XCTAssertEqual(15, array.last)
    }
}
