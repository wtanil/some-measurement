//
//  ValidatorTests.swift
//  Some MeasurementTests
//
//  Created by William Suryadi Tanil on 15/03/22.
//

import XCTest
@testable import Some_Measurement

class ValidatorTests: XCTestCase {
    var validator: Validator!
    
    override func setUp() {
        validator = Validator()
    }
    
    override func tearDown() {
        validator = nil
    }
    
    // MARK: - validateStringIsNotEmpty(String) -> Bool
    func testValidateStringIsNotEmpty_validString_true() {
        // Arrange
        let string = "string"
        let numberString = "123"
        // Act
        let result = validator.validateStringIsNotEmpty(string: string)
        let numberResult = validator.validateStringIsNotEmpty(string: numberString)
        // Assert
        XCTAssertTrue(result)
        XCTAssertTrue(numberResult)
    }
    
    func testValidateStringIsNotEmpty_emptyString_false() {
        // Arrange
        let string = ""
        // Act
        let result = validator.validateStringIsNotEmpty(string: string)
        // Assert
        XCTAssertFalse(result)
    }
    
    // MARK: - validateStringIsDecimal(String) -> Bool
    func testValidateStringIsDecimal_valid_true() {
        // Arrange
        let string = "123"
        // Act
        let result = validator.validateStringIsDecimal(string: string)
        // Assert
        XCTAssertTrue(result)
    }
    
    func testValidateStringIsDecimal_notNumber_false() {
        // Arrange
        let emptyString = ""
        let notNumberString = "abc"
        let combinationString = "1ab2ba1"
        // Act
        let emptyResult = validator.validateStringIsDecimal(string: emptyString)
        let notNumberResult = validator.validateStringIsDecimal(string: notNumberString)
        let combinationResult = validator.validateStringIsDecimal(string: combinationString)
        // Assert
        XCTAssertFalse(emptyResult)
        XCTAssertFalse(notNumberResult)
        XCTAssertFalse(combinationResult)
    }
    
}
