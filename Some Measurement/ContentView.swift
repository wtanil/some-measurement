//
//  ContentView.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 06/12/20.
//

import SwiftUI
import CoreData

struct ContentView: View {
    typealias Labels = Constant.ContentView
    
    @Environment(\.managedObjectContext) private var viewContext
    @AppStorage(UserDefaults.Keys.isNotFirstTime.rawValue) var isNotFirstTime: Bool = false
    
    @State private var selection: Tab = .main
    
    enum Tab {
        case main, setting
    }
    
    var body: some View {
        
        if isNotFirstTime {
            TabView(selection: $selection) {
                
                MeasurementHome(persistenceController: PersistenceController.shared)
                    .tabItem {
                        Label(Labels.homeTabItemTitle, systemImage: "house")
                            .accessibilityIdentifier("HomeTabButton")
                    }
                    .tag(Tab.main)
                    
                
                SettingHome()
                    .tabItem {
                        Label(Labels.settingTabItemTitle, systemImage: "gear")
                            .accessibilityIdentifier("SettingTabButton")
                    }
                    .tag(Tab.setting)
            }

        } else {
            tutorialView
        }
    }
}

extension ContentView {
    private var tutorialView: some View {
        VStack {
            Spacer()
            TutorialView()
            Spacer()
            VStack(alignment: .trailing) {
                Button(action: {
                    setIsNotFirstTime()
                }, label: {
                    Text(Labels.tutorialDoneButtonLabel)
                })
                    .accessibilityIdentifier("SkipButton")
            }
            .padding(5)
        }
    }
    
    func setIsNotFirstTime(as bool: Bool = true) {
        isNotFirstTime = bool
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let viewContext = PersistenceController.preview.container.viewContext
        
        // Uncomment to start preview with a new User Defaults each time
        //        setup()
        
        return ContentView()
            .environment(\.managedObjectContext, viewContext)
    }
    
    private static func setup() {
        UserDefaults.standard.reset()
    }
}
