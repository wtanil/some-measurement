//
//  Persistence.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 06/12/20.
//

import CoreData

struct PersistenceController {
    static let shared = PersistenceController()
    
    let container: NSPersistentContainer
    
    enum StorageType {
        case persistent, inMemory
    }
    
    init(_ storageType: StorageType = .persistent) {
        container = NSPersistentContainer(name: "Some_Measurement")
        
        if storageType == .inMemory || ProcessInfo.processInfo.environment["is_unit_test"] == "1" {
            let description = NSPersistentStoreDescription()
            description.url = URL(fileURLWithPath: "/dev/null")
            self.container.persistentStoreDescriptions = [description]
        }
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does #imageLiteral(resourceName: "persistenceControlter PersistenceControlter.png")not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
}

// MARK: -
extension PersistenceController {
    
    // from Donny Wals presentation about Core Data and SwiftUI
    func childViewContext() -> NSManagedObjectContext {
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.parent = container.viewContext
        return context
    }
    
    // from Donny Wals presentation about Core Data and SwiftUI
    func newTemporaryInstance<T: NSManagedObject>(in context: NSManagedObjectContext) -> T {
//        let entityDescription = NSEntityDescription.entity(forEntityName: "Record", in: context)
//        let record = Record(entity: entityDescription!, insertInto: context)
        
        let entityDescription: NSEntityDescription = NSEntityDescription.entity(forEntityName: String(describing: T.self), in: context)!
        return T(entity: entityDescription, insertInto: context)
    }
    
    // from Donny Wals presentation about Core Data and SwiftUI
    func copyForEditing<T: NSManagedObject>(of object: T, in context: NSManagedObjectContext) -> T {
        guard let object = (try? context.existingObject(with: object.objectID)) as? T else {
            fatalError("Requested copy of a managed object that doesn't exist")
        }
        return object
    }
    
    func delete<T: NSManagedObject>(_ object: T, in context: NSManagedObjectContext) {
        context.delete(object)
    }
    
    /*
    func has<T: NSManagedObject>(_ object: T, with predicate: NSPredicate, in context: NSManagedObjectContext) -> Bool {
        let fetchRequest = T.fetchRequest()
        fetchRequest.predicate = predicate
        
        do {
            let count = try context.count(for: fetchRequest)
            
            if count > 0 {
                return true
            }
        } catch { fatalError("Failed checking unit") }
        
        return false
    }
    */
    
    func hasUnitNone(in context: NSManagedObjectContext) -> Bool {
        
        let fetchRequest = RecordUnit.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "isNone = true")
        
        do {
            let count = try context.count(for: fetchRequest)
            
            if count > 0 {
                return true
            }
        } catch { fatalError("Failed checking unit") }
        
        return false
    }
    
    // from Donny Wals presentation about Core Data and SwiftUI
    func persist(_ object: NSManagedObject) {
        do {
            try object.managedObjectContext?.save()
            if let parent = object.managedObjectContext?.parent {
                try parent.save()
            }
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
    
    func persist(for context: NSManagedObjectContext) {
        do {
            try context.save()
            if let parent = context.parent {
                try parent.save()
            }
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
    
}

// MARK: - reset
extension PersistenceController {
    func resetStore() {

        let storeCoordinator = container.persistentStoreCoordinator

        for store in storeCoordinator.persistentStores {
            try? storeCoordinator.destroyPersistentStore(
                at: store.url!,
                ofType: store.type,
                options: nil)
        }
    }
}

// MARK: - preview
extension PersistenceController {
    
    static var preview: PersistenceController = {
//        let result = PersistenceController(inMemory: true)
        let result = PersistenceController(.inMemory)
        let viewContext = result.container.viewContext
        
//        generateExamples(context: viewContext)
        
        return result
    }()
    
//    static func generateExamples(context viewContext: NSManagedObjectContext) {
//        // example Unit
//        
//        // set unitNone as default unit
//        let unitNone = RecordUnit.create(context: viewContext, title: "none")
//        unitNone.isNone = true
//        _ = RecordUnit.create(context: viewContext, title: "m")
//        let unitMm = RecordUnit.create(context: viewContext, title: "mm")
//        let unitL = RecordUnit.create(context: viewContext, title: "L")
//        let unitS = RecordUnit.create(context: viewContext, title: "s")
//        // end of example Unit
//        
//        // example Tag
//        let tagPlant = Tag.create(context: viewContext, name: "Plant")
//        let tagComputer = Tag.create(context: viewContext, name: "Computer")
//        let tagDuration = Tag.create(context: viewContext, name: "Duration")
//        let tagAquarium = Tag.create(context: viewContext, name: "Aquarium")
//        let tagHealth = Tag.create(context: viewContext, name: "Health")
//        let tagWater = Tag.create(context: viewContext, name: "Water")
//        _ = Tag.create(context: viewContext, name: "Empty")
//        // end of example Tag
//        
//        // example Measurement
//        _ = Record.create(context: viewContext,
//                          title: "Empty measurement",
//                          note: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
//                          tags: [],
//                          defaultUnit: unitNone)
//        
//        let measurementPlantGrowth = Record.create(context: viewContext,
//                                                   title: "Plant growth",
//                                                   note: "Spinach growth journal",
//                                                   tags: [tagPlant],
//                                                   defaultUnit: unitMm)
//        for index in 0..<21 {
//            _ = Value.create(context: viewContext,
//                             record: measurementPlantGrowth,
//                             amount: Double(index),
//                             date: Date().addingTimeInterval(TimeInterval(-1 * (21 - index) * 86400)),
//                             unit: unitMm)
//        }
//        
//        let measurementDrinkWater = Record.create(context: viewContext,
//                                                  title: "Daily water intake",
//                                                  note: "",
//                                                  tags: [tagHealth, tagWater],
//                                                  defaultUnit: unitL)
//        _ = Value.create(context: viewContext,
//                         record: measurementDrinkWater,
//                         amount: 2.3,
//                         date: Date().addingTimeInterval(-1 * 5 * 86400),
//                         unit: unitL)
//        _ = Value.create(context: viewContext,
//                         record: measurementDrinkWater,
//                         amount: 2.2,
//                         date: Date().addingTimeInterval(-1 * 4 * 86400),
//                         unit: unitL)
//        _ = Value.create(context: viewContext,
//                         record: measurementDrinkWater,
//                         amount: 2.1,
//                         date: Date().addingTimeInterval(-1 * 3 * 86400),
//                         unit: unitL)
//        _ = Value.create(context: viewContext,
//                         record: measurementDrinkWater,
//                         amount: 2.4,
//                         date: Date().addingTimeInterval(-1 * 2 * 86400),
//                         unit: unitL)
//        _ = Value.create(context: viewContext,
//                         record: measurementDrinkWater,
//                         amount: 2.3,
//                         date: Date().addingTimeInterval(-1 * 1 * 86400),
//                         unit: unitL)
//        
//        let measurementAquariumInhabitant = Record.create(context: viewContext,
//                                                          title: "Aquarium inhabitants",
//                                                          note: "Mostly fish",
//                                                          tags: [tagAquarium, tagWater, tagPlant],
//                                                          defaultUnit: unitNone)
//        _ = Value.create(context: viewContext,
//                         record: measurementAquariumInhabitant,
//                         amount: 10.0,
//                         date: Date().addingTimeInterval(-1 * 31 * 86400),
//                         unit: unitNone)
//        _ = Value.create(context: viewContext,
//                         record: measurementAquariumInhabitant,
//                         amount: 30.0,
//                         date: Date().addingTimeInterval(-1 * 3 * 86400),
//                         unit: unitNone)
//        
//        let measurementComputerStartTime = Record.create(context: viewContext,
//                                                         title: "Computer start time",
//                                                         note: "", tags: [tagComputer, tagDuration],
//                                                         defaultUnit: unitS)
//        
//        for index in 0..<5 {
//            
//            if index != 2 {
//                _ = Value.create(context: viewContext,
//                                 record: measurementComputerStartTime,
//                                 amount: 10.0 - Double(index),
//                                 date: Date().addingTimeInterval(TimeInterval(-1 * (21 - index) * 86400)),
//                                 unit: unitS)
//            }
//            
//        }
//        // end of example Measurement
//        
//        do {
//            try viewContext.save()
//        } catch {
//            // Replace this implementation with code to handle the error appropriately.
//            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            let nsError = error as NSError
//            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
//        }
//    }
}
