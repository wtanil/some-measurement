//
//  Tag+extension.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 25/01/21.
//

import Foundation
import CoreData

// Default
extension Tag {
    override public func awakeFromInsert() {
        super.awakeFromInsert()
        
        setPrimitiveValue("", forKey: "name")
    }
}

extension Tag {
    var displayName: String {
        name ?? "Unknown name"
    }
}

// Validator
extension Tag {
    
    var isNameValid: Bool {
        guard let name = name else {
            return false
        }
        if name.isEmpty {
            return false
        }
        return true
    }
    
    func isUnique(in context: NSManagedObjectContext) -> Bool {
        let fetchRequest = Tag.fetchRequest()
        
        guard let name = name else { return false }
        fetchRequest.predicate = NSPredicate(format: "name =[c] %@", name)
        
//        fetchRequest.predicate = NSPredicate(format: "name =[c] %@", displayName)
        
        do {
            let count = try context.count(for: fetchRequest)
            
            if count > 1 {
                return false
            }
        } catch { fatalError("Error verifying object is unique") }
        
        return true
    }
    
    func isValid(in context: NSManagedObjectContext) -> Bool {
        if !isUnique(in: context) {
            return false
        }
        
        if !isNameValid {
            return false
        }
        
        return true
    }
    
}

// Placeholder object
extension Tag {
    static func placeholder(in context: NSManagedObjectContext) -> Tag {
        let placeholderTag = Tag.make(in: context, name: "placeholder tag")
        return placeholderTag
    }
    
    static func make(in context: NSManagedObjectContext, name: String?) -> Tag {
//        let tag = Tag.init(entity: Tag.entity(), insertInto: context)
//        let tag = Tag(context: context)
        let entityDescription = NSEntityDescription.entity(forEntityName: "Tag", in: context)
        let tag = Tag(entity: entityDescription!, insertInto: context)
        tag.name = name
        
        return tag
    }
    
    static func populate(_ n: Int, in context: NSManagedObjectContext) -> [Tag] {
        var tags = [Tag]()
        for index in 0..<n {
            let tempTag = Tag.make(in: context, name: "tag\(index)")
            tags.append(tempTag)
        }
        return tags
    }
}
