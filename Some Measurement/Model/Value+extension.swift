//
//  Value+extension.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 09/02/21.
//

import Foundation
import CoreData

// Default
extension Value {
    
    override public func awakeFromInsert() {
        super.awakeFromInsert()
        
        setPrimitiveValue(0, forKey: "value")
        setPrimitiveValue(Date(), forKey: "date")
        setPrimitiveValue(Date(), forKey: "createDate")
        setPrimitiveValue(Date(), forKey: "updateDate")
    }
    
    override public func willSave() {
        if let updateDate = updateDate {
            let timedifference = Date().timeIntervalSinceReferenceDate - updateDate.timeIntervalSinceReferenceDate
            if timedifference > 10 {
                self.updateDate = Date()
            }
        } else {
            self.updateDate = Date()
        }
        super.willSave()
    }
}

extension Value {
    
    var displayValue: String {
        NumberFormatter.numberFormatterDecimal.string(from: NSNumber(value: value)) ?? "Unknown value"
    }
    
    var displayValueWithUnit: String {
        
        let valueString = displayValue
        
        guard let unit = unit else {
            return valueString
        }
        let space: String = unit.displayTitle == "" ? "" : " "
        let unitString = unit.displayTitle
        
        return "\(valueString)\(space)\(unitString)"
        
    }
    
    var displayDate: String {
        formattedDate(for: date)
    }
    
    var displayCreateDate: String {
        formattedDate(for: createDate)
    }
    
    var displayUpdateDate: String {
        formattedDate(for: updateDate)
    }
    
    private func formattedDate(for date: Date?) -> String {
        guard let date = date else { return "Unknown date" }
        return DateFormatter.dateFormatterMediumMedium.string(from: date)
    }
    
}

extension Value {
    
    func set(amount: Double, date: Date, unit: RecordUnit) {
        self.value = amount
        self.date = date
        self.unit = unit
    }
    
    static func replaceAllUnits(from oldUnit: RecordUnit, to newUnit: RecordUnit, in context: NSManagedObjectContext) -> Int {
        let fetchRequest: NSFetchRequest<Value> = Value.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "unit == %@", oldUnit)
        
        do {
            
            let targetValues = try context.fetch(fetchRequest)
            
            for value in targetValues {
                value.unit = newUnit
//                value.updateDate = Date()
                
                // refresh affected value
                // because relationship changes doesn't trigger context refresh
                context.refresh(value, mergeChanges: true)
            }
            
            return targetValues.count
            
        } catch {
            fatalError("Error replacing units")
        }
    }
}

// Placeholder object
extension Value {
    static func placeholder(in context: NSManagedObjectContext) -> Value {
        return Value.make(in: context, record: nil, unit: nil, amount: 10)
    }
    
    static func make(in context: NSManagedObjectContext, record: Record?, unit: RecordUnit?, amount: Double) -> Value {
//        let value = Value(context: context)
//        let value = Value.init(entity: Value.entity(), insertInto: context)
        let entityDescription = NSEntityDescription.entity(forEntityName: "Value", in: context)
        let value = Value(entity: entityDescription!, insertInto: context)
        value.value = amount
        value.updateDate = Date()
        value.record = record
        value.unit = unit
        
        return value
    }
    
    static func populate(_ n: Int, in context: NSManagedObjectContext, record: Record, unit: RecordUnit) -> [Value] {
        var values = [Value]()
        for index in 0..<n {
            let amount: Double = Double(index)
            let tempValue = Value.make(in: context, record: record, unit: unit, amount: amount)
            values.append(tempValue)
        }
        return values
    }
}
