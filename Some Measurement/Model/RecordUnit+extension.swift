//
//  RecordUnit+extension.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 24/03/21.
//

import Foundation
import CoreData

extension RecordUnit {
    override public func awakeFromInsert() {
        super.awakeFromInsert()
        
        setPrimitiveValue(false, forKey: "isNone")
        setPrimitiveValue("", forKey: "title")
    }
}

extension RecordUnit {
    var displayTitle: String {
        if isNone { return "" }
        guard let title = title else { return "Unknown unit"}
        return title
    }
    
    var displayListTitle: String {
        guard let title = title else { return "Unknown unit"}
        return title
    }
}

// Validator
extension RecordUnit {
    
    var isTitleValid: Bool {
        guard let title = title else {
            return false
        }
        if title.isEmpty{
            return false
        }
        return true
    }
    
    func isValid(in context: NSManagedObjectContext) -> Bool {
        if !isUnique(in: context) {
            return false
        }
        
        if !isTitleValid {
            return false
        }
        
        return true
    }
    
    func isUnique(in context: NSManagedObjectContext) -> Bool {
        let fetchRequest = RecordUnit.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "title = %@", displayTitle)
        
        do {
            let count = try context.count(for: fetchRequest)
            
            if count > 1 {
                return false
            }
        } catch { fatalError("Error verifying object is unique") }
        
        return true
    }
}

extension RecordUnit {
    
    static func createUnitNone(in context: NSManagedObjectContext) -> RecordUnit {
//        let newUnit = RecordUnit(context: context)
        let entityDescription = NSEntityDescription.entity(forEntityName: "RecordUnit", in: context)
        let newUnit = RecordUnit(entity: entityDescription!, insertInto: context)
        newUnit.title = "none"
        newUnit.isNone = true
        
        return newUnit
    }
    
    static func getUnitNone(in context: NSManagedObjectContext) -> RecordUnit {
        let fetchRequest: NSFetchRequest<RecordUnit> = RecordUnit.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "isNone = true")
        do {
            
            let fetchedUnits = try context.fetch(fetchRequest)
            
            return fetchedUnits[0]
        } catch {
            fatalError("Failed fetching unit none")
        }
    }
    
}

// Placeholder object
extension RecordUnit {
    
    static func placeholder(in context: NSManagedObjectContext) -> RecordUnit {
        return RecordUnit.make(in: context, isNone: false, title: "unit")
    }
    
    static func make(in context: NSManagedObjectContext, isNone: Bool, title: String?) -> RecordUnit {
//        let unit = RecordUnit(context: context)
//        let unit = RecordUnit.init(entity: RecordUnit.entity(), insertInto: context)
        let entityDescription = NSEntityDescription.entity(forEntityName: "RecordUnit", in: context)
        let unit = RecordUnit(entity: entityDescription!, insertInto: context)
        unit.isNone = isNone
        unit.title = title
        
        return unit
    }
    
    static func populate(_ n: Int, in context: NSManagedObjectContext) -> [RecordUnit] {
        var units = [RecordUnit]()
        for index in 0..<n {
            let tempUnit = RecordUnit.make(in: context, isNone: false, title: "unit\(index)")
            units.append(tempUnit)
        }
        return units
    }
}
