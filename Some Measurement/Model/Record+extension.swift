//
//  Record+extension.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 22/01/21.
//

import Foundation
import CoreData
import SwiftUI

// Default
extension Record {
    
    override public func awakeFromInsert() {
        super.awakeFromInsert()
        
        setPrimitiveValue("", forKey: "title")
        setPrimitiveValue(Date(), forKey: "createDate")
        setPrimitiveValue(Date(), forKey: "updateDate")
        setPrimitiveValue(0, forKey: "quickUpdateInterval")
        setPrimitiveValue("", forKey: "note")
    }
    
    override public func willSave() {
        if let updateDate = updateDate {
            let timedifference = Date().timeIntervalSinceReferenceDate - updateDate.timeIntervalSinceReferenceDate
            if timedifference > 10 {
                self.updateDate = Date()
            }
        } else {
            self.updateDate = Date()
        }
        super.willSave()
    }
}

extension Record {
    
    enum SortOption: String, CaseIterable, Identifiable {
        case title = "Title"
        case latest = "Date Added (Latest)"
        case oldest = "Date Added (Oldest)"
        case lastUpdate = "Recently Updated"
        
        var id: String { self.rawValue }
    }
    
    var displayTitle: String {
        title ?? "Unknown title"
    }
    
    var displayNote: String {
        note ?? "Unknown note"
    }
    
    var displayQuickUpdateInterval: String {
        NumberFormatter.numberFormatterDecimal.string(from: NSNumber(value: quickUpdateInterval)) ?? "Unknown value"
    }
    
    var displayCreateDate: String {
        formattedDate(for: createDate)
    }
    
    var displayUpdateDate: String {
        formattedDate(for: updateDate)
    }
    
    private func formattedDate(for date: Date?) -> String {
        guard let date = date else { return "Unknown date" }
        return DateFormatter.dateFormatterMediumMedium.string(from: date)
    }
    
    var displayTags: String {
        guard let tags = tags else {
            // won't trigger since child will always return empty set
            return "Unknown tags"
        }
        let set: Set<Tag> = tags as! Set<Tag>
        if set.isEmpty { return "No tags" }
        let strings: [String] = set.map { $0.displayName }.sorted(by: <)
        return strings.joined(separator: ", ")
    }
    
    var tagsAsSet: Set<Tag> {
        guard let tags = tags else {
            return Set<Tag>()
        }
        let set: Set<Tag> = tags as! Set<Tag>
        return set
    }
    
    var tagsAsArray: [Tag] {
        guard let tags = tags else {
            return [Tag]()
        }
        let set: Set<Tag> = tags as! Set<Tag>
        let array: [Tag] = Array(set)
        return array
    }
    
    var valuesAsArray: [Value] {
        guard let values = values else {
            return [Value]()
        }
        let array: [Value] = values.allObjects as! [Value]
        return array
    }
    
    var lastValue: Value? {
        guard let values = values else {
            return nil
        }
        
        let array: [Value] = (values.allObjects as! [Value]).sorted { $1.date! < $0.date! }
        let lastValue = array.first
        
        return lastValue
    }
    
    // Binding helper
    var computedTags: [Tag] {
        get {
            tagsAsArray
        }
        set {
            self.tags = NSSet(array: newValue)
        }
    }
}

// Validation
extension Record {
    var isTitleValid: Bool {
        guard let title = title else {
            return false
        }
        if title.isEmpty{
           return false
        }
        return true
    }
    
    func isValid(in context: NSManagedObjectContext) -> Bool {
        if !isUnique(in: context) {
            return false
        }
        
        if !isTitleValid {
            return false
        }
        
        return true
    }
    
    func isUnique(in context: NSManagedObjectContext) -> Bool {
        let fetchRequest = Record.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "title =[c] %@", displayTitle)
        
        do {
            let count = try context.count(for: fetchRequest)
            
            if count > 1 {
                return false
            }
        } catch { fatalError("Error verifying object is unique") }
        
        return true
    }
    
}

extension Record {
    
    static func replaceAllDefaultUnits(from oldUnit: RecordUnit, to newUnit: RecordUnit, in context: NSManagedObjectContext) -> Int {

        let fetchRequest: NSFetchRequest<Record> = Record.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "defaultUnit == %@", oldUnit)
        
        do {
            
            let targetRecords = try context.fetch(fetchRequest)
            
            for record in targetRecords {
                record.defaultUnit = newUnit
//                record.updateDate = Date()
                
                // refresh affected record
                // because relationship changes doesn't trigger context refresh
                context.refresh(record, mergeChanges: true)
            }
            
            return targetRecords.count
            
        } catch {
            fatalError("Error replacing default units")
        }
    }
    
}

// Placeholder object
extension Record {
    
    static func placeholder(in context: NSManagedObjectContext) -> Record {
        
        let placeholderUnit = RecordUnit.make(in: context, isNone: false, title: "unit")
        
        let placeholderMeasurement = Record.make(in: context,
                                                 unit: placeholderUnit,
                                                 title: "Lorem ipsum dolor sit amet",
                                                 note: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                                                 quickUpdateInterval: 1)
        
        for index in 0...3 {
            let placeholderTag = Tag.make(in: context, name: "Test tag \(index)")
            placeholderMeasurement.addToTags(placeholderTag)
        }
        
        for index in 0...3 {
            let placeholderValue = Value.make(in: context, record: placeholderMeasurement, unit: placeholderUnit, amount: Double(index))
            placeholderMeasurement.addToValues(placeholderValue)
        }
        
        return placeholderMeasurement
    }
    
    static func make(in context: NSManagedObjectContext,
                     unit: RecordUnit?,
                     title: String?,
                     note: String?,
                     quickUpdateInterval: Double
    ) -> Record {
//        let record = Record(context: context)
//        let record = Record.init(entity: Record.entity(), insertInto: context)
//        let record = Record(entity: Record.entity(), insertInto: context)
        let entityDescription = NSEntityDescription.entity(forEntityName: "Record", in: context)
        let record = Record(entity: entityDescription!, insertInto: context)
        record.title = title
        record.note = note
        record.quickUpdateInterval = quickUpdateInterval
        record.updateDate = Date()
        record.defaultUnit = unit
        return record
    }
    
    static func populate(_ n: Int, in context: NSManagedObjectContext, unit: RecordUnit) -> [Record] {
        var records = [Record]()
        for index in 0..<n {
            let tempRecord = Record.make(in: context, unit: unit, title: "Record \(index)", note: "notes", quickUpdateInterval: 1)
            records.append(tempRecord)
        }
        return records
    }
}
