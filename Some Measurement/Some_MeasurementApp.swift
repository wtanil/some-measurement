//
//  Some_MeasurementApp.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 06/12/20.
//

import SwiftUI

@main
struct Some_MeasurementApp: App {
    
    var body: some Scene {
        
        preSetUpTestConfiguration()
        
        let persistenceController = PersistenceController.shared
        
        setUp(with: persistenceController)
        
        postSetUpTestConfiguration()
        
        return WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
    
    private func setUp(with persistenceController: PersistenceController) {
        
        let viewContext = persistenceController.container.viewContext
        // Make sure UnitNone exist in core data
        if !UserDefaults.standard.bool(forKey: UserDefaults.Keys.isNotFirstTime.rawValue) {
            
            if !persistenceController.hasUnitNone(in: viewContext) {
                let unitNone = RecordUnit.createUnitNone(in: viewContext)
                persistenceController.persist(unitNone)
            }
        }
    }
}

extension Some_MeasurementApp {
    
    private func preSetUpTestConfiguration() {
        
        if isUITest {
            //            resetUserDefault()
            let keyWindow = UIApplication
                .shared
                .connectedScenes
                .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
                .first { $0.isKeyWindow }
            keyWindow?.layer.speed = 50
        }
        
        if shouldUseAppData {
            UserDefaults.standard.set(true, forKey: UserDefaults.Keys.isNotFirstTime.rawValue)
            loadAppData()
        }
    }
    
    private func postSetUpTestConfiguration() {
        
        if shouldResetState {
            PersistenceController.shared.resetStore()
            resetUserDefault()
        }
    }
    
    private var isUITest: Bool {
        CommandLine.arguments.contains("--uitest") ? true : false
    }
    
    private var shouldUseAppData: Bool {
        CommandLine.arguments.contains("--useappdata") ? true : false
    }
    
    private var shouldResetState: Bool {
        CommandLine.arguments.contains("--resetstate") ? true : false
    }
    
    private func resetUserDefault() {
        // reset UserDefaults
        let defaultsName = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: defaultsName)
    }
    
    private func loadAppData() {
        
        guard let contentsURL = Bundle.main.url(forResource: "UITestAppData", withExtension: "xcappdata")?.appendingPathComponent("AppData") else {
            return
        }
        
        guard let destinationRoot = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last?.deletingLastPathComponent() else {
            return
        }
        
        guard let enumerator = Foundation.FileManager.default.enumerator(at: contentsURL, includingPropertiesForKeys: [.isDirectoryKey], options: [], errorHandler: nil) else {
            return
        }
        
        while let sourceURL = enumerator.nextObject() as? URL {
            guard let resourceValues = try? sourceURL.resourceValues(forKeys: [.isDirectoryKey]),
                  let isDirectory = resourceValues.isDirectory,
                  !isDirectory else { continue }
            
            let path = sourceURL.standardizedFileURL.path.replacingOccurrences(of: contentsURL.standardizedFileURL.path, with: "")
            let destinationURL = destinationRoot.appendingPathComponent(path)
            
            do {
                try Foundation.FileManager.default.createDirectory(at: destinationURL.deletingLastPathComponent(), withIntermediateDirectories: true, attributes: nil)
                
                if Foundation.FileManager.default.fileExists(atPath: destinationURL.path) {
                    try Foundation.FileManager.default.removeItem(at: destinationURL)
                }
                
                // copy new file from appdata
                try Foundation.FileManager.default.copyItem(at: sourceURL, to: destinationURL)
            }
            catch {
                print("loadappdata error \(error.localizedDescription)")
            }
        }
    }
    
}

/*
 @main
 struct Some_MeasurementApp: App {
 
 let persistenceController = PersistenceController.shared
 
 var body: some Scene {
 
 setup(with: persistenceController)
 
 return WindowGroup {
 ContentView()
 .environment(\.managedObjectContext, persistenceController.container.viewContext)
 }
 }
 
 private func setup(with persistenceController: PersistenceController) {
 
 // resetState
 if isUITest {
 resetState()
 }
 
 let viewContext = persistenceController.container.viewContext
 // Make sure UnitNone exist in core data
 if !UserDefaults.standard.bool(forKey: UserDefaults.Keys.isNotFirstTime.rawValue) {
 
 if !persistenceController.hasUnitNone(in: viewContext) {
 let unitNone = RecordUnit.createUnitNone(in: viewContext)
 persistenceController.persist(unitNone)
 }
 }
 
 // helper for unit testing
 if shouldSkipTutorial {
 UserDefaults.standard.set(true, forKey: UserDefaults.Keys.isNotFirstTime.rawValue)
 }
 }
 
 private var shouldSkipTutorial: Bool {
 CommandLine.arguments.contains("--skiptutorial") ? true : false
 }
 
 private var isUITest: Bool {
 CommandLine.arguments.contains("--uitest") ? true : false
 }
 
 private func resetState() {
 // reset UserDefaults
 let defaultsName = Bundle.main.bundleIdentifier!
 UserDefaults.standard.removePersistentDomain(forName: defaultsName)
 // reset CoreData
 }
 }
 */
