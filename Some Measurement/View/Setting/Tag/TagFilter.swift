//
//  TagFilter.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 15/03/21.
//

import SwiftUI

struct TagFilter: View {
    typealias Labels = Constant.TagFilter
    
    @Binding var selectedTags: [Tag]
    @State private var searchKeyword: String = ""
    
    private let sortDescriptor: NSSortDescriptor = NSSortDescriptor(keyPath: \Tag.name, ascending: true)
    
    var body: some View {
        
        VStack(alignment: .leading) {
            Text(filterLabel)
                .accessibilityIdentifier("SelectedTagText")
            
            TextField(Labels.searchTextField, text: $searchKeyword)
                .disableAutocorrection(true)
                .accessibilityIdentifier("SearchTextfield")
            
            DynamicList(sortDescriptors: [sortDescriptor], predicate: predicate) { (tag: Tag) in
                Button(action: {
                    updateSelectedTags(with: tag)
                }, label: {
                    HStack {
                        Text(tag.displayName).layoutPriority(1)
                        
                        if selectedTags.contains(tag) {
                            Spacer(minLength: 5)
                            Image(systemName: "checkmark").layoutPriority(1)
                        }
                    }
                })
                .accessibilityIdentifier("TagCell-\(tag.displayName)")
            }
        }
        .padding()
        .navigationTitle(Labels.navigationBarTitle)
        .navigationBarTitleDisplayMode(.inline)
    }
}

extension TagFilter {
    
    private var filterLabel: String {
        if selectedTags.isEmpty {
            return Labels.emptySelectedLabel
        } else {
            return selectedTags.map { $0.displayName }.sorted(by: <).joined(separator: ", ")
        }
    }
    
    private var predicate: NSPredicate? {
        if searchKeyword.isEmpty {
            return nil
        } else {
            return NSPredicate(format: "name CONTAINS[c] %@", searchKeyword)
            //            return NSPredicate(format: "name BEGINSWITH[c] %@", searchKeyword)
        }
    }
    
    func updateSelectedTags(with tag: Tag) {
        if selectedTags.contains(tag) {
            selectedTags.remove(tag)
        } else {
            selectedTags.appendAndSort(tag, comparator: { $0.displayName < $1.displayName})
        }
    }
}

struct TagFilter_Previews: PreviewProvider {
    static var previews: some View {
        
        NavigationView {
            TagFilter(selectedTags: .constant([Tag]()))
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
        }
    }
}
