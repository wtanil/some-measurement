//
//  TagHome.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 23/03/21.
//

import SwiftUI

struct TagHome: View {
    typealias Labels = Constant.TagHome
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @State private var selectedTag: Tag?
    @State private var showingTagEditorCreateSheet: Bool = false
    @State private var searchKeyword: String = ""
    
    private var sortDescriptor: NSSortDescriptor = NSSortDescriptor(keyPath: \Tag.name, ascending: true)
    
    var body: some View {
        
        VStack {
            TextField(Labels.searchPlaceholder, text: $searchKeyword)
                .disableAutocorrection(true)
            
            DynamicList(sortDescriptors: [sortDescriptor], predicate: predicate) { (tag: Tag) in
                Button(action: {
                    setSelectedTag(with: tag)
                }, label: {
                    Text(tag.displayName)
                })
                .accessibilityIdentifier("TagCell-\(tag.displayName)")
            }
            .navigationTitle(Labels.navigationBarTitle)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .sheet(item: $selectedTag, content: { tag in
                TagEditor(persistenceController: PersistenceController.shared, tag: tag)
                    .environment(\.managedObjectContext, self.viewContext)
            })
            .sheet(isPresented: $showingTagEditorCreateSheet, content: {
                TagEditor(persistenceController: PersistenceController.shared)
                    .environment(\.managedObjectContext, self.viewContext)
            })
        }
        .padding()
    }
    
    private var navigationBarTrailingItem: some View {
        Button(action: {
            showingTagEditorCreateSheet.toggle()
        }, label: {
            Image(systemName: "plus")
                .imageScale(.medium)
                .padding()
        })
        .accessibilityIdentifier("NewTagButton")
    }
}

extension TagHome {
    private var predicate: NSPredicate? {
        
        var predicates: [NSPredicate] = [NSPredicate]()
        
        if searchKeyword.isEmpty {
            return nil
        }
        
        if !searchKeyword.isEmpty {
            let searchPredicate = NSPredicate(format: "name CONTAINS[c] %@", searchKeyword)
            predicates.append(searchPredicate)
        }
        
        return NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
    }
    
    private func setSelectedTag(with tag: Tag) {
        self.selectedTag = tag
    }
}

struct TagHome_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            TagHome()
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
        }
    }
}
