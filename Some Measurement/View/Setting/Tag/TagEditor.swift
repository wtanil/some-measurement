//
//  TagEditor.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 23/03/21.
//

import SwiftUI
import CoreData

struct TagEditor: View {
    typealias Labels = Constant.TagEditor
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var tag: Tag
    @State private var alertMode: AlertMode?
    
    let persistenceController: PersistenceController
    let childContext: NSManagedObjectContext
    private var mode: Mode = .create
    private var oldName: String = ""
    
    init(persistenceController: PersistenceController, tag: Tag? = nil) {
        self.persistenceController = persistenceController
        self.childContext = persistenceController.childViewContext()
        
        if let tag = tag {
            self.tag = persistenceController.copyForEditing(of: tag, in: childContext)
            
            self.mode = .edit
            self.oldName = tag.displayName
        } else {
            self.tag = persistenceController.newTemporaryInstance(in: childContext)
            
            self.mode = .create
        }
    }
    
    var body: some View {
        NavigationView {
            Form {
                // Workaround - Embeded HStack to fix TextField value not showing when the view appears
                HStack {
                    TextField(Labels.nameTextField, text: $tag.name.withDefaultValue(""))
                        .accessibilityIdentifier("NameTextField")
                    
                    if !tag.isNameValid {
                        Image(systemName: "exclamationmark.circle")
                            .foregroundColor(.red)
                            .accessibilityIdentifier("NameValidationImage")
                    }
                }
            }
            .navigationBarTitle(navigationBarTitle)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .alert(item: $alertMode) { mode in
                switch mode.id {
                case .delete:
                    return deleteAlert
                case .save:
                    return saveAlert
                }
            }
        }
    }
    
    private var navigationBarTrailingItem: some View {
        HStack(spacing: 0) {
            if mode == .edit {
                Button(action: {
                    self.alertMode = .delete
                }, label: {
                    Image(systemName: "trash")
                        .foregroundColor(.red)
                        .imageScale(.medium)
                        .padding(5)
                })
                .accessibilityIdentifier("DeleteButton")
            }
            
            Button(action: {
                self.save()
            }, label: {
                Text(saveButtonLabel)
                    .padding(5)
            })
                .disabled(!tag.isNameValid)
                .accessibilityIdentifier("SaveButton")
        }
    }
    
    private var saveAlert: Alert {
        Alert(title: Text(Labels.saveAlertTitle), message: Text(Labels.saveAlertMessage), dismissButton: .default(Text(Labels.saveAlertConfirmLabel)))
    }
    
    private var deleteAlert: Alert {
        Alert(title: Text(Labels.deleteAlertTitle),
              message: Text(Labels.deleteAlertMessage),
              primaryButton: .destructive(Text(Labels.deleteAlertConfirmLabel), action: deleteTag),
              secondaryButton: .default(Text(Labels.deleteAlertCancelLabel)))
    }
}

extension TagEditor {
    
    private var navigationBarTitle: String {
        switch mode {
        case .create:
            return Labels.navigationBarAddTitle
        case .edit:
            return Labels.navigationBarEditTitle
        }
    }
    
    private var saveButtonLabel: String {
        switch mode {
        case .create:
            return Labels.saveAddButtonLabel
        case .edit:
            return Labels.saveEditButtonLabel
        }
    }
    
    func deleteTag() {
        persistenceController.delete(tag, in: childContext)
        persistenceController.persist(for: childContext)
        
        presentationMode.wrappedValue.dismiss()
    }
    
    var shouldSave: Bool {
        if tag.isUnique(in: childContext) {
            return true
        } else {
            return false
        }
    }
    
    func save() {
        if shouldSave {
            persistenceController.persist(tag)
            presentationMode.wrappedValue.dismiss()
        } else {
            alertMode = .save
        }

//        switch mode {
//        case .create:
//            if tag.isUnique(in: childContext) {
//                persistenceController.persist(tag)
//                presentationMode.wrappedValue.dismiss()
//            } else {
//                alertMode = .save
//            }
//        case .edit:
//            if tag.displayName == oldName || tag.isUnique(in: childContext) {
//                persistenceController.persist(tag)
//
//                presentationMode.wrappedValue.dismiss()
//            } else {
//                alertMode = .save
//            }
//        }
    }
}

struct TagEditor_Previews: PreviewProvider {
    static var previews: some View {
        let previewContext = PersistenceController.shared.container.viewContext
        
        // create
        TagEditor(persistenceController: PersistenceController.shared)
        
        // edit
        TagEditor(persistenceController: PersistenceController.shared, tag: Tag.placeholder(in: previewContext))
    }
}
