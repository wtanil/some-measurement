//
//  UnitEditor.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 24/03/21.
//

import SwiftUI
import CoreData

struct UnitEditor: View {
    typealias Labels = Constant.UnitEditor
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var unit: RecordUnit
    @State private var alertMode: AlertMode?
    
    private let persistenceController: PersistenceController
    private let childContext: NSManagedObjectContext
    private var mode: Mode = .create
    
    init(persistenceController: PersistenceController, unit: RecordUnit? = nil) {
        self.persistenceController = persistenceController
        self.childContext = persistenceController.childViewContext()
        
        if let unit = unit {
            self.unit = persistenceController.copyForEditing(of: unit, in: childContext)
            
            self.mode = .edit
        } else {
            self.unit = persistenceController.newTemporaryInstance(in: childContext)
            
            self.mode = .create
        }
    }
    
    var body: some View {
        NavigationView {
            Form {
                // Workaround - Embeded HStack to fix TextField value not showing when the view appears
                HStack {
                    TextField(Labels.nameTextField, text: $unit.title.withDefaultValue(""))
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .accessibilityIdentifier("TitleTextField")
                    
                    if !unit.isTitleValid {
                        Image(systemName: "exclamationmark.circle")
                            .foregroundColor(.red)
                            .accessibilityIdentifier("TitleValidationImage")
                    }
                }
            }
            .navigationBarTitle(navigationBarTitle)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .alert(item: $alertMode) { mode in
                switch mode.id {
                case .delete:
                    return deleteAlert
                case .save:
                    return saveAlert
                }
            }
        }
    }
    
    private var navigationBarTrailingItem: some View {
        HStack(spacing: 0) {
            if mode == .edit {
                Button(action: {
                    // notify user about the risk of removing a unit
                    // all occurrences will be replaced to the unit 'none'
                    // it might take some times depending on how many occurrences
                    self.alertMode = .delete
                }, label: {
                    Image(systemName: "trash")
                        .foregroundColor(.red)
                        .imageScale(.medium)
                        .padding(5)
                })
                    .accessibilityIdentifier("DeleteButton")
            }
            
            Button(action: {
                self.save()
            }, label: {
                Text(saveButtonLabel)
                    .padding(5)
            })
                .disabled(!unit.isTitleValid)
                .accessibilityIdentifier("SaveButton")
        }
    }
    
    private var saveAlert: Alert {
        Alert(title: Text(Labels.saveAlertTitle), message: Text(Labels.saveAlertMessage), dismissButton: .default(Text(Labels.saveAlertConfirmLabel)))
    }
    
    private var deleteAlert: Alert {
        Alert(title: Text(Labels.deleteAlertTitle),
              message: Text(Labels.deleteAlertMessage),
              primaryButton: .destructive(Text(Labels.deleteAlertConfirmLabel), action: replaceAndDeleteUnit),
              secondaryButton: .default(Text(Labels.deleteAlertCancelLabel)))
    }
}

extension UnitEditor {
    
    private var navigationBarTitle: String {
        switch mode {
        case .create:
            return Labels.navigationBarAddTitle
        case .edit:
            return Labels.navigationBarEditTitle
        }
    }
    
    private var saveButtonLabel: String {
        switch mode {
        case .create:
            return Labels.saveAddButtonLabel
        case .edit:
            return Labels.saveEditButtonLabel
        }
    }
    
    var shouldSave: Bool {
        if unit.isUnique(in: childContext) {
            return true
        } else {
            return false
        }
    }
    
    private func save() {
        if shouldSave {
            persistenceController.persist(unit)
            presentationMode.wrappedValue.dismiss()
        } else {
            alertMode = .save
        }
    }
    
    
    func replaceAndDeleteUnit() {
        replaceUnit()
        deleteUnit()
        persistenceController.persist(for: childContext)
        presentationMode.wrappedValue.dismiss()
    }
    
    func replaceUnit() {
        let replacedRecordCount = Record.replaceAllDefaultUnits(from: unit, to: RecordUnit.getUnitNone(in: childContext), in: childContext)
        print("replaced record count: \(replacedRecordCount)")
        
        let replacedValueCount = Value.replaceAllUnits(from: unit, to: RecordUnit.getUnitNone(in: childContext), in: childContext)
        print("replaced value count: \(replacedValueCount)")
    }
    
    func deleteUnit() {
        persistenceController.delete(unit, in: childContext)
    }
    
//    func deleteUnit() {
//        let replacedRecordCount = Record.replaceAllDefaultUnits(from: unit, to: RecordUnit.getUnitNone(in: childContext), in: childContext)
//        print("replaced record count: \(replacedRecordCount)")
//
//        let replacedValueCount = Value.replaceAllUnits(from: unit, to: RecordUnit.getUnitNone(in: childContext), in: childContext)
//        print("replaced value count: \(replacedValueCount)")
//
//        persistenceController.delete(unit, in: childContext)
//        persistenceController.persist(for: childContext)
//        presentationMode.wrappedValue.dismiss()
//    }
}

struct UnitEditor_Previews: PreviewProvider {
    static var previews: some View {
        
        // create
        UnitEditor(persistenceController: PersistenceController.shared)
        
        // edit
        UnitEditor(persistenceController: PersistenceController.shared, unit: RecordUnit.placeholder(in: PersistenceController.shared.container.viewContext))
    }
}
