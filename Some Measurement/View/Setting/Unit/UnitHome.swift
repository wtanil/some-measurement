//
//  UnitHome.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 24/03/21.
//

import SwiftUI

struct UnitHome: View {
    typealias Labels = Constant.UnitHome
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @State private var selectedUnit: RecordUnit?
    @State private var showingUnitEditorSheet: Bool = false
    @State private var showingUnitEditorCreateSheet: Bool = false
    @State private var searchKeyword: String = ""
    
    private var sortDescriptor: NSSortDescriptor = NSSortDescriptor(keyPath: \RecordUnit.title, ascending: true)
    
    var body: some View {
        
        VStack {
            
            TextField(Labels.searchPlaceholder, text: $searchKeyword)
                .disableAutocorrection(true)
            
            DynamicList(sortDescriptors: [sortDescriptor], predicate: predicate) { (unit: RecordUnit) in
                
                Button(action: {
                    setSelectedUnit(with: unit)
                }, label: {
                    Text(unit.displayListTitle)
                })
                    .disabled(unit.isNone)
                    .accessibilityIdentifier("UnitCell-\(unit.displayListTitle)")
                
            }
            .navigationTitle(Labels.navigationBarTitle)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .sheet(item: $selectedUnit) { unit in
                UnitEditor(persistenceController: PersistenceController.shared, unit: unit)
                    .environment(\.managedObjectContext, self.viewContext)
            }
            .sheet(isPresented: $showingUnitEditorCreateSheet, content: {
                UnitEditor(persistenceController: PersistenceController.shared)
                    .environment(\.managedObjectContext, self.viewContext)
            })
        }
        .padding()
    }
    
    private var navigationBarTrailingItem: some View {
        Button(action: {
            self.showingUnitEditorCreateSheet.toggle()
        }, label: {
            Image(systemName: "plus")
                .imageScale(.medium)
                .padding()
        })
            .accessibilityIdentifier("NewUnitButton")
    }
}

extension UnitHome {
    private var predicate: NSPredicate? {
        var predicates: [NSPredicate] = [NSPredicate]()
        
        if searchKeyword.isEmpty {
            return nil
        }
        
        if !searchKeyword.isEmpty {
            let searchPredicate = NSPredicate(format: "title CONTAINS[c] %@", searchKeyword)
            predicates.append(searchPredicate)
        }
        
        return NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
    }
    
    private func setSelectedUnit(with unit: RecordUnit) {
        self.selectedUnit = unit
    }
}

struct UnitHome_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            UnitHome()
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
        }
    }
}
