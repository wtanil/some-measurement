//
//  UnitSelect.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 26/03/21.
//

import SwiftUI

struct UnitSelect: View {
    typealias Labels = Constant.UnitSelect
    
    @Binding var selectedUnit: RecordUnit
    @State private var searchKeyword: String = ""
    
    private let sortDescriptor: NSSortDescriptor = NSSortDescriptor(keyPath: \RecordUnit.title, ascending: true)
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text(selectedLabel)
                .accessibilityIdentifier("SelectedUnitText")
            
            TextField(Labels.searchTextField, text: $searchKeyword)
                .disableAutocorrection(true)
                .accessibilityIdentifier("SearchTextField")
            
            DynamicList(sortDescriptors: [sortDescriptor], predicate: predicate) { (unit: RecordUnit) in
                Button(action: {
                    setSelectedUnit(with: unit)
                }, label: {
                    HStack {
                        Text(unit.displayListTitle).layoutPriority(1)
                        
                        if selectedUnit == unit {
                            Spacer(minLength: 5)
                            Image(systemName: "checkmark")
                                .layoutPriority(1)
                                .accessibilityIdentifier("CheckmarkImage")
                        }
                    }
                })
                .accessibilityIdentifier("UnitCell-\(unit.displayListTitle)")
            }
        }
        .padding()
        .navigationTitle(Labels.navigationBarTitle)
        .navigationBarTitleDisplayMode(.inline)
    }
}

extension UnitSelect {
    
    private var selectedLabel: String {
        return selectedUnit.displayListTitle
    }
    
    private var predicate: NSPredicate? {
        if searchKeyword.isEmpty {
            return nil
        } else {
            return NSPredicate(format: "title CONTAINS[c] %@", searchKeyword)
            //            return NSPredicate(format: "name BEGINSWITH[c] %@", searchKeyword)
        }
    }
    
    private func setSelectedUnit(with unit: RecordUnit) {
        self.selectedUnit = unit
    }
}

struct UnitSelect_Previews: PreviewProvider {
    static var previews: some View {
        let viewContext = PersistenceController.preview.container.viewContext
        
        NavigationView {
            UnitSelect(selectedUnit: .constant (RecordUnit.placeholder(in: viewContext)))
                .environment(\.managedObjectContext, viewContext)
        }
    }
}
