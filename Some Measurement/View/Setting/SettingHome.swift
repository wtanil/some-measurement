//
//  SettingHome.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 23/03/21.
//

import SwiftUI

struct SettingHome: View {
    typealias Labels = Constant.SettingHome
    
    var body: some View {
        NavigationView {
            List {
                NavigationLink(
                    destination: TagHome(),
                    label: {
                        Text(Labels.manageTagsLabel)
                    })
                    .accessibilityIdentifier("ManageTagsCell")
                
                NavigationLink(
                    destination: UnitHome(),
                    label: {
                        Text(Labels.manageUnitsLabel)
                    })
                    .accessibilityIdentifier("ManageUnitsCell")
            }
            .listStyle(PlainListStyle())
            .navigationTitle(Labels.navigationBarTitle)
        }
    }
}

struct SettingHome_Previews: PreviewProvider {
    static var previews: some View {
        SettingHome()
    }
}
