//
//  DynamicList.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 22/01/21.
//

import SwiftUI
import CoreData

struct DynamicList<T: NSManagedObject, Content: View>: View {
    
    let content: (T) -> Content
    var fetchRequest: FetchRequest<T>
    
    init(sortDescriptors: [NSSortDescriptor], @ViewBuilder content: @escaping(T) -> Content) {
        fetchRequest = FetchRequest<T>(entity: T.entity(), sortDescriptors: sortDescriptors)
        self.content = content
    }
    
    init(sortDescriptors: [NSSortDescriptor], predicate: NSPredicate?, @ViewBuilder content: @escaping(T) -> Content) {
        fetchRequest = FetchRequest<T>(entity: T.entity(), sortDescriptors: sortDescriptors, predicate: predicate)
        self.content = content
    }
    
    var body: some View {
        List(fetchRequest.wrappedValue, id: \.self) { item in
            self.content(item)
        }
        .listStyle(PlainListStyle())
        
    }
}


/*
 
 init(filterKey: String, filterValue: String, sortDescriptors: [NSSortDescriptor], @ViewBuilder content: @escaping (T) -> Content) {
 fetchRequest = FetchRequest<T>(entity: T.entity(), sortDescriptors: sortDescriptors, predicate: NSPredicate(format: "%K BEGINSWITH %@", filterKey, filterValue))
 self.content = content
 }
 
 init(filterKey: String, filterValue: String, predicateOption: PredicateOptions, sortDescriptors: [NSSortDescriptor], @ViewBuilder content: @escaping (T) -> Content) {
 var predicateFormat = ""
 switch predicateOption {
 case .beginsWith:
 predicateFormat = "%K BEGINSWITH %@"
 case .equalsTo:
 predicateFormat = "%K = %@"
 }
 
 fetchRequest = FetchRequest<T>(entity: T.entity(), sortDescriptors: sortDescriptors, predicate: NSPredicate(format: predicateFormat, filterKey, filterValue))
 
 self.content = content
 }
 */
