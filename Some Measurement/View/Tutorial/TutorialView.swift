//
//  TutorialView.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 22/06/21.
//

import SwiftUI

struct TutorialView: View {
    
    typealias Labels = Constant.TutorialView
    
    init() {
        UIPageControl.appearance().currentPageIndicatorTintColor = .red
        UIPageControl.appearance().pageIndicatorTintColor = UIColor.red.withAlphaComponent(0.5)
    }
    
    var body: some View {
        TabView {
            /*
             1. how to add unit and tag
             2. how to add measurement
             3. how to use quick update and visual indicator
             4. thank you page
             */
            firstPage
            secondPage
            thirdPage
            fourthPage
        }
        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
    }
}

extension TutorialView {
    private var firstPage: some View {
        VStack {
            
            Spacer()
            
            Text("Hi, thanks for downloading Some Measurement")
                .font(.title)
                .bold()
                .multilineTextAlignment(.center)
                .padding()
                .accessibilityIdentifier("WelcomeText")
            
            Text("Swipe to go through a short tutorial")
            Spacer()
            
        }
    }
    
    private var secondPage: some View {
        // create unit
        //        VStack {
        //            Text("How to add a new unit")
        //            Text("1. Go to Setting")
        //            Text("Image")
        //            Text("2. Go to Manage Units")
        //            Text("Image")
        //            Text("3. Tap on the Plus button")
        //            Text("Image")
        //        }
        HStack {
            VStack(alignment: .leading) {
                Text("How to add a new unit")
                    .font(.title)
                    .bold()
                    .padding()
                
                Text("1. Go to Setting")
                Image("createunit-tab")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(Color.gray, lineWidth: 1)
                    )
                
                Text("2. Go to Manage units")
                Image("createunit-manage")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(Color.gray, lineWidth: 1)
                    )
                
                Text("3. Tap on the Plus button")
                Image("createunit-home")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(Color.gray, lineWidth: 1)
                    )
                
                Spacer()
            }
            Spacer()
        }
        .padding()
    }
    
    private var thirdPage: some View {
        VStack {
            //            Text("How to add a new measurement")
            //            Text("1. Tap on Plus button on Home Screen")
            //            Text("Image")
            HStack {
                VStack(alignment: .leading) {
                    Text("How to add a new measurement")
                        .font(.title)
                        .bold()
                        .padding()
                    
                    Text("1. Tap on Plus button on Home Screen")
                    Image("createmeasurement-home")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.gray, lineWidth: 1)
                        )
                    
                    Spacer()
                }
                Spacer()
            }
            .padding()
        }
        
    }
    
    private var fourthPage: some View {
        VStack {
            //            Text("Quick update")
            //            Text("You can enable quick update by going to the measurement edit screen")
            //            Text("Image")
            //            Text("When you see symbol, you can long press on the measurement to quickly update the value")
            //            Text("1. Tap on Plus button on Home Screen")
            //            Text("Image")
            HStack {
                VStack(alignment: .leading) {
                    Text("Quick update")
                        .font(.title)
                        .bold()
                        .padding()
                    
                    Text("Quick update functionality allows a measurement to be updated using a predetermined value. Quick update is disabled by default and can be enabled when creating and editing the measurement.")
                    
                    Text("1. When quick update is enabled, a \(Image(systemName: "arrow.up.arrow.down")) icon is shown.")
                    Image("quickupdate-home")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.gray, lineWidth: 1)
                        )
                    Text("2. Long press on the measurement to update the value ")
                    Image("quickupdate-menu")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.gray, lineWidth: 1)
                        )
                    
                    Spacer()
                }
                Spacer()
            }
            .padding()
        }
    }
}

struct TutorialView_Previews: PreviewProvider {
    static var previews: some View {
        TutorialView()
    }
}
