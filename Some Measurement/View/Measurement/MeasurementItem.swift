//
//  MeasurementItem.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 12/03/21.
//

import SwiftUI

struct MeasurementItem: View {
    
    let title: String
    let date: String
    let value: String
    let shouldQuickUpdate: Bool
    
    var body: some View {
        HStack(alignment: .center) {
            VStack(alignment: .leading) {
                HStack(spacing: 4) {
                    Text(title)
                        .accessibilityIdentifier("RecordCellTitleText")
                    if shouldQuickUpdate {
                        Image(systemName: "arrow.up.arrow.down")
                            .imageScale(.small)
                            .foregroundColor(.green)
                            .accessibilityIdentifier("QUIImage")
                    }
                }
                Text(date)
                    .font(.caption)
                    .foregroundColor(.secondary)
            }
            
            Spacer()
            
            Text(value)
                .font(.title2)
                .accessibilityIdentifier("ValueText")
        }
    }
}

struct MeasurementItem_Previews: PreviewProvider {
    static var previews: some View {
        MeasurementItem(title: "Test measurement", date: "January 1, 1991 at 10:00:00", value: "100 m", shouldQuickUpdate: false)
            .previewLayout(.sizeThatFits)
        
        MeasurementItem(title: "Test measurement", date: "January 1, 1991 at 10:00:00", value: "100 m", shouldQuickUpdate: true)
            .previewLayout(.sizeThatFits)
    }
}
