//
//  MeasurementControl.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 12/03/21.
//

import SwiftUI

struct MeasurementControl: View {
    typealias Labels = Constant.MeasurementControl
    
    @Binding var sortOption: Record.SortOption
    @Binding var selectedTags: [Tag]
    @Binding var searchKeyword: String
    
    @State private var showingSearch: Bool = false
    
    var body: some View {
        
        HStack {
            
            HStack {
                Button(action: {
                    withAnimation {
                        self.showingSearch.toggle()
                    }
                }) {
                    Image(systemName: showingSearch ? "chevron.left" : "magnifyingglass")
                        .modifier(Bold(shouldBold: !searchKeyword.isEmpty))
                    //                        .padding(9)
                }
                .accessibilityIdentifier("SearchToggleButton")
                
                if showingSearch {
                    TextField(Labels.searchPlaceholder, text: $searchKeyword)
                        .disableAutocorrection(true)
                        .accessibilityIdentifier("SearchTextField")
                    
                    Button(action: {
                        setSearchKeyword("")
                    }, label: {
                        Image(systemName: "xmark.circle")
                    })
                        .accessibilityIdentifier("SearchClearButton")
                }
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                
                HStack {
                    
                    NavigationLink(sortLabel, destination: MeasurementSort(sortOption: $sortOption))
                        .accessibilityIdentifier("SortNavLink")
                    
                    NavigationLink(filterLabel, destination: TagFilter(selectedTags: $selectedTags))
                        .accessibilityIdentifier("FilterNavLink")
                    
                    // FIX for unexpected pop after changing parent state from inside
                    // NavigationLink causing parent view to refresh and remove NavigationLink view to pop, .hidden to prevent "Unable to present. Please file a bug" bug
                    NavigationLink("", destination: Text(""))
                        .hidden()
                    
                }
                //                .padding()
            }
        }
        .padding()
    }
}

extension MeasurementControl {
    
    private var sortLabel: String {
        return Labels.sortButtonLabel + sortOption.rawValue
    }
    
    private var filterLabel: String {
        if selectedTags.isEmpty {
            return Labels.emptySelectedLabel
        } else {
            return "\(Labels.notEmptySelectedLabel)\(selectedTags.map { $0.displayName }.joined(separator: ", "))"
        }
    }
    
    private func setSearchKeyword(_ keyword: String) {
        self.searchKeyword = keyword
    }
}

struct MeasurementControl_Previews: PreviewProvider {
    static var previews: some View {
        MeasurementControl(sortOption: .constant(Record.SortOption.latest), selectedTags: .constant([Tag]()), searchKeyword: .constant("constant string")
        )
            .previewLayout(PreviewLayout.sizeThatFits)
        
        MeasurementControl(sortOption: .constant(Record.SortOption.latest), selectedTags: .constant([Tag]()), searchKeyword: .constant("")
        )
            .previewLayout(PreviewLayout.sizeThatFits)
    }
}
