//
//  MeasurementSort.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 12/03/21.
//

import SwiftUI

struct MeasurementSort: View {
    
    typealias Labels = Constant.MeasurementSort
    
    @Binding var sortOption: Record.SortOption
    
    var body: some View {
        List {
            ForEach(Record.SortOption.allCases) { option in
                Button(action: {
                    setOption(sortOption: option)
                }, label: {
                    HStack {
                        Text(option.rawValue).layoutPriority(1)
                        
                        if self.sortOption == option {
                            Spacer(minLength: 5)
                            Image(systemName: "checkmark")
                                .layoutPriority(1)
                                .accessibilityIdentifier("CheckmarkImage")
                        }
                    }
                })
                .accessibilityIdentifier("SortCell-\(option.rawValue)")
            }
        }
        .navigationTitle(Labels.navigationTitle)
    }
}

extension MeasurementSort {
    private func setOption(sortOption: Record.SortOption) {
        self.sortOption = sortOption
    }
}

struct MeasurementSort_Previews: PreviewProvider {
    static var previews: some View {
        MeasurementSort(sortOption: .constant(Record.SortOption.title))
    }
}
