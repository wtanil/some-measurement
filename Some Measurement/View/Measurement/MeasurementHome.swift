//
//  MeasurementHome.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 12/03/21.
//

import SwiftUI
import CoreData

struct MeasurementHome: View {
    typealias Labels = Constant.MeasurementHome
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @State private var showingMeasurementEditorSheet: Bool = false
    @State private var selectedMeasurement: Record?
    @State private var sortOption: Record.SortOption = .title
    @State private var selectedTags: [Tag] = [Tag]()
    @State private var searchKeyword: String = ""
    
    private let persistenceController: PersistenceController
    
    init(persistenceController: PersistenceController) {
        self.persistenceController = persistenceController
    }
    
    var body: some View {
        NavigationView {
            VStack {
                
                MeasurementControl(sortOption: $sortOption, selectedTags: $selectedTags, searchKeyword: $searchKeyword)
                
                DynamicList(sortDescriptors: [sortDescriptor], predicate: predicate) { (measurement: Record) in
                    
                    NavigationLink(
                        destination: MeasurementDetail(persistenceController: PersistenceController.shared, measurement: measurement)) {
                            MeasurementItem(title: measurement.displayTitle,
                                            date: lastValueDateString(for: measurement),
                                            value: lastValueString(for: measurement),
                                            shouldQuickUpdate: shouldQuickUpdate(for: measurement))
                        }
                        .contextMenu {
                            listContextMenu(measurement: measurement)
                        }
                    // bug fix for context menu not updating
                        .id("\(measurement.objectID)\(measurement.quickUpdateInterval)")
//                        .id("\(measurement.objectID)")
                        .accessibilityIdentifier("RecordCell-\(measurement.displayTitle)")
                }
                // Implement pull-to-refresh ONLY for iOS 15
                //                .modifier(customRefreshable(action: {
                //                    viewContext.refreshAllObjects()
                //                }))
            }
            .navigationTitle(Labels.navigationTitle)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .sheet(isPresented: $showingMeasurementEditorSheet, content: {
                MeasurementEditor(persistenceController: PersistenceController.shared, validator: Validator())
            })
            .sheet(item: $selectedMeasurement) { measurement in
                QUIEditor(persistenceController: PersistenceController.shared, measurement: measurement, validator: Validator())
            }
            .onAppear {
                viewContext.refreshAllObjects()
            }
        }
    }
    
    private var navigationBarTrailingItem: some View {
        Button(action: {
            showingMeasurementEditorSheet.toggle()
        }, label: {
            Image(systemName: "plus")
                .imageScale(.medium)
                .padding()
        })
            .accessibilityIdentifier("NewRecordButton")
    }
    
    private func listContextMenu(measurement: Record) -> some View {
        let quickUpdateIntervalString = quickUpdateIntervalString(for: measurement)
        let lastValueUnitString = lastValueUnitString(for: measurement)
        
        return VStack {
            if shouldQuickUpdate(for: measurement) {
                Button {
                    contextMenuButtonAction(for: measurement, mode: .increment)
                } label: {
                    let string = Labels.quickUpdateIncreaseLabel + quickUpdateIntervalString + " " + lastValueUnitString
                    Label(string, systemImage: "arrow.up")
                }
                .accessibilityIdentifier("IncreaseContextButton")
                .disabled(measurement.quickUpdateInterval == 0)
                
                Button {
                    contextMenuButtonAction(for: measurement, mode: .decrement)
                } label: {
                    let string = Labels.quickUpdateDecreaseLabel + quickUpdateIntervalString + " " + lastValueUnitString
                    Label(string, systemImage: "arrow.down")
                }
                .accessibilityIdentifier("DecreaseContextButton")
                .disabled(measurement.quickUpdateInterval == 0)
                
                Divider()
            }
            Button {
                self.selectedMeasurement = measurement
            } label: {
                let string = "Edit interval"
                Label(string, systemImage: "pencil")
            }
            .accessibilityIdentifier("EditContextButton")
            
        }
    }
}

extension MeasurementHome {
    
    private var sortDescriptor: NSSortDescriptor {
        
        switch sortOption {
        case .latest:
            return NSSortDescriptor(keyPath: \Record.createDate, ascending: false)
        case .oldest:
            return NSSortDescriptor(keyPath: \Record.createDate, ascending: true)
        case .lastUpdate:
            return NSSortDescriptor(keyPath: \Record.updateDate, ascending: false)
        default:
            return NSSortDescriptor(keyPath: \Record.title, ascending: true)
        }
    }
    
    private var predicate: NSPredicate? {
        
        var predicates: [NSPredicate] = [NSPredicate]()
        
        if selectedTags.isEmpty && searchKeyword.isEmpty {
            return nil
        }
        
        if !selectedTags.isEmpty {
            for tag in selectedTags {
                let tagPredicate = NSPredicate(format: "tags CONTAINS %@", tag)
                predicates.append(tagPredicate)
            }
        }
        
        if !searchKeyword.isEmpty {
            let searchPredicate = NSPredicate(format: "title CONTAINS[c] %@", searchKeyword)
            predicates.append(searchPredicate)
        }
        
        return NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
    }
    
    func shouldQuickUpdate(for measurement: Record) -> Bool {
        measurement.quickUpdateInterval != 0
    }
    
    func lastValueString(for measurement: Record) -> String {
        guard let lastValue = measurement.lastValue else {
            return Labels.lastValueNone
        }
        
        return lastValue.displayValueWithUnit
    }
    
    func lastValueDateString(for measurement: Record) -> String {
        guard let lastValue = measurement.lastValue else {
            return Labels.lastValueDateNone
        }
        
        return lastValue.displayDate
    }
    
    func quickUpdateIntervalString(for measurement: Record) -> String {
        return measurement.displayQuickUpdateInterval
    }
    
    func lastValueUnitString(for measurement: Record) -> String {
        var string = ""
        if let lastValue = measurement.lastValue, let valueUnit = lastValue.unit {
            string = valueUnit.displayTitle
        }
        return string
    }
    
    func contextMenuButtonAction(for measurement: Record, mode: ContextMenuMode) {
        
        // to fix bug context menu not updating
        if measurement.quickUpdateInterval == 0 {
            return
        }
        
        var lastAmount: Double = 0
//        var lastValueUnit: RecordUnit = RecordUnit.getUnitNone(in: measurement.managedObjectContext!)
        var lastValueUnit: RecordUnit = measurement.defaultUnit ?? RecordUnit.getUnitNone(in: measurement.managedObjectContext!)
        
        if let lastValue = measurement.lastValue {
            lastAmount = lastValue.value
            if let testUnit = lastValue.unit {
                lastValueUnit = testUnit
            }
        }
        
        var newAmount: Double = 0
        
        switch mode {
        case .increment:
            newAmount = lastAmount + measurement.quickUpdateInterval
        case .decrement:
            newAmount = lastAmount - measurement.quickUpdateInterval
        }
        
        _ = createValue(measurement: measurement, amount: newAmount, unit: lastValueUnit)
    }
    
    func createValue(measurement: Record, amount: Double, unit: RecordUnit) -> Value {
        
//        let newValue: Value = Value(context: measurement.managedObjectContext!)
        let entityDescription = NSEntityDescription.entity(forEntityName: "Value", in: measurement.managedObjectContext!)
        let newValue = Value(entity: entityDescription!, insertInto: measurement.managedObjectContext!)
        newValue.set(amount: amount, date: Date(), unit: unit)
        
        measurement.addToValues(newValue)
        persistenceController.persist(newValue)
        
        return newValue
    }
}

struct MeasurementHome_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            MeasurementHome(persistenceController: PersistenceController.preview)
                .preferredColorScheme(.dark)
        }
    }
}
