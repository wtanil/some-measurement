//
//  QUIEditor.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 22/04/22.
//

import SwiftUI
import CoreData

struct QUIEditor: View {
    typealias Labels = Constant.MeasurementEditor
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var measurement: Record
    @State private var quickUpdateIntervalString: String
    
    private let persistenceController: PersistenceController
    private let childContext: NSManagedObjectContext
    private let validator: Validator
    
    init(persistenceController: PersistenceController, measurement: Record, validator: Validator) {
        self.persistenceController = persistenceController
        self.childContext = persistenceController.childViewContext()
        self.validator = validator
        
        self.measurement = persistenceController.copyForEditing(of: measurement, in: childContext)
        _quickUpdateIntervalString = State(initialValue: measurement.displayQuickUpdateInterval)
        
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section(
                    header: Text(Labels.quickUpdateIntervalHeader),
                    footer: Text(Labels.quickUpdateIntervalFooter))
                {
                    HStack {
                        TextField(Labels.quickUpdateIntervalTextField, text: $quickUpdateIntervalString)
                            .onChange(of: quickUpdateIntervalString) { value in
                                setQUI(value)
                            }
                            .keyboardType(.decimalPad)
                            .accessibilityIdentifier("QUITextField")
                        
                        if !isQuickUpdateIntervalValid {
                            Image(systemName: "exclamationmark.circle")
                                .foregroundColor(.red)
                                .accessibilityIdentifier("CheckmarkImage")
                        }
                    }
                }
            }
            .navigationTitle(navigationTitle)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: navigationBarTrailingItem)
        }
    }
    
    private var navigationTitle: String {
        return Labels.navigationBarEditTitle
    }
    
    private var saveButtonLabel: String {
        return Labels.saveEditButtonLabel
    }
    
    private var navigationBarTrailingItem: some View {
        Button(action: {
            save()
        }, label: {
            Text(saveButtonLabel)
        })
        .disabled(!isFormValid)
        .padding()
        .accessibilityIdentifier("SaveButton")
    }
    
    var isFormValid: Bool {
        return isQuickUpdateIntervalValid
    }
    
    func setQUI(_ string: String) {
        if isQuickUpdateIntervalValid {
            guard let nsNumberQuickUpdateInterval = NumberFormatter.numberFormatterDecimal.number(from: string) else { return }
            measurement.quickUpdateInterval = nsNumberQuickUpdateInterval.doubleValue
        }
    }
    
    var isQuickUpdateIntervalValid: Bool {
        return validator.validateStringIsDecimal(string: quickUpdateIntervalString)
    }
    
    var shouldSave: Bool {
        if measurement.isUnique(in: childContext) {
            return true
        } else {
            return false
        }
    }
    
    func save() {
        if shouldSave {
            persistenceController.persist(measurement)
            presentationMode.wrappedValue.dismiss()
        }
    }
}

struct QUIEditor_Previews: PreviewProvider {
    static var previews: some View {
        
        let persistenceController = PersistenceController.preview
        let viewContext = persistenceController.container.viewContext
        
        QUIEditor(persistenceController: persistenceController, measurement: Record.placeholder(in: viewContext), validator: Validator())
    }
}
