//
//  MeasurementDetail.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 15/03/21.
//

import SwiftUI

struct MeasurementDetail: View {
    typealias Labels = Constant.MeasurementDetail
    
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var measurement: Record
    @State private var showingDeleteAlert: Bool = false
    @State private var showingActionSheet: Bool = false
    @State private var showingValueInlineEditor: Bool = false
    @State private var sheetOption: SheetOptions? = nil
    
    private let persistenceController: PersistenceController

    private enum SheetOptions: Identifiable {
        case edit, history, newValue
        var id: Self { self }
    }
    
    init(persistenceController: PersistenceController, measurement: Record) {
        self.measurement = measurement
        self.persistenceController = persistenceController
    }
    
    var body: some View {
        GeometryReader() { geometry in
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading, spacing: 5) {
                    
                    Text(measurement.displayTitle)
                        .font(.title)
                        .accessibilityIdentifier("TitleText")
                    
                    Text(measurement.displayTags)
                        .font(.caption)
                        .accessibilityIdentifier("TagText")
                    
                    if !measurement.displayNote.isEmpty {
                        Text(measurement.displayNote)
                            .padding(.bottom, 10)
                            .padding(.top, 10)
                            .accessibilityIdentifier("NoteText")
                    }
                    
                    HStack {
                        Text("\(Labels.lastValueLabel)\(lastValue)")
                            .onTapGesture {
                                withAnimation {
                                    self.showingValueInlineEditor.toggle()
                                }
                            }
                            .accessibilityIdentifier("ValueDetailText")
                        
                        Spacer()
                        
                        Button(action: {
                            withAnimation {
                                self.showingValueInlineEditor.toggle()
                            }
                        }) {
                            Image(systemName: showingValueInlineEditor ? "chevron.up" : "plus")
                                .imageScale(.large)
                            //                                .rotationEffect(.degrees(showingValueInlineEditor ? 45 : 0))
                            //                                .scaleEffect(showingValueInlineEditor ? 1.5 : 1)
                                .padding()
                        }
                        .accessibilityIdentifier("ValueToggleButton")
                    }
                    valueInlineEditor
                }
                .padding()
            }
        }
        .navigationTitle(Labels.navigationTitle)
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing: navigationBarTrailingItem)
        .alert(isPresented: $showingDeleteAlert, content: { deleteAlert })
        .actionSheet(isPresented: $showingActionSheet, content: { actionSheet })
        .sheet(item: $sheetOption, content: { option in
            sheet(option: option)
        })
    }
    
    @ViewBuilder private var valueInlineEditor: some View {
        if showingValueInlineEditor {
            ValueInlineEditor(persistenceController: PersistenceController.shared, measurement: measurement, defaultUnit: measurement.defaultUnit!, validator: Validator())
        }
    }
    
    @ViewBuilder private func sheet(option: SheetOptions) -> some View {
        switch option {
        case .edit:
            MeasurementEditor(persistenceController: PersistenceController.shared, measurement: measurement, validator: Validator())
        case .history:
            ValueHistory(measurement: measurement)
                .environment(\.managedObjectContext, self.viewContext)
        case .newValue:
            ValueEditor(persistenceController: PersistenceController.shared, measurement: measurement, validator: Validator())
        }
    }
    
    private var navigationBarTrailingItem: some View {
        Button(action: {
            self.showingActionSheet.toggle()
        }, label: {
            Image(systemName: "ellipsis")
                .imageScale(.medium)
                .padding()
        })
        .accessibilityIdentifier("MoreBarButton")
    }
    
    private var deleteAlert: Alert {
        Alert(title: Text(Labels.deleteAlertTitle),
              message: Text(Labels.deleteAlertMessage),
              primaryButton: .destructive(Text(Labels.deleteAlertConfirmLabel),
                                          action: deleteMeasurement),
              secondaryButton: .default(Text(Labels.deleteAlertCancelLabel))
        )
    }
    
    private var actionSheet: ActionSheet {
        let actionButtons: [ActionSheet.Button] = [
            ActionSheet.Button.default(Text(Labels.actionSheetEditLabel), action: {
                self.sheetOption = .edit
            }),
            ActionSheet.Button.default(Text(Labels.actionSheetNewValueLabel), action: {
                self.sheetOption = .newValue
            }),
            .default(Text(Labels.actionSheetHistoryLabel), action: {
                self.sheetOption = .history
            }),
            .destructive(Text(Labels.actionSheetDeleteLabel), action: {
                self.showingDeleteAlert.toggle()
            }),
            .cancel()
        ]
        
        return ActionSheet(title: Text(Labels.actionSheetTitle), buttons: actionButtons)
    }
}

extension MeasurementDetail {
    var lastValue: String {
        guard let lastValue = measurement.lastValue else {
            return Labels.lastValueNone
        }
        
        return lastValue.displayValueWithUnit
    }
    
    func deleteMeasurement() {
        persistenceController.delete(measurement, in: viewContext)
        persistenceController.persist(for: viewContext)
        
        presentationMode.wrappedValue.dismiss()
    }
}

struct MeasurementDetail_Previews: PreviewProvider {
    static var previews: some View {
        let viewContext = PersistenceController.preview.container.viewContext
        NavigationView {
            MeasurementDetail(persistenceController: PersistenceController.preview, measurement: Record.placeholder(in: viewContext))
        }
    }
}
