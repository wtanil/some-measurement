//
//  MeasurementEditor.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 15/03/21.
//

import SwiftUI
import CoreData

struct MeasurementEditor: View {
    typealias Labels = Constant.MeasurementEditor
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var measurement: Record
    @State private var showingSaveAlert: Bool = false
    @State private var quickUpdateIntervalString: String
    
    private let persistenceController: PersistenceController
    private let childContext: NSManagedObjectContext
    private let unitNone: RecordUnit
    private var mode: Mode = .create
    private var validator: Validator
    
    init(persistenceController: PersistenceController, measurement: Record? = nil, validator: Validator) {
        self.persistenceController = persistenceController
        self.childContext = persistenceController.childViewContext()
        self.unitNone = RecordUnit.getUnitNone(in: childContext)
        self.validator = validator
        
        if let measurement = measurement {
            self.measurement = persistenceController.copyForEditing(of: measurement, in: childContext)
//            self.quickUpdateIntervalString = self.measurement.displayQuickUpdateInterval
            _quickUpdateIntervalString = State(initialValue: measurement.displayQuickUpdateInterval)
            
            self.mode = .edit
        } else {
            _quickUpdateIntervalString = State(initialValue: "0")
            self.measurement = persistenceController.newTemporaryInstance(in: childContext)
            self.measurement.defaultUnit = unitNone
            self.mode = .create
            
        }
        
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    HStack {
                        TextField(Labels.titleTextField, text: $measurement.title.withDefaultValue(""))
                            .accessibilityIdentifier("TitleTextField")
                        if !measurement.isTitleValid {
                            Image(systemName: "exclamationmark.circle")
                                .foregroundColor(.red)
                                .accessibilityIdentifier("TitleValidationImage")
                        }
                    }
                    TextField(Labels.noteTextField, text: $measurement.note.withDefaultValue(""))
                        .accessibilityIdentifier("NoteTextField")
                }
                
                Section(header: Text(Labels.defaultUnitSectionHeader)) {
                    NavigationLink(selectedUnitLabel, destination:
                                    UnitSelect(selectedUnit: $measurement.defaultUnit.withDefaultValue(unitNone))
                                    .environment(\.managedObjectContext, childContext)
                    )
                    .accessibilityIdentifier("SelectUnitButton")
                }
                
                Section(header: Text(Labels.tagsSectionHeader)) {
                    NavigationLink(selectTagLabel, destination:
                                    TagFilter(selectedTags: $measurement.computedTags)
                                    .environment(\.managedObjectContext, childContext)
                    )
                    .accessibilityIdentifier("SelectTagsButton")
                }
                
                Section(
                    header: Text(Labels.quickUpdateIntervalHeader),
                    footer: Text(mode == .create ? Labels.quickUpdateIntervalFooter : Labels.quickUpdateIntervalEditFooter))
                {
                    if mode == .create {
                        HStack {
                            TextField(Labels.quickUpdateIntervalTextField, text: $quickUpdateIntervalString)
                                .onChange(of: quickUpdateIntervalString) { value in
                                    setQUI(value)
                                }
                                .keyboardType(.decimalPad)
                                .accessibilityIdentifier("QUITextField")
                            
                            if !isQuickUpdateIntervalValid {
                                Image(systemName: "exclamationmark.circle")
                                    .foregroundColor(.red)
                                    .accessibilityIdentifier("QuiValidationImage")
                            }
                        }
                        .disabled(mode == .edit)
                    } else {
                        Text(measurement.displayQuickUpdateInterval)
                            .foregroundColor(Color.gray)
                            .accessibilityIdentifier("QUIText")
                    }
                    
                }
            }
            .navigationTitle(navigationTitle)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .alert(isPresented: $showingSaveAlert, content: { saveAlert })
        }
    }
    
    private var navigationBarTrailingItem: some View {
        Button(action: {
            save()
        }, label: {
            Text(saveButtonLabel)
        })
            .disabled(!isFormValid)
            .padding()
            .accessibilityIdentifier("SaveButton")
    }
    
    private var saveAlert: Alert {
        Alert(title: Text(Labels.saveAlertTitle), message: Text(Labels.saveAlertMessage), dismissButton: .default(Text(Labels.saveAlertConfirmLabel)))
    }
}

extension MeasurementEditor {
    private var navigationTitle: String {
        switch mode {
        case .create:
            return Labels.navigationBarAddTitle
        case .edit:
            return Labels.navigationBarEditTitle
        }
    }
    
    private var saveButtonLabel: String {
        switch mode {
        case .create:
            return Labels.saveAddButtonLabel
        case .edit:
            return Labels.saveEditButtonLabel
        }
    }
    
    private var selectTagLabel: String {
        if measurement.tagsAsArray.isEmpty {
            return Labels.emptySelectedTagsLabel
        } else {
//            return measurement.computedTags.map { $0.displayName }.joined(separator: ", ")
            return measurement.displayTags
        }
    }
    
    private var selectedUnitLabel: String {
        guard let defaultUnit = measurement.defaultUnit else {
            fatalError("Default unit can't be nil")
        }
        return defaultUnit.displayListTitle
    }
    
    func setQUI(_ string: String) {
        if isQuickUpdateIntervalValid {
            guard let nsNumberQuickUpdateInterval = NumberFormatter.numberFormatterDecimal.number(from: string) else { return }
            measurement.quickUpdateInterval = nsNumberQuickUpdateInterval.doubleValue
        }
    }
    
    var isQuickUpdateIntervalValid: Bool {
        return validator.validateStringIsDecimal(string: quickUpdateIntervalString)
    }
    
    var isFormValid: Bool {
        return measurement.isTitleValid && isQuickUpdateIntervalValid
    }
    
    var shouldSave: Bool {
        if measurement.isUnique(in: childContext) {
            return true
        } else {
            return false
        }
    }
    
    func save() {
        if shouldSave {
            persistenceController.persist(measurement)
            presentationMode.wrappedValue.dismiss()
        } else {
            showingSaveAlert = true
        }
    }
}

struct MeasurementEditor_Previews: PreviewProvider {
    static var previews: some View {
        
        let viewContext = PersistenceController.preview.container.viewContext
        
        // Creating, measurement is nil
        MeasurementEditor(persistenceController: PersistenceController.preview, validator: Validator())
        
        // Editing, measurement is passed
        MeasurementEditor(persistenceController: PersistenceController.preview, measurement: Record.placeholder(in: viewContext), validator: Validator())
    }
}
