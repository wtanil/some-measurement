//
//  ValueEditor.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 22/03/21.
//

import SwiftUI
import CoreData

struct ValueEditor: View {
    typealias Labels = Constant.ValueEditor
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var value: Value
    @State private var showingDeleteAlert: Bool = false
    @State private var amountString: String
    
    private let persistenceController: PersistenceController
    private let childContext: NSManagedObjectContext
    private let measurement: Record
    private let defaultUnit: RecordUnit
    private var mode: Mode = .create
    private let validator: Validator
    
    init(persistenceController: PersistenceController, measurement: Record, value: Value? = nil, validator: Validator) {
        self.persistenceController = persistenceController
        self.childContext = persistenceController.childViewContext()
        self.measurement = persistenceController.copyForEditing(of: measurement, in: childContext)
        self.validator = validator
        
        if let measurementUnit = measurement.defaultUnit {
            self.defaultUnit = persistenceController.copyForEditing(of: measurementUnit, in: childContext)
            
        } else {
            self.defaultUnit = RecordUnit.getUnitNone(in: childContext)
        }
        
        if let value = value {
            self.value = persistenceController.copyForEditing(of: value, in: childContext)
            _amountString = State(initialValue: value.displayValue)
            
            self.mode = .edit
        } else {
            _amountString = State(initialValue: "0")
            self.value = persistenceController.newTemporaryInstance(in: childContext)
            self.value.unit = defaultUnit
            self.measurement.addToValues(self.value)
            
            self.mode = .create
        }
    }
    
    var body: some View {
        NavigationView {
            
            Form {
                Section {
                    // Workaround - Embeded HStack to fix TextField value not showing when the view appears
                    HStack {
                        TextField(Labels.valueTextField, text: $amountString)
                            .onChange(of: amountString) { value in
                                setAmount(value)
                            }
                            .disableAutocorrection(true)
                            .keyboardType(.decimalPad)
                            .accessibilityIdentifier("ValueTextField")
                    }
                    
                    NavigationLink(
                        destination: UnitSelect(selectedUnit: $value.unit.withDefaultValue(defaultUnit)).environment(\.managedObjectContext, childContext),
                        label: {
                            Text(selectedUnitLabel)
                        })
                    .accessibilityIdentifier("SelectUnitButton")
                }
                
                Section {
                    DatePicker(Labels.datePickerTitle, selection: $value.date.withDefaultValue(Date()), displayedComponents: [.date, .hourAndMinute])
                        .accessibilityIdentifier("DatePicker")
                }
            }
            .navigationTitle(navigationTitle)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .alert(isPresented: $showingDeleteAlert, content: { deleteAlert })
        }
    }
    
    private var navigationBarTrailingItem: some View {
        HStack(spacing: 0) {
            if mode == .edit {
                Button(action: {
                    self.showingDeleteAlert.toggle()
                }, label: {
                    Image(systemName: "trash")
                        .foregroundColor(.red)
                        .imageScale(.medium)
                        .padding(5)
                })
                .accessibilityIdentifier("DeleteButton")
            }
            
            Button(action: {
                save()
            }, label: {
                Text(saveButtonLabel)
                    .padding(5)
            })
                .disabled(!isFormValid)
                .accessibilityIdentifier("SaveButton")
        }
    }
    
    private var deleteAlert: Alert {
        Alert(title: Text(Labels.deleteAlertTitle),
              message: Text(Labels.deleteAlertMessage),
              primaryButton: .destructive(Text(Labels.deleteAlertConfirmLabel), action: deleteValue),
              secondaryButton: .default(Text(Labels.deleteAlertCancelLabel)))
    }
}

extension ValueEditor {
    
    func setAmount(_ string: String) {
        if isValueValid {
            guard let nsNumberQuickUpdateInterval = NumberFormatter.numberFormatterDecimal.number(from: string) else { return }
            value.value = nsNumberQuickUpdateInterval.doubleValue
        }
    }
    
    var isValueValid: Bool {
//        return Value.validateFor(value: value.displayValue)
        return validator.validateStringIsDecimal(string: amountString)
    }
    
    var isFormValid: Bool {
        return isValueValid && !amountString.isEmpty
    }
    
    var navigationTitle: String {
        switch mode {
        case .create:
            return Labels.navigationBarAddTitle
        case .edit:
            return Labels.navigationBarEditTitle
        }
    }
    
    var saveButtonLabel: String {
        switch mode {
        case .create:
            return Labels.saveAddButtonLabel
        case .edit:
            return Labels.saveEditButtonLabel
        }
    }
    
    var selectedUnitLabel: String {
        guard let unit = value.unit else {
//            return defaultUnit.displayListTitle
            #warning("Need to find out why after deleting, but before segueing back to value history, this function is triggered. Causing fatalerror to be called since value is already nil. Expected should not trigger any of the functions")
            return ""
        }
        return unit.displayListTitle
    }
    
    private func deleteValue() {
        persistenceController.delete(value, in: childContext)
        persistenceController.persist(for: childContext)
        
        presentationMode.wrappedValue.dismiss()
    }
    
    private func save() {
        persistenceController.persist(value)
        presentationMode.wrappedValue.dismiss()
    }
}

struct ValueEditor_Previews: PreviewProvider {
    static var previews: some View {
        let viewContext = PersistenceController.preview.container.viewContext
        
        ValueEditor(persistenceController: PersistenceController.shared, measurement: Record.placeholder(in: viewContext), validator: Validator())
        
        ValueEditor(persistenceController: PersistenceController.shared, measurement: Record.placeholder(in: viewContext), value: Value.placeholder(in: viewContext), validator: Validator())
    }
}
