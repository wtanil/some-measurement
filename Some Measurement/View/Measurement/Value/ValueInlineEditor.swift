//
//  ValueInlineEditor.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 18/03/21.
//

import SwiftUI

struct ValueInlineEditor: View {
    typealias Labels = Constant.ValueInlineEditor
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @State var amount: String = ""
    @State var unit: RecordUnit
    
    let persistenceController: PersistenceController
    let measurement: Record
    private let validator: Validator
    
    init(persistenceController: PersistenceController, measurement: Record, defaultUnit: RecordUnit, validator: Validator) {
        self.persistenceController = persistenceController
        
        self.measurement = measurement
        self._unit = State(initialValue: defaultUnit)
        self.validator = validator
    }
    
    var body: some View {
        HStack {
            TextField(Labels.valueTextField, text: $amount)
                .disableAutocorrection(true)
                .keyboardType(.decimalPad)
                .accessibilityIdentifier("ValueTextField")
            
            NavigationLink(
                destination: UnitSelect(selectedUnit: $unit)
                    .environment(\.managedObjectContext, viewContext),
                label: {
                    Text(selectedUnitLabel)
                })
            .accessibilityIdentifier("InlineSelectUnitButton")
            
            Button(action: {
                save()
            }, label: {
                Text(Labels.buttonLabel)
            })
                .disabled(!isFormValid)
                .accessibilityIdentifier("SaveButton")
        }
    }
}

extension ValueInlineEditor {
    
    var selectedUnitLabel: String {
        return unit.displayListTitle
    }
    
    var isValueValid: Bool {
        return validator.validateStringIsDecimal(string: amount)
    }
    
    var isFormValid: Bool {
        return isValueValid
    }
    
    func createValue(measurement: Record, amount: Double, unit: RecordUnit) -> Value {
        let newValue: Value = persistenceController.newTemporaryInstance(in: measurement.managedObjectContext!)
        newValue.set(amount: amount, date: Date(), unit: unit)
        
        measurement.addToValues(newValue)
        
        return newValue
    }
    
    func save() {
        guard let nsNumberAmount = NumberFormatter.numberFormatterDecimal.number(from: amount) else {
            amount = ""
            return
        }
        
        let newValue = createValue(measurement: measurement, amount: nsNumberAmount.doubleValue, unit: unit)
        
        persistenceController.persist(newValue)
        
        amount = ""
    }
}

struct ValueInlineEditor_Previews: PreviewProvider {
    static var previews: some View {
        let viewContext = PersistenceController.preview.container.viewContext
        
        NavigationView {
            ValueInlineEditor(
                persistenceController: PersistenceController.preview,
                measurement: Record.placeholder(in: viewContext),
                defaultUnit: RecordUnit.placeholder(in: viewContext), validator: Validator())
                .environment(\.managedObjectContext, viewContext)
            
        }
    }
}
