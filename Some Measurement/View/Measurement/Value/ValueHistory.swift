//
//  ValueHistory.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 19/03/21.
//

import SwiftUI

struct ValueHistory: View {
    typealias Labels = Constant.ValueHistory
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject var measurement: Record
    
    @State private var selectedValue: Value?
    @State private var showingValueEditorCreateSheet: Bool = false
    
    var body: some View {
        NavigationView {
            DynamicList(sortDescriptors: [sortDescriptor], predicate: predicate){ (value: Value) in
                
                Button(action: {
                    setSelectedValue(with: value)
                }, label: {
                    VStack(alignment: .leading) {
                        Text("\(value.displayValueWithUnit)")
                        Text(value.displayDate)
                            .font(.caption)
                    }
                })
                .accessibilityIdentifier("ValueCell-\(value.displayValueWithUnit)")
            }
            .navigationTitle(Labels.navigationTitle)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: navigationBarTrailingItem)
            .sheet(item: $selectedValue) { value2 in
//                ValueEditor(persistenceController: PersistenceController.shared, measurement: measurement, value: selectedValue, validator: Validator())
                ValueEditor(persistenceController: PersistenceController.shared, measurement: measurement, value: value2, validator: Validator())
            }
            .sheet(isPresented: $showingValueEditorCreateSheet, content: {
                ValueEditor(persistenceController: PersistenceController.shared, measurement: measurement, validator: Validator())
            })
        }
    }
    
    private var navigationBarTrailingItem: some View {
        Button(action: {
            self.showingValueEditorCreateSheet.toggle()
        }, label: {
            Image(systemName: "plus")
                .imageScale(.medium)
                .padding()
        })
        .accessibilityIdentifier("NewValueButton")
    }
}

extension ValueHistory {
    private var sortDescriptor: NSSortDescriptor {
        NSSortDescriptor(keyPath: \Value.date, ascending: false)
    }
    
    private var predicate: NSPredicate? {
        NSPredicate(format: "record = %@", measurement)
    }
    
    private func setSelectedValue(with value: Value) {
        self.selectedValue = value
    }
}

struct ValueHistory_Previews: PreviewProvider {
    static var previews: some View {
        ValueHistory(measurement: Record.placeholder(in: PersistenceController.preview.container.viewContext))
    }
}
