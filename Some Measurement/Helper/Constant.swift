//
//  Constant.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 20/01/21.
//

import Foundation

enum Constant {
    
    enum ContentView {
        static let homeTabItemTitle = "Home"
        static let settingTabItemTitle = "Setting"
        static let tutorialDoneButtonLabel = "Skip"
    }
    
    enum TutorialView {
        static let tutorialMessage = "Thank you for using Some Measurement"
    }
    
    enum MeasurementHome {
        static let navigationTitle: String = "Measurements"
        static let lastValueNone: String = "-"
        static let lastValueDateNone: String = "-"
        static let searchTextField: String = "Search"
        static let quickUpdateIncreaseLabel: String = "Increase by "
        static let quickUpdateDecreaseLabel: String = "Decrease by "
    }
    
    enum MeasurementControl {
        static let sortButtonLabel: String = "Sort by: "
        static let sortActionLabel: String = "Sort by"
        static let emptySelectedLabel: String = "Filter by: none"
        static let notEmptySelectedLabel: String = "Filter by: "
        static let searchPlaceholder: String = "Search"
    }
    
    enum MeasurementSort {
        static let navigationTitle: String = "Sort Options"
    }
    
    enum TagFilter {
        static let navigationBarTitle: String = "Select Tags"
        static let emptySelectedLabel: String = "No tags"
        static let searchTextField: String = "Search"
    }
    
    enum MeasurementEditor {
        static let navigationBarAddTitle: String = "New Measurement"
        static let navigationBarEditTitle: String = "Edit Measurement"
        static let saveAddButtonLabel: String = "Add"
        static let saveEditButtonLabel: String = "Save"
        static let titleTextField: String = "Title"
        static let noteTextField: String = "Note"
        static let quickUpdateIntervalTextField: String = "Interval value"
        static let tagsSectionHeader: String = "Tags"
        static let emptySelectedTagsLabel: String = "Select Tags"
        static let defaultUnitSectionHeader: String = "Default Unit"
        static let quickUpdateIntervalHeader: String = "Quick Update Interval"
        static let quickUpdateIntervalFooter: String = "Enter 0 to disable quick update functionality"
        static let quickUpdateIntervalEditFooter: String = "Change quick update value from context menu on home"
        static let emptySelectedDefaultUnitLabel: String = "Select default unit"
        static let saveAlertTitle: String = "Measurement with a same title already exists"
        static let saveAlertMessage: String = "Please rename the measurement"
        static let saveAlertConfirmLabel: String = "Rename"
    }
    
    enum MeasurementDetail {
        static let navigationTitle: String = "Measurement"
        static let editButtonLabel: String = "Edit"
        static let deleteAlertTitle: String = "Warning"
        static let deleteAlertMessage: String = "Do you want to remove the measurement?"
        static let deleteAlertConfirmLabel: String = "Delete"
        static let deleteAlertCancelLabel: String = "Cancel"
        static let actionSheetTitle: String = "More options"
        static let actionSheetEditLabel: String = "Edit"
        static let actionSheetNewValueLabel: String = "Update value"
        static let actionSheetHistoryLabel: String = "History"
        static let actionSheetDeleteLabel: String = "Delete"
        static let lastValueNone: String = "None"
        static let lastValueLabel: String = "Value: "
    }
    
    enum ValueInlineEditor {
        static let valueTextField: String = "New Value"
        static let buttonLabel: String = "Update"
        static let emptySelectedUnitLabel: String = "Unit"
    }
    
    enum ValueHistory {
        static let navigationTitle: String = "History"
    }
    
    enum ValueEditor {
        static let navigationBarAddTitle: String = "New Value"
        static let navigationBarEditTitle: String = "Edit Value"
        static let saveAddButtonLabel: String = "Add"
        static let saveEditButtonLabel: String = "Save"
        static let valueTextField: String = "Value"
        static let emptySelectedUnitLabel: String = "Select a unit"
        static let deleteAlertTitle: String = "Warning"
        static let deleteAlertMessage: String = "Do you want to remove the value?"
        static let deleteAlertConfirmLabel: String = "Delete"
        static let deleteAlertCancelLabel: String = "Cancel"
        static let datePickerTitle: String = "Date"
    }
    
    enum SettingHome {
        static let navigationBarTitle: String = "Setting"
        static let manageTagsLabel: String = "Manage tags"
        static let manageUnitsLabel: String = "Manage units"
    }
    
    enum TagHome {
        static let navigationBarTitle: String = "Manage Tags"
        static let searchPlaceholder: String = "Search"
    }
    
    enum TagEditor {
        static let navigationBarAddTitle: String = "New Tag"
        static let navigationBarEditTitle: String = "Edit Tag"
        static let saveAddButtonLabel: String = "Add"
        static let saveEditButtonLabel: String = "Save"
        static let nameTextField: String = "Title"
        static let deleteAlertTitle: String = "Warning"
        static let deleteAlertMessage: String = "Do you want to remove the tag?"
        static let deleteAlertConfirmLabel: String = "Delete"
        static let deleteAlertCancelLabel: String = "Cancel"
        static let saveAlertTitle: String = "Tag already exists"
        static let saveAlertMessage: String = "Please rename the tag"
        static let saveAlertConfirmLabel: String = "Rename"
    }
    
    enum UnitHome {
        static let navigationBarTitle: String = "Manage Units"
        static let searchPlaceholder: String = "Search"
    }
    
    enum UnitEditor {
        static let navigationBarAddTitle: String = "New Unit"
        static let navigationBarEditTitle: String = "Edit Unit"
        static let saveAddButtonLabel: String = "Add"
        static let saveEditButtonLabel: String = "Save"
        static let nameTextField: String = "Title"
        static let deleteAlertTitle: String = "Warning"
        static let deleteAlertMessage: String = "Do you want to remove the unit? Removing it will replace all occurrences with unit \"none\"."
        static let deleteAlertConfirmLabel: String = "Delete"
        static let deleteAlertCancelLabel: String = "Cancel"
        static let saveAlertTitle: String = "Unit already exists"
        static let saveAlertMessage: String = "Please rename the unit"
        static let saveAlertConfirmLabel: String = "Rename"
    }
    
    enum UnitSelect {
        static let navigationBarTitle: String = "Select a Unit"
        static let emptySelectedLabel: String = "No unit selected"
        static let searchTextField: String = "Search"
    }
    
}
