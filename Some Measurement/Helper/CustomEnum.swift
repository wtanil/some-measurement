//
//  CustomEnum.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 21/02/22.
//

import Foundation

enum Mode {
    case create
    case edit
}

enum AlertMode: Identifiable {
    case delete, save
    var id: Self { self }
}

enum ContextMenuMode {
    case increment, decrement
}
