//
//  Array+extension.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 13/03/22.
//

import Foundation

extension Array {
    
    mutating func remove(_ element: Element) where Element: Equatable {
        if let index = firstIndex(of: element) {
            remove(at: index)
        }
    }
    
    mutating func appendAndSort(_ element: Element, comparator: (Element, Element) -> Bool) {
        append(element)
        sort(by: comparator)
    }
    
}
