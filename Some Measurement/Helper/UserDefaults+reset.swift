//
//  UserDefaults+reset.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 05/07/21.
//

import Foundation

extension UserDefaults {
    
    enum Keys: String, CaseIterable {
        case isNotFirstTime
    }
    
    func reset() {
        for key in Keys.allCases {
            removeObject(forKey: key.rawValue)
        }
    }
}
