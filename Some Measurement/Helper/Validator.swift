//
//  Validator.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 15/03/22.
//

import Foundation

class Validator {
    
    func validateStringIsNotEmpty(string: String) -> Bool {
        string.isEmpty ? false : true
    }
    
    func validateStringIsDecimal(string: String) -> Bool {
        guard let _ = NumberFormatter.numberFormatterDecimal.number(from: string) else {
            return false
        }
        return true
    }
    
    
}
