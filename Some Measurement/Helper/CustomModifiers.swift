//
//  CustomModifiers.swift
//  Some Measurement
//
//  Created by William Suryadi Tanil on 17/11/21.
//

import SwiftUI

struct Bold: ViewModifier {
    var shouldBold: Bool
    
    func body(content: Content) -> some View {
        if shouldBold {
            content
                .font(Font.system(.body).weight(.heavy))
        } else {
            content
        }
    }
}

// Pull-to-refresh modifier ONLY for iOS 15 and List
//public struct customRefreshable: ViewModifier {
//    var action: () -> Void
//    
//    @available(iOS 15.0.0, *)
//    @Sendable
//    func actionFunction() async -> Void {
//        action()
//    }
//    
//    public func body(content: Content) -> some View {
//        
//        if #available(iOS 15, *) {
//            content
//                .refreshable(action: actionFunction)
//        } else {
//            content
//        }
//    }
//}


