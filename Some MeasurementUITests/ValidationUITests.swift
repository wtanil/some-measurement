//
//  ValidationUITests.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 26/04/22.
//

import XCTest

class ValidationUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
    }
    
    override func tearDownWithError() throws {
        app = nil
        app = makeAppForReset()
        app.launch()
        app = nil
        try super.tearDownWithError()
    }
    
    // MARK: Tag Validation UI Tests
    /*
    TAG
    title: unique, not empty, case sensitive
     */
    func testTagValidation_create() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        app.tables.buttons["ManageTagsCell"].tap()
        XCTAssertTrue(app.navigationBars["Manage Tags"].waitForExistence(timeout: 3))
        app.navigationBars["Manage Tags"].buttons["NewTagButton"].tap()
        XCTAssertTrue(app.navigationBars["New Tag"].waitForExistence(timeout: 3))
        let nameTextField = app.textFields["NameTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        let alert = app.alerts["Tag already exists"]
        let renameButton = alert.buttons["Rename"]
        let nameValidationIndicator = app.images["NameValidationImage"]
        XCTAssertTrue(nameTextField.exists)
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(deleteButton.exists)
        XCTAssertFalse(saveButton.isEnabled)
        XCTAssertTrue(nameValidationIndicator.exists)
        
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        
        // test empty string
        nameTextField.typeTextLoop("T")
        XCTAssertFalse(nameValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        nameTextField.clearText("T")
        XCTAssertTrue(nameValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("12")
        XCTAssertFalse(nameValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        nameTextField.clearText("12")
        XCTAssertTrue(nameValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
        // test tag exists, case insensitive
        nameTextField.typeTextLoop("Tag1")
        XCTAssertFalse(nameValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        renameButton.tap()
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("Tag1")
        XCTAssertTrue(nameValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("tag1")
        XCTAssertFalse(nameValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        renameButton.tap()
        XCTAssertTrue(saveButton.isEnabled)
    }
    
    func testTagValidation_edit() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        app.tables.buttons["ManageTagsCell"].tap()
        XCTAssertTrue(app.navigationBars["Manage Tags"].waitForExistence(timeout: 3))
        
        let tag1Cell = app.tables.cells.buttons["TagCell-Tag1"]
        tag1Cell.tap()
        XCTAssertTrue(app.navigationBars["Edit Tag"].waitForExistence(timeout: 3))
        
        let nameTextField = app.textFields["NameTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        let alert = app.alerts["Tag already exists"]
        let renameButton = alert.buttons["Rename"]
        let nameValidationIndicator = app.images["NameValidationImage"]
        XCTAssertTrue(nameTextField.exists)
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(deleteButton.exists)
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertFalse(nameValidationIndicator.exists)
        
        XCTAssertTrue(nameTextField.value as! String == "Tag1")
        
        // saving without changing
        saveButton.tap()
        XCTAssertTrue(tag1Cell.exists)
        
        tag1Cell.tap()
        
        // test name already exists
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("1")
        XCTAssertFalse(nameValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        nameTextField.typeTextLoop("2")
        XCTAssertFalse(nameValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        renameButton.tap()
        
        // test same name
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("2")
        nameTextField.typeTextLoop("1")
        saveButton.tap()
        XCTAssertTrue(tag1Cell.exists)
        
        tag1Cell.tap()
        
        // test number as name
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("Tag1")
        XCTAssertTrue(nameValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("11")
        XCTAssertFalse(nameValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        nameTextField.clearText("11")
        XCTAssertTrue(nameValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
        // test case insensitive
        nameTextField.typeTextLoop("tag2")
        saveButton.tap()
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        renameButton.tap()
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("2")
        nameTextField.typeTextLoop("1")
        saveButton.tap()
        XCTAssertFalse(tag1Cell.exists)
        XCTAssertTrue(app.tables.cells.buttons["TagCell-tag1"].exists)
    }
    
    // MARK: Unit Validation UI Tests
    /*
     UNIT
     1. title: unique, not empty, case insensitive
     */
    func testUnitValidation_create() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        app.tables.buttons["ManageUnitsCell"].tap()
        XCTAssertTrue(app.navigationBars["Manage Units"].waitForExistence(timeout: 3))
        app.navigationBars["Manage Units"].buttons["NewUnitButton"].tap()
        XCTAssertTrue(app.navigationBars["New Unit"].waitForExistence(timeout: 3))
        
        // Unit Editor .create
        let nameTextField = app.textFields["TitleTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        let alert = app.alerts["Unit already exists"]
        let renameButton = alert.buttons["Rename"]
        let titleValidationIndicator = app.images["TitleValidationImage"]
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(deleteButton.exists)
        XCTAssertFalse(saveButton.isEnabled)
        XCTAssertTrue(titleValidationIndicator.exists)
        
        // test empty and unique
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.typeTextLoop("111")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        nameTextField.clearText("111")
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("u")
        nameTextField.clearText("u")
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("unit")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        renameButton.tap()
        
        // test case insensitive
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("t")
        nameTextField.typeTextLoop("T")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        XCTAssertTrue(app.tables.cells.buttons["UnitCell-unit"].exists)
        XCTAssertTrue(app.tables.cells.buttons["UnitCell-uniT"].exists)
    }
    
    func testUnitValidation_edit() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        app.tables.buttons["ManageUnitsCell"].tap()
        let nothingCell = app.tables.cells.buttons["UnitCell-nothing"]
        
        XCTAssertTrue(nothingCell.exists)
        XCTAssertTrue(nothingCell.isEnabled)
        
        nothingCell.tap()
        
        XCTAssertTrue(app.navigationBars["Edit Unit"].waitForExistence(timeout: 3))
        
        let nameTextField = app.textFields["TitleTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        let alert = app.alerts["Unit already exists"]
        let renameButton = alert.buttons["Rename"]
        let titleValidationIndicator = app.images["TitleValidationImage"]
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(deleteButton.exists)
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertTrue(nameTextField.value as! String == "nothing")
        XCTAssertFalse(titleValidationIndicator.exists)
        
        // test without changing
        saveButton.tap()
        XCTAssertTrue(nothingCell.exists)
        nothingCell.tap()
        
        // test name already exists
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("othing")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        nameTextField.typeTextLoop("one")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        renameButton.tap()
        
        // test number as name
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("none")
        nameTextField.typeTextLoop("111")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        
        // test same name after change
        nameTextField.clearText("111")
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("nothing")
        saveButton.tap()
        
        XCTAssertTrue(nothingCell.exists)
        nothingCell.tap()
        
        // test case insensitive
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("g")
        nameTextField.typeTextLoop("G")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        XCTAssertFalse(nothingCell.exists)
        XCTAssertTrue(app.tables.cells.buttons["UnitCell-nothinG"].exists)
    }
    
    // MARK: Unit Validation UI Tests
    /*
     VALUE
     1. value: number
     2. default unit
     */
    func testValueValidation_create() {
        app = makeApp()
        app.launch()
        
        app.tables.cells.buttons["RecordCell-With note"].tap()
        
        XCTAssertTrue(app.navigationBars["Measurement"].waitForExistence(timeout: 3))
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        
        XCTAssertTrue(app.sheets["More options"].waitForExistence(timeout: 3))
        app.sheets["More options"].scrollViews.otherElements.buttons["Update value"].tap()
        
        XCTAssertTrue(app.navigationBars["New Value"].waitForExistence(timeout: 3))
        
        let valueTextField = app.textFields["ValueTextField"]
        let selectUnitButton = app.buttons["SelectUnitButton"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertFalse(deleteButton.exists)
        XCTAssertTrue(valueTextField.value as! String == "0")
        XCTAssertTrue(selectUnitButton.label == "unit")
        
        // test not number
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.clearText("0")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.typeTextLoop("14")
        XCTAssertTrue(saveButton.isEnabled)
        valueTextField.clearText("4")
        XCTAssertTrue(saveButton.isEnabled)
        valueTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.clearText("1a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.clearText("a")
        XCTAssertFalse(saveButton.isEnabled)
        
    }
    
    func testValueValidation_inlineCreate() {
        app = makeApp()
        app.launch()
        
        // test not number
        app.tables.cells.buttons["RecordCell-With note"].tap()
        
        let valueToggleButton = app.buttons["ValueToggleButton"]
        XCTAssertTrue(valueToggleButton.exists)
        let valueTextField = app.textFields["ValueTextField"]
        let selectUnitButton = app.buttons["InlineSelectUnitButton"]
        let saveButton = app.buttons["SaveButton"]
        XCTAssertFalse(valueTextField.exists)
        XCTAssertFalse(selectUnitButton.exists)
        XCTAssertFalse(saveButton.exists)
        
        valueToggleButton.tap()
        
        XCTAssertTrue(valueTextField.exists)
        XCTAssertTrue(selectUnitButton.exists)
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.typeTextLoop("14")
        XCTAssertTrue(saveButton.isEnabled)
        valueTextField.clearText("4")
        XCTAssertTrue(saveButton.isEnabled)
        valueTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.clearText("1a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.clearText("a")
        XCTAssertFalse(saveButton.isEnabled)

    }
    
    func testValueValidation_edit() {
        app = makeApp()
        app.launch()
        
        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        fullMeasurementCell.tap()
        
        // Measurement detail
        XCTAssertTrue(app.navigationBars["Measurement"].waitForExistence(timeout: 3))
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        XCTAssertTrue(app.sheets["More options"].waitForExistence(timeout: 3))
        app.sheets["More options"].scrollViews.otherElements.buttons["History"].tap()
        
        // Value history
        XCTAssertTrue(app.navigationBars["History"].waitForExistence(timeout: 3))
        app.tables.cells.buttons["ValueCell-11 unit"].tap()
        
        let valueTextField = app.textFields["ValueTextField"]
        let selectUnitButton = app.buttons["SelectUnitButton"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertTrue(deleteButton.exists)
        XCTAssertTrue(deleteButton.isEnabled)
        XCTAssertTrue(valueTextField.value as! String == "11")
        XCTAssertTrue(selectUnitButton.label == "unit")
        
        // test not number
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.clearText("11")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.typeTextLoop("14")
        XCTAssertTrue(saveButton.isEnabled)
        valueTextField.clearText("4")
        XCTAssertTrue(saveButton.isEnabled)
        valueTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.clearText("1a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.clearText("a")
        XCTAssertFalse(saveButton.isEnabled)
        
    }
    
    // MARK: QUI Validation UI Tests
    /*
     QUI
     1. value: number
     */
    func testQUIValidation_edit() {
        app = makeApp()
        app.launch()
        
        app.tables.cells.buttons["RecordCell-Full measurement"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        app.buttons["EditContextButton"].tap()
        let quiTextField = app.textFields["QUITextField"]
        let quiCheckmarkImage = app.images["CheckmarkImage"]
        let saveButton = app.buttons["SaveButton"]
        
        quiTextField.tapEndCorner()
        quiTextField.tapEndCorner()
        XCTAssertFalse(quiCheckmarkImage.exists)
        quiTextField.clearText("1")
        XCTAssertTrue(quiCheckmarkImage.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.typeTextLoop("14")
        XCTAssertFalse(quiCheckmarkImage.exists)
        XCTAssertTrue(saveButton.isEnabled)
        quiTextField.clearText("4")
        XCTAssertFalse(quiCheckmarkImage.exists)
        XCTAssertTrue(saveButton.isEnabled)
        quiTextField.typeTextLoop("a")
        XCTAssertTrue(quiCheckmarkImage.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.clearText("1a")
        XCTAssertTrue(quiCheckmarkImage.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.typeTextLoop("a")
        XCTAssertTrue(quiCheckmarkImage.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.clearText("a")
        XCTAssertTrue(quiCheckmarkImage.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
    }
    
    // MARK: Record Validation UI Tests
    /*
     RECORD
     1. title is valid: unique, not empty
     2. default unit
     */
    func testRecordValidation_create() {
        app = makeApp()
        app.launch()
        
        let newRecordButton = app.navigationBars["Measurements"].buttons["NewRecordButton"]
        newRecordButton.tap()
        
        let titleTextField = app.textFields["TitleTextField"]
        let quiTextField = app.textFields["QUITextField"]
        let saveButton = app.buttons["SaveButton"]
        let titleValidationIndicator = app.images["TitleValidationImage"]
        let quiValidationIndicator = app.images["QuiValidationImage"]
        
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(saveButton.isEnabled)
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(quiValidationIndicator.exists)
        
        // title unique
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("With note")
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertFalse(titleValidationIndicator.exists)
        saveButton.tap()
        
        let alert = app.alerts["Measurement with a same title already exists"]
        let renameButton = alert.buttons["Rename"]
        
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.clearText("With note")
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
        // title empty
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("11")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        titleTextField.clearText("11")
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        titleTextField.typeTextLoop(" ")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        titleTextField.typeTextLoop("a")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        titleTextField.swipeUp()
        
        // qui
        XCTAssertFalse(quiValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        quiTextField.tapEndCorner()
        quiTextField.tapEndCorner()
        quiTextField.clearText("0")
        XCTAssertTrue(quiValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.typeTextLoop("a")
        XCTAssertTrue(quiValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.typeTextLoop("1")
        XCTAssertTrue(quiValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.clearText("a1")
        XCTAssertTrue(quiValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.typeTextLoop("1")
        XCTAssertFalse(quiValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        quiTextField.typeTextLoop("2")
        XCTAssertFalse(quiValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        quiTextField.typeTextLoop("a")
        XCTAssertTrue(quiValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.clearText("a")
        XCTAssertFalse(quiValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        quiTextField.clearText("12")
        XCTAssertTrue(quiValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        quiTextField.typeTextLoop("7")
        XCTAssertFalse(quiValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        quiTextField.swipeDown()
        
        // title and qui
        // title false qui true
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.clearText(" a")
        XCTAssertFalse(saveButton.isEnabled)
        app.buttons["SelectUnitButton"].swipeUp()
        // title false qui false
        quiTextField.tapEndCorner()
        quiTextField.tapEndCorner()
        quiTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        app.buttons["SelectUnitButton"].swipeDown()
        // title true qui false
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
    }
    
    func testRecordValidation_edit() {
        app = makeApp()
        app.launch()
        
        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        XCTAssertTrue(fullMeasurementCell.exists)
        
        fullMeasurementCell.tap()
        
        let detailNavBar = app.navigationBars["Measurement"]
        XCTAssertTrue(detailNavBar.waitForExistence(timeout: 3))
        detailNavBar.buttons["MoreBarButton"].tap()
        // Action Sheet
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        
        XCTAssertTrue(app.navigationBars["Edit Measurement"].waitForExistence(timeout: 3))
        
        // Editor
        let titleTextField = app.textFields["TitleTextField"]
        let quiTextField = app.textFields["QUITextField"]
        let quiText = app.staticTexts["QUIText"]
        let saveButton = app.buttons["SaveButton"]
        let titleValidationIndicator = app.images["TitleValidationImage"]
        let quiValidationIndicator = app.images["QuiValidationImage"]
        
        // qui - disabled when editing
        XCTAssertTrue(quiText.exists)
        XCTAssertFalse(quiTextField.exists)
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertFalse(quiValidationIndicator.exists)
        
        // no changes
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        detailNavBar.buttons["MoreBarButton"].tap()
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        
        // title unique
        XCTAssertTrue(saveButton.isEnabled)
        
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.clearText("Full measurement")
        titleTextField.typeTextLoop("With note")
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertFalse(titleValidationIndicator.exists)
        saveButton.tap()
        
        let alert = app.alerts["Measurement with a same title already exists"]
        let renameButton = alert.buttons["Rename"]
        
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.clearText("With note")
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
        // title empty
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("11")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        titleTextField.clearText("11")
        XCTAssertTrue(titleValidationIndicator.exists)
        XCTAssertFalse(saveButton.isEnabled)
        titleTextField.typeTextLoop(" ")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
        titleTextField.typeTextLoop("a")
        XCTAssertFalse(titleValidationIndicator.exists)
        XCTAssertTrue(saveButton.isEnabled)
    }
}
