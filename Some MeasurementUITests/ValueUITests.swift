//
//  ValueUITests.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 05/04/22.
//

import XCTest

class ValueUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
    }
    
    override func tearDownWithError() throws {
        app = nil
        app = makeAppForReset()
        app.launch()
        app = nil
        try super.tearDownWithError()
    }
    
    // to create, 1 via context menu, 2 via update action sheet, 3 via inline, 4 via value history
    
    // MARK: UI Tests
    func testCreateValue_contextMenu() {
        app = makeApp()
        app.launch()
        
        let withNoteCell = app.tables.cells.buttons["RecordCell-With note"]
        let homeValueText = withNoteCell.staticTexts["ValueText"]
        XCTAssertTrue(homeValueText.exists)
        XCTAssertTrue(homeValueText.label == "-")
        withNoteCell.press(forDuration: 1)
        let increaseContextButton = app.buttons["IncreaseContextButton"]
        let decreaseContextButton = app.buttons["DecreaseContextButton"]
        XCTAssertTrue(increaseContextButton.exists)
        XCTAssertTrue(decreaseContextButton.exists)
        increaseContextButton.tap()
        XCTAssertTrue(homeValueText.label == "1 unit")
        withNoteCell.press(forDuration: 1)
        decreaseContextButton.tap()
        XCTAssertTrue(homeValueText.label == "0 unit")
        withNoteCell.press(forDuration: 1)
        decreaseContextButton.tap()
        XCTAssertTrue(homeValueText.label == "-1 unit")
        
        withNoteCell.tap()
        let valueText = app.staticTexts["ValueDetailText"]
        XCTAssertTrue(valueText.exists)
        XCTAssertTrue(valueText.label == "Value: -1 unit")
    }
    // bug: create a new record with 0 qui, then check if context menu works, edit the record qui to 1, then check if context menu works, should work but doesnt, icon appears tho, possible solution, reload measurement home
    
    func testCreateValue_actionSheet() {
        app = makeApp()
        app.launch()
        
        let withNoteCell = app.tables.cells.buttons["RecordCell-With note"]
        let homeValueText = withNoteCell.staticTexts["ValueText"]
        XCTAssertTrue(homeValueText.exists)
        XCTAssertTrue(homeValueText.label == "-")
        
        withNoteCell.tap()
        
        let valueText = app.staticTexts["ValueDetailText"]
        let detailNavBar = app.navigationBars["Measurement"]
        XCTAssertTrue(detailNavBar.exists)
        detailNavBar.buttons["MoreBarButton"].tap()
        // Action Sheet
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["Update value"].tap()
        
        let valueTextField = app.textFields["ValueTextField"]
        let selectUnitButton = app.buttons["SelectUnitButton"]
        let datePicker = app.datePickers["DatePicker"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        
        XCTAssertTrue(valueTextField.waitForExistence(timeout: 3))
        
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(deleteButton.exists)
        XCTAssertTrue(saveButton.isEnabled)
        
        // value textfield
        
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.clearText("0")
        valueTextField.typeTextLoop("a")
        XCTAssertFalse(saveButton.isEnabled)
        
        valueTextField.clearText("a")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.typeTextLoop("12")
        XCTAssertTrue(saveButton.isEnabled)
        
        // select unit
        
        XCTAssertTrue(selectUnitButton.label == "unit")
        
        selectUnitButton.tap()
        
        let selectedUnitCell = app.tables.buttons["UnitCell-none"]
        selectedUnitCell.tap()
        
        XCTAssertTrue(app.staticTexts["SelectedUnitText"].label == "none")
        
        app.navigationBars["Select a Unit"].buttons["New Value"].tap()
        
        XCTAssertTrue(selectUnitButton.label == "none")
        
        
        // datepicker
        XCTAssertTrue(datePicker.exists)
        XCTAssertTrue(datePicker.isEnabled)
        
        saveButton.tap()
        
        XCTAssertTrue(valueText.label == "Value: 12")
        app.navigationBars["Measurement"].buttons["Measurements"].tap()
        XCTAssertTrue(homeValueText.label == "12")
    }
    
    func testCreateValue_inlineEditor() {
        app = makeApp()
        app.launch()
        
        let withNoteCell = app.tables.cells.buttons["RecordCell-With note"]
        let homeValueText = withNoteCell.staticTexts["ValueText"]
        XCTAssertTrue(homeValueText.exists)
        XCTAssertTrue(homeValueText.label == "-")
        
        withNoteCell.tap()
        
        let valueText = app.staticTexts["ValueDetailText"]
        let valueToggleButton = app.buttons["ValueToggleButton"]
        XCTAssertTrue(valueText.exists)
        XCTAssertTrue(valueToggleButton.exists)
        let valueTextField = app.textFields["ValueTextField"]
        let selectUnitButton = app.buttons["InlineSelectUnitButton"]
        let saveButton = app.buttons["SaveButton"]
        XCTAssertFalse(valueTextField.exists)
        XCTAssertFalse(selectUnitButton.exists)
        XCTAssertFalse(saveButton.exists)
        
        valueToggleButton.tap()
                
        XCTAssertTrue(valueText.exists)
        XCTAssertTrue(valueToggleButton.exists)
        XCTAssertTrue(valueTextField.exists)
        XCTAssertTrue(selectUnitButton.exists)
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.typeText("1")
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        XCTAssertTrue(valueText.label == "Value: 1 unit")
        // when textfield is empty, placeholder value is the value
        XCTAssertTrue(valueTextField.value as! String == "New Value")
        XCTAssertFalse(saveButton.isEnabled)
        
        // update with different unit
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.clearText("1")
        valueTextField.typeText("2")
        selectUnitButton.tap()
        app.tables.buttons["UnitCell-none"].tap()
        app.navigationBars["Select a Unit"].buttons["Measurement"].tap()
        XCTAssertTrue(selectUnitButton.label == "none")
        
        saveButton.tap()
        
        XCTAssertTrue(valueText.label == "Value: 2")
        // when textfield is empty, placeholder value is the value
        XCTAssertTrue(valueTextField.value as! String == "New Value")
        XCTAssertFalse(saveButton.isEnabled)
        
        app.navigationBars["Measurement"].buttons["Measurements"].tap()
        XCTAssertTrue(homeValueText.label == "2")
    }
    
    func testCreateValue_history() {
        app = makeApp()
        app.launch()
        
        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        let homeValueText = fullMeasurementCell.staticTexts["ValueText"]
        
        fullMeasurementCell.tap()
        
        // Measurement detail
        let valueText = app.staticTexts["ValueDetailText"]
        let detailNavBar = app.navigationBars["Measurement"]
        XCTAssertTrue(detailNavBar.exists)
        detailNavBar.buttons["MoreBarButton"].tap()
        // Action Sheet
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["History"].tap()
        
        // Value history
        let valueHistoryNavBar = app.navigationBars["History"]
        let newValueBarButton = valueHistoryNavBar.buttons["NewValueButton"]
        XCTAssertTrue(valueHistoryNavBar.waitForExistence(timeout: 3))
        
        newValueBarButton.tap()
        
        let valueTextField = app.textFields["ValueTextField"]
        let selectUnitButton = app.buttons["SelectUnitButton"]
        let datePicker = app.datePickers["DatePicker"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(deleteButton.exists)
        XCTAssertTrue(saveButton.isEnabled)
        
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.clearText("0")
        XCTAssertFalse(saveButton.isEnabled)
        valueTextField.typeText("0")
        XCTAssertTrue(saveButton.isEnabled)
        
        // unit
        XCTAssertTrue(selectUnitButton.label == "unit")
        
        // datepicker
        XCTAssertTrue(datePicker.exists)
        XCTAssertTrue(datePicker.isEnabled)
        
        saveButton.tap()
        
        XCTAssertTrue(app.tables.cells.buttons["ValueCell-0 unit"].exists)
        
        valueHistoryNavBar.swipeToDismiss()
        
        XCTAssertTrue(valueText.label == "Value: 0 unit")
        app.navigationBars["Measurement"].buttons["Measurements"].tap()
        XCTAssertTrue(homeValueText.label == "0 unit")
        
    }
    
    func testEditValue() {
        app = makeApp()
        app.launch()
        
        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        let homeValueText = fullMeasurementCell.staticTexts["ValueText"]
        XCTAssertTrue(homeValueText.exists)
        XCTAssertTrue(homeValueText.label == "11 unit")
        
        fullMeasurementCell.tap()
        
        // Measurement detail
        let valueText = app.staticTexts["ValueDetailText"]
        let detailNavBar = app.navigationBars["Measurement"]
        XCTAssertTrue(detailNavBar.exists)
        detailNavBar.buttons["MoreBarButton"].tap()
        // Action Sheet
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["History"].tap()
        
        // Value history
        let value11Cell = app.tables.cells.buttons["ValueCell-11 unit"]
        XCTAssertTrue(value11Cell.waitForExistence(timeout: 3))
        XCTAssertTrue(value11Cell.exists)
        value11Cell.tap()
        
        let valueTextField = app.textFields["ValueTextField"]
        let selectUnitButton = app.buttons["SelectUnitButton"]
        let datePicker = app.datePickers["DatePicker"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertTrue(deleteButton.exists)
        XCTAssertTrue(deleteButton.isEnabled)
        XCTAssertTrue(valueTextField.value as! String == "11")
        XCTAssertTrue(selectUnitButton.label == "unit")
        
        // value textfield
        valueTextField.tapEndCorner()
        valueTextField.tapEndCorner()
        valueTextField.clearText("11")
        valueTextField.typeTextLoop("1")
        
        
        // select unit button
        selectUnitButton.tap()
        
        app.tables.buttons["UnitCell-none"].tap()
        app.navigationBars["Select a Unit"].buttons["Edit Value"].tap()
        XCTAssertTrue(selectUnitButton.label == "none")
        
        // date picker
        XCTAssertTrue(datePicker.exists)
        
        saveButton.tap()
        
//        let value11Cell = app.tables.cells.buttons["ValueCell-11 unit"]
        XCTAssertFalse(value11Cell.exists)
        let value1Cell = app.tables.cells.buttons["ValueCell-1"]
        XCTAssertTrue(value1Cell.exists)
        
        // delete
        
        value1Cell.tap()
        
        let delete2Button = app.buttons["DeleteButton"]
        
        XCTAssertTrue(delete2Button.exists)
        
        delete2Button.tap()
        
        let deleteAlert = app.alerts["Warning"]
        let alertCancelButton = deleteAlert.buttons["Cancel"]
        let alertDeleteButton = deleteAlert.buttons["Delete"]
        
        XCTAssertTrue(deleteAlert.exists)
        XCTAssertTrue(alertCancelButton.exists)
        XCTAssertTrue(alertDeleteButton.exists)
        
        alertCancelButton.tap()
        delete2Button.tap()
        alertDeleteButton.tap()
        
        sleep(1)
        
        XCTAssertFalse(value1Cell.waitForExistence(timeout: 3))
        
        // Check if measurement detail and home are updated
        app.navigationBars["History"].swipeToDismiss()
//        let valueText = app.staticTexts["ValueDetailText"]
        XCTAssertTrue(valueText.label != "Value: 11 unit")
        XCTAssertTrue(valueText.label == "Value: 27 unit")
        
        app.navigationBars["Measurement"].buttons["Measurements"].tap()
        XCTAssertTrue(homeValueText.label != "11 unit")
        XCTAssertTrue(homeValueText.label == "27 unit")
        
    }
        
}


//    func testasd() {
//
//        let app = XCUIApplication()
//        app.collectionViews.cells.children(matching: .other).element.children(matching: .other).element.tap()
//        app/*@START_MENU_TOKEN@*/.buttons["SkipButton"]/*[[".buttons[\"Skip\"]",".buttons[\"SkipButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let newrecordbuttonButton = app.navigationBars["Measurements"]/*@START_MENU_TOKEN@*/.buttons["NewRecordButton"]/*[[".buttons[\"Add\"]",".buttons[\"NewRecordButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        newrecordbuttonButton.tap()
//
//        let tablesQuery2 = app.tables
//        let titletextfieldTextField = tablesQuery2/*@START_MENU_TOKEN@*/.textFields["TitleTextField"]/*[[".cells[\"Title, Error\"]",".textFields[\"Title\"]",".textFields[\"TitleTextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
//        titletextfieldTextField.tap()
//
//        let tablesQuery = tablesQuery2
//        tablesQuery/*@START_MENU_TOKEN@*/.textFields["TitleTextField"]/*[[".cells",".textFields[\"Title\"]",".textFields[\"TitleTextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["New Measurement"]/*@START_MENU_TOKEN@*/.buttons["SaveButton"]/*[[".buttons[\"Add\"]",".buttons[\"SaveButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        tablesQuery/*@START_MENU_TOKEN@*/.buttons["RecordCell-asd"]/*[[".cells[\"asd, -, -\"]",".buttons[\"asd, -, -\"]",".buttons[\"RecordCell-asd\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let valuetextStaticText = app.scrollViews.otherElements/*@START_MENU_TOKEN@*/.staticTexts["ValueText"]/*[[".staticTexts[\"Value: None\"]",".staticTexts[\"ValueText\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        valuetextStaticText.tap()
//        app.navigationBars["Measurement"].buttons["Measurements"].tap()
//        newrecordbuttonButton.tap()
//        titletextfieldTextField.tap()
//        titletextfieldTextField.tap()
//        tablesQuery/*@START_MENU_TOKEN@*/.buttons["RecordCell-zxv"]/*[[".cells[\"zxv, -, -\"]",".buttons[\"zxv, -, -\"]",".buttons[\"RecordCell-zxv\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        valuetextStaticText.tap()
//
//    }
//    func testasd() {
//
//        let app = XCUIApplication()
//        app/*@START_MENU_TOKEN@*/.buttons["SkipButton"]/*[[".buttons[\"Skip\"]",".buttons[\"SkipButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["Measurements"]/*@START_MENU_TOKEN@*/.buttons["NewRecordButton"]/*[[".buttons[\"Add\"]",".buttons[\"NewRecordButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let tablesQuery2 = app.tables
//        let tablesQuery = tablesQuery2
//        tablesQuery/*@START_MENU_TOKEN@*/.textFields["TitleTextField"]/*[[".cells[\"Title, Error\"]",".textFields[\"Title\"]",".textFields[\"TitleTextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["New Measurement"]/*@START_MENU_TOKEN@*/.buttons["SaveButton"]/*[[".buttons[\"Add\"]",".buttons[\"SaveButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let element = tablesQuery2.cells["123, -, -"].children(matching: .other).element(boundBy: 0).children(matching: .other).element
//        element/*@START_MENU_TOKEN@*/.press(forDuration: 3.0);/*[[".tap()",".press(forDuration: 3.0);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//
//        let measurementNavigationBar = app.navigationBars["Measurement"]
//        let measurementsButton = measurementNavigationBar.buttons["Measurements"]
//        measurementsButton.tap()
//
//        let recordcell123Button = tablesQuery/*@START_MENU_TOKEN@*/.buttons["RecordCell-123"]/*[[".cells[\"123, -, -\"]",".buttons[\"123, -, -\"]",".buttons[\"RecordCell-123\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
//        recordcell123Button.swipeDown()
//        recordcell123Button.swipeRight()
//        measurementsButton.tap()
//        element/*@START_MENU_TOKEN@*/.swipeLeft()/*[[".swipeDown()",".swipeLeft()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        recordcell123Button.tap()
//        measurementNavigationBar/*@START_MENU_TOKEN@*/.buttons["MoreBarButton"]/*[[".buttons[\"More\"]",".buttons[\"MoreBarButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.sheets["More options"].scrollViews.otherElements.buttons["Edit"].tap()
//
//        let quitextfieldTextField = tablesQuery/*@START_MENU_TOKEN@*/.textFields["QUITextField"]/*[[".cells",".textFields[\"Interval value\"]",".textFields[\"QUITextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
//        quitextfieldTextField.tap()
//        quitextfieldTextField.tap()
//        app.navigationBars["Edit Measurement"]/*@START_MENU_TOKEN@*/.buttons["SaveButton"]/*[[".buttons[\"Save\"]",".buttons[\"SaveButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        measurementsButton.tap()
//        recordcell123Button.swipeDown()
//        recordcell123Button/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        measurementsButton.tap()
//        recordcell123Button/*@START_MENU_TOKEN@*/.press(forDuration: 2.3);/*[[".tap()",".press(forDuration: 2.3);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        measurementsButton.tap()
//        element.swipeDown()
//        measurementsButton.tap()
//        element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//
//        let tabBar = app.tabBars["Tab Bar"]
//        tabBar/*@START_MENU_TOKEN@*/.buttons["SettingTabButton"]/*[[".buttons[\"Setting\"]",".buttons[\"SettingTabButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        tabBar/*@START_MENU_TOKEN@*/.buttons["HomeTabButton"]/*[[".buttons[\"Home\"]",".buttons[\"HomeTabButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        recordcell123Button/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        measurementsButton.tap()
//        recordcell123Button.tap()
//        element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 0).tap()
//        app/*@START_MENU_TOKEN@*/.icons["Some Measurements"]/*[[".otherElements[\"Home screen icons\"]",".icons.icons[\"Some Measurements\"]",".icons[\"Some Measurements\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        element.tap()
//
//    }
