//
//  Helper.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 25/03/22.
//

import XCTest

extension XCTestCase {
//    func makeApp(skipTutorial: Bool = true, useAppData: Bool = true) -> XCUIApplication {
//        let app = XCUIApplication()
//        app.launchArguments.append("--uitest")
//        if skipTutorial {
//            app.launchArguments.append("--skiptutorial")
//        }
//        if useAppData {
//            app.launchArguments.append("--useappdata")
//        }
//        return app
//    }
    
    func makeApp(useAppData: Bool = true) -> XCUIApplication {
        let app = XCUIApplication()
        app.launchArguments.append("--uitest")
        if useAppData {
            app.launchArguments.append("--useappdata")
        }
        return app
    }
    
    func makeAppForReset() -> XCUIApplication {
        let app = XCUIApplication()
        app.launchArguments.append("--resetstate")
        return app
    }
    
}


extension XCUIElement {
    func typeTextLoop(_ string: String) {
        for character in string {
            self.typeText(String(character))
        }
    }
    
    func clearText(_ string: String) {
//        String(repeating: XCUIKeyboardKey.delete.rawValue, count: "Full measurement".count)
        for _ in string {
            self.typeText(XCUIKeyboardKey.delete.rawValue)
        }
    }
    
    func swipeToDismiss() {
        let start = self.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5))
        let finish = self.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 12))
        
        start.press(forDuration: 0, thenDragTo: finish, withVelocity: .fast, thenHoldForDuration: 0)
    }
    
    func tapEndCorner() {
        self.coordinate(withNormalizedOffset: CGVector(dx: 0.9, dy: 0.5)).tap()
    }
    
    func tapVeryStartCorner() {
        self.coordinate(withNormalizedOffset: CGVector(dx: 0.05, dy: 0.5)).tap()
    }
}
