//
//  TagUITests.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 04/04/22.
//

import XCTest

class TagUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
    }
    
    override func tearDownWithError() throws {
        app = nil
        app = makeAppForReset()
        app.launch()
        app = nil
        try super.tearDownWithError()
    }
    
    // MARK: UI Test
    func testCreateTag() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        // Setting
        app.tables.buttons["ManageTagsCell"].tap()
        // Tag Home
        XCTAssertTrue(app.navigationBars["Manage Tags"].buttons["NewTagButton"].waitForExistence(timeout: 3))
        app.navigationBars["Manage Tags"].buttons["NewTagButton"].tap()
        
        // Tag Editor .create
        let nameTextField = app.textFields["NameTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        
        // When in .create mode, save button should exist but not hittable, delete button should not exist
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(deleteButton.exists)
        // Save button should be disabled when name is not valid
        XCTAssertFalse(saveButton.isEnabled)
        
        // Input not unique name
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        // Note: Disable auto-correction on test device when testing
        nameTextField.typeTextLoop("Tagnone")
        
        // Save button should be enabled when title is valid even when name is not unique
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        // A warning alert should be displayed because name is not unique
        let alert = app.alerts["Tag already exists"]
        let renameButton = alert.buttons["Rename"]
        
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        
        // Dismiss alert
        renameButton.tap()
        
        // Rename name to a valid one
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        // Note: Disable auto-correction on test device when testing
        nameTextField.typeText("s")
        
        saveButton.tap()
        
        XCTAssertTrue(app.navigationBars["Manage Tags"].waitForExistence(timeout: 3))
        
        let tagnonesCell = app.tables.cells.buttons["TagCell-Tagnones"]
        XCTAssertTrue(tagnonesCell.exists)
        XCTAssertTrue(tagnonesCell.isEnabled)
        
    }
    
    func testEditTag_empty() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        // Setting
        app.tables.buttons["ManageTagsCell"].tap()
        XCTAssertTrue(app.navigationBars["Manage Tags"].waitForExistence(timeout: 3))
        // Tag Home
        let tagnoneCell = app.tables.cells.buttons["TagCell-Tagnone"]
        
        XCTAssertTrue(tagnoneCell.exists)
        XCTAssertTrue(tagnoneCell.isEnabled)
        
        tagnoneCell.tap()
        
        let tagEditorTitle = app.navigationBars["Edit Tag"].staticTexts["Edit Tag"]
        XCTAssertTrue(tagEditorTitle.exists)
        
        let nameTextField = app.textFields["NameTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(deleteButton.exists)
        // Save button should be disabled when name is not valid
        XCTAssertTrue(saveButton.isEnabled)
        
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("Tagnone")
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("Tag1")
        
        // Save button should be enabled when title is valid even when name is not unique
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        // A warning alert should be displayed because name is not unique
        let alert = app.alerts["Tag already exists"]
        let renameButton = alert.buttons["Rename"]
        
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        
        // Dismiss alert
        renameButton.tap()
        
        // Rename name to a valid one
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("Tag1")
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("new")
        
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        let newCell = app.tables.cells.buttons["TagCell-new"]
        XCTAssertTrue(newCell.exists)
        XCTAssertTrue(newCell.isEnabled)
//
//        newCell.tap()
//
//        let delete2Button = app.buttons["DeleteButton"]
//        XCTAssertTrue(delete2Button.waitForExistence(timeout: 3))
//
//        delete2Button.tap()
//
//        let deleteAlert = app.alerts["Warning"]
//        let alertCancelButton = deleteAlert.buttons["Cancel"]
//        let alertDeleteButton = deleteAlert.buttons["Delete"]
//
//        XCTAssertTrue(deleteAlert.exists)
//        XCTAssertTrue(alertCancelButton.exists)
//        XCTAssertTrue(alertDeleteButton.exists)
//
//        alertCancelButton.tap()
//        delete2Button.tap()
//        alertDeleteButton.tap()
//
//        XCTAssertFalse(newCell.exists)
        
    }
    
    func testEditTag_used() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        // Setting
        app.tables.buttons["ManageTagsCell"].tap()
        // Tag Home
        XCTAssertTrue(app.navigationBars["Manage Tags"].waitForExistence(timeout: 3))
        let tag1Cell = app.tables.cells.buttons["TagCell-Tag1"]
        
        tag1Cell.tap()
        XCTAssertTrue(app.navigationBars["Edit Tag"].waitForExistence(timeout: 3))
        
        let nameTextField = app.textFields["NameTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        XCTAssertTrue(saveButton.waitForExistence(timeout: 3))
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(deleteButton.exists)
        
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("1")
        XCTAssertTrue(saveButton.isEnabled)
        nameTextField.typeTextLoop("7")
        
        saveButton.tap()
        
        let tag7Cell = app.tables.cells.buttons["TagCell-Tag7"]
        XCTAssertFalse(tag1Cell.exists)
        XCTAssertTrue(tag7Cell.exists)
        XCTAssertTrue(tag7Cell.isEnabled)
        
        app.tabBars["Tab Bar"].buttons["Home"].tap()
        
        // check in tag filter
//        FilterNavLink
        app.buttons["FilterNavLink"].tap()
        XCTAssertTrue(app.tables.cells.buttons["TagCell-Tag7"].exists)
        app.navigationBars["Select Tags"].buttons["Measurements"].tap()
        
        let fullCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        
        fullCell.tap()
        XCTAssertTrue(app.navigationBars["Measurement"].waitForExistence(timeout: 3))
        
        XCTAssertFalse(app.staticTexts["TagText"].label == "Tag1, Tag2")
        XCTAssertTrue(app.staticTexts["TagText"].label == "Tag2, Tag7")
        
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        XCTAssertTrue(app.navigationBars["Edit Measurement"].waitForExistence(timeout: 3))
        
        XCTAssertTrue(app.buttons["SelectTagsButton"].label == "Tag2, Tag7")
        
        app.buttons["SelectTagsButton"].tap()
        
        XCTAssertFalse(app.tables.cells.buttons["Tag1"].exists)
        XCTAssertTrue(app.tables.cells.buttons["Tag7"].exists)
    }
    
    func testDeleteTag() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        // Setting
        app.tables.buttons["ManageTagsCell"].tap()
        XCTAssertTrue(app.navigationBars["Manage Tags"].waitForExistence(timeout: 3))
        // Tag Home
        let tag1Cell = app.tables.cells.buttons["TagCell-Tag1"]
        
        tag1Cell.tap()
        XCTAssertTrue(app.navigationBars["Edit Tag"].waitForExistence(timeout: 3))
        
        let deleteButton = app.buttons["DeleteButton"]
        XCTAssertTrue(deleteButton.exists)
        deleteButton.tap()
        app.alerts["Warning"].buttons["Delete"].tap()
        
        XCTAssertFalse(tag1Cell.exists)
        
        app.tabBars["Tab Bar"].buttons["Home"].tap()
        
        app.buttons["FilterNavLink"].tap()
        XCTAssertFalse(app.tables.cells.buttons["TagCell-Tag1"].exists)
        app.navigationBars["Select Tags"].buttons["Measurements"].tap()
        
        let fullCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        
        fullCell.tap()
        
        XCTAssertFalse(app.staticTexts["TagText"].label == "Tag1, Tag2")
        XCTAssertTrue(app.staticTexts["TagText"].label == "Tag2")
        
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        XCTAssertTrue(app.navigationBars["Edit Measurement"].waitForExistence(timeout: 3))
        
        XCTAssertTrue(app.buttons["SelectTagsButton"].label == "Tag2")
        
        app.buttons["SelectTagsButton"].tap()
        
        XCTAssertFalse(app.tables.cells.buttons["Tag1"].exists)
        
    }
    
    func testTagFilter() {
        app = makeApp()
        app.launch()
        
        let filterTagButton = app.buttons["FilterNavLink"]
        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        let withNoteCell = app.tables.cells.buttons["RecordCell-With note"]
        let emptyCell = app.tables.cells.buttons["RecordCell-Empty measurement"]
        
        filterTagButton.tap()
        
        let searchTextField = app.textFields["SearchTextfield"]
        let selectedTagText = app.staticTexts["SelectedTagText"]
        let tag1Cell = app.tables.cells.buttons["TagCell-Tag1"]
        let tag2Cell = app.tables.cells.buttons["TagCell-Tag2"]
        let tagnoneCell = app.tables.cells.buttons["TagCell-Tagnone"]
        
        XCTAssertTrue(tag1Cell.exists)
        XCTAssertTrue(tag2Cell.exists)
        XCTAssertTrue(tagnoneCell.exists)
        XCTAssertTrue(selectedTagText.label == "No tags")
        XCTAssertTrue(searchTextField.value as! String == "Search")
        
        searchTextField.tapEndCorner()
        searchTextField.tapEndCorner()
        searchTextField.typeTextLoop("tag")
        XCTAssertTrue(tag1Cell.exists)
        XCTAssertTrue(tag2Cell.exists)
        XCTAssertTrue(tagnoneCell.exists)
        searchTextField.typeTextLoop("t")
        XCTAssertFalse(tag1Cell.exists)
        XCTAssertFalse(tag2Cell.exists)
        XCTAssertFalse(tagnoneCell.exists)
        searchTextField.clearText("t")
        searchTextField.typeTextLoop("1")
        XCTAssertTrue(tag1Cell.exists)
        XCTAssertFalse(tag2Cell.exists)
        XCTAssertFalse(tagnoneCell.exists)
        tag1Cell.tap()
        XCTAssertTrue(selectedTagText.label == "Tag1")
        searchTextField.clearText("1")
        searchTextField.typeTextLoop("n")
        XCTAssertFalse(tag1Cell.exists)
        XCTAssertFalse(tag2Cell.exists)
        XCTAssertTrue(tagnoneCell.exists)
        tagnoneCell.tap()
        XCTAssertTrue(selectedTagText.label == "Tag1, Tagnone")
        tagnoneCell.tap()
        XCTAssertTrue(selectedTagText.label == "Tag1")
        searchTextField.clearText("n")
        searchTextField.typeTextLoop("1")
        XCTAssertTrue(tag1Cell.exists)
        XCTAssertFalse(tag2Cell.exists)
        XCTAssertFalse(tagnoneCell.exists)
        tag1Cell.tap()
        XCTAssertTrue(selectedTagText.label == "No tags")
        tag1Cell.tap()
        let backButton = app.navigationBars["Select Tags"].buttons["Measurements"]
        backButton.tap()
        
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertTrue(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        
        filterTagButton.tap()
        
        XCTAssertTrue(searchTextField.value as! String == "Search")
        XCTAssertTrue(selectedTagText.label == "Tag1")
        tag2Cell.tap()
        XCTAssertTrue(selectedTagText.label == "Tag1, Tag2")
        backButton.tap()
        
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        
        filterTagButton.tap()
        
        XCTAssertTrue(searchTextField.value as! String == "Search")
        XCTAssertTrue(selectedTagText.label == "Tag1, Tag2")
        tagnoneCell.tap()
        XCTAssertTrue(selectedTagText.label == "Tag1, Tag2, Tagnone")
        backButton.tap()
        
        XCTAssertFalse(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        
        filterTagButton.tap()
        
        XCTAssertTrue(searchTextField.value as! String == "Search")
        XCTAssertTrue(selectedTagText.label == "Tag1, Tag2, Tagnone")
        tag1Cell.tap()
        tag2Cell.tap()
        tagnoneCell.tap()
        XCTAssertTrue(selectedTagText.label == "No tags")
        backButton.tap()
        
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertTrue(withNoteCell.exists)
        XCTAssertTrue(emptyCell.exists)
    }
}
