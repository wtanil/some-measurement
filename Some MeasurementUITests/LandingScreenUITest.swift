//
//  LandingScreenUITest.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 04/04/22.
//

import XCTest

class LandingScreenUITest: XCTestCase {

    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
        app = makeAppForReset()
        app.launch()
        sleep(1)
        app.terminate()
        app = nil
    }
    
    override func tearDownWithError() throws {
        app = nil
        app = makeAppForReset()
        app.launch()
        app = nil
        try super.tearDownWithError()
    }
    
    // MARK: - UI Test
    // Confirm that "Skip" button is presented when the app is accessed for the first time and when tapped, segue to home screen
    func testLandingScreen_firstTime_tutorialView() {
        
        app = makeApp(useAppData: false)
        app.launch()
        
        let skipButton = app.buttons["SkipButton"]
        let tutorialWelcomeText = app.staticTexts["WelcomeText"]
        
        XCTAssertTrue(tutorialWelcomeText.exists)
        XCTAssertTrue(skipButton.isHittable)
        
        skipButton.tap()
        
        XCTAssertTrue(app.navigationBars["Measurements"].exists)
    }
    
    func testLandingPage_notFirstTime_measurementHome() {
        app = makeApp(useAppData: false)
        app.launch()
        app.buttons["SkipButton"].tap()
        sleep(1)
        app.terminate()
        sleep(1)
        app = nil
        app = makeApp(useAppData: false)
        app.launch()
        
        let tabBar = app.tabBars["Tab Bar"]
        let measurementTabBarButton = tabBar.buttons["Home"]
        let settingTabBarButton = tabBar.buttons["Setting"]
        
        let measurementsNavBar = app.navigationBars["Measurements"]
        let settingNavBar = app.navigationBars["Setting"]
        
        XCTAssertFalse(app.buttons["Skip"].exists)
        XCTAssertTrue(measurementTabBarButton.isHittable)
        XCTAssertTrue(settingTabBarButton.isHittable)
        XCTAssertTrue(measurementsNavBar.exists)
        
        settingTabBarButton.tap()
        
        XCTAssertFalse(measurementsNavBar.exists)
        XCTAssertTrue(settingNavBar.exists)
    }
}


/*
 // MARK: - UI Test
 // Confirm that "Skip" button is presented when the app is accessed for the first time and when tapped, segue to home screen
 func testLandingScreen_isFirstTime() throws {
 app = makeApp(skipTutorial: false)
 app.launch()
 
 let skipButton = app.buttons["Skip"]
 
 XCTAssertTrue(skipButton.isHittable)
 
 skipButton.tap()
 
 XCTAssertTrue(app.navigationBars["Measurements"].exists)
 }
 
 // Confirm that for when the next time app is accessed, landing screen is home screen
 func testLandingScreen_isNotFirstTime() throws {
 app = makeApp()
 app.launch()
 
 let tabBar = app.tabBars["Tab Bar"]
 let measurementTabBarButton = tabBar.buttons["Home"]
 let settingTabBarButton = tabBar.buttons["Setting"]
 
 let measurementsNavBar = app.navigationBars["Measurements"]
 let settingNavBar = app.navigationBars["Setting"]
 
 XCTAssertFalse(app.buttons["Skip"].exists)
 XCTAssertTrue(measurementTabBarButton.isHittable)
 XCTAssertTrue(settingTabBarButton.isHittable)
 XCTAssertTrue(measurementsNavBar.exists)
 
 settingTabBarButton.tap()
 
 XCTAssertFalse(measurementsNavBar.exists)
 XCTAssertTrue(settingNavBar.exists)
 }
 */
