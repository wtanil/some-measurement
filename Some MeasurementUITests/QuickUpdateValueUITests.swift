//
//  QuickUpdateValueUITests.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 20/04/22.
//

import XCTest

class QuickUpdateValueUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
    }
    
    override func tearDownWithError() throws {
        app = nil
        app = makeAppForReset()
        app.launch()
        app = nil
        try super.tearDownWithError()
    }
    
    // MARK: UI Tests
    // if qui is not 0, then show icon, if 0 then no icon
    func testHomeScreenIcon() {
        app = makeApp()
        app.launch()
        
        let homeNavBar = app.navigationBars["Measurements"]
        
        let emptyCell = app.tables.cells.buttons["RecordCell-Empty measurement"]
        let withCell = app.tables.cells.buttons["RecordCell-With note"]
        let fullNoteCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        
        XCTAssertFalse(emptyCell.images["QUIImage"].exists)
        XCTAssertTrue(withCell.images["QUIImage"].exists)
        XCTAssertTrue(fullNoteCell.images["QUIImage"].exists)
        
        let newRecordButton = app.navigationBars["Measurements"].buttons["NewRecordButton"]
        
        
        // new measurement without qui
        newRecordButton.tap()
        
        let titleTextField = app.textFields["TitleTextField"]
        let quiTextField = app.textFields["QUITextField"]
        let saveButton = app.buttons["SaveButton"]
        
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("qui1")
        saveButton.tap()
        
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui1"].exists)
        XCTAssertFalse(app.tables.cells.buttons["RecordCell-qui1"].images["QUIImage"].exists)
        
        app.tables.cells.buttons["RecordCell-qui1"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertFalse(app.buttons["IncreaseContextButton"].exists)
        XCTAssertFalse(app.buttons["DecreaseContextButton"].exists)
        
//        coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0))
        homeNavBar.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0)).tap()
        
//        app.navigationBars["Measurement"].buttons["Measurements"].tap()
        
        // new measurement with qui
        
        newRecordButton.tap()
        
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("qui2")
        titleTextField.swipeUp(velocity: .fast)
        quiTextField.tapEndCorner()
        quiTextField.tapEndCorner()
        quiTextField.clearText("0")
        quiTextField.typeTextLoop("22")
        saveButton.tap()
        
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui2"].exists)
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui2"].images["QUIImage"].exists)
        
        app.tables.cells.buttons["RecordCell-qui2"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertTrue(app.buttons["IncreaseContextButton"].exists)
        XCTAssertTrue(app.buttons["DecreaseContextButton"].exists)
        app.buttons["IncreaseContextButton"].tap()
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui2"].staticTexts["ValueText"].label == "22")
        
        // old measurement without qui -> with qui
        
        app.tables.cells.buttons["RecordCell-qui1"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertFalse(app.buttons["IncreaseContextButton"].exists)
        XCTAssertFalse(app.buttons["DecreaseContextButton"].exists)
        app.buttons["EditContextButton"].tap()
        
        let quiTextField2 = app.textFields["QUITextField"]
        quiTextField2.tap()
        quiTextField2.clearText("0")
        quiTextField2.typeTextLoop("11")
        app.buttons["SaveButton"].tap()
        
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui1"].exists)
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui1"].images["QUIImage"].exists)
        
        app.tables.cells.buttons["RecordCell-qui1"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertTrue(app.buttons["IncreaseContextButton"].exists)
        XCTAssertTrue(app.buttons["DecreaseContextButton"].exists)
        app.buttons["IncreaseContextButton"].tap()
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui1"].staticTexts["ValueText"].label == "11")
        
        // old measurement with qui -> another qui
        
        app.tables.cells.buttons["RecordCell-qui1"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertTrue(app.buttons["IncreaseContextButton"].exists)
        XCTAssertTrue(app.buttons["DecreaseContextButton"].exists)
        app.buttons["EditContextButton"].tap()
        
        app.textFields["QUITextField"].tap()
        app.textFields["QUITextField"].typeTextLoop("1")
        app.buttons["SaveButton"].tap()
        
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui1"].exists)
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui1"].images["QUIImage"].exists)
        
        app.tables.cells.buttons["RecordCell-qui1"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertTrue(app.buttons["IncreaseContextButton"].exists)
        XCTAssertTrue(app.buttons["DecreaseContextButton"].exists)
        app.buttons["IncreaseContextButton"].tap()
        XCTAssertTrue(app.tables.cells.buttons["RecordCell-qui1"].staticTexts["ValueText"].label == "122")
        
        // old measurement with qui -> without qui
        app.tables.cells.buttons["RecordCell-qui2"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertTrue(app.buttons["IncreaseContextButton"].exists)
        XCTAssertTrue(app.buttons["DecreaseContextButton"].exists)
        app.buttons["EditContextButton"].tap()
        
        app.textFields["QUITextField"].tap()
        app.textFields["QUITextField"].clearText("22")
        app.textFields["QUITextField"].typeTextLoop("0")
        app.buttons["SaveButton"].tap()
        
        app.tables.cells.buttons["RecordCell-qui2"].press(forDuration: 1)
        XCTAssertTrue(app.buttons["EditContextButton"].exists)
        XCTAssertFalse(app.buttons["IncreaseContextButton"].exists)
        XCTAssertFalse(app.buttons["DecreaseContextButton"].exists)
        
    }
}
