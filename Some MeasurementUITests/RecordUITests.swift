//
//  RecordUITests.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 06/04/22.
//

import XCTest

class RecordUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
    }
    
    override func tearDownWithError() throws {
        app = nil
        app = makeAppForReset()
        app.launch()
        app = nil
        try super.tearDownWithError()
    }
    
    // MARK: UI Test
    func testCreateRecord() {
        app = makeApp()
        app.launch()
        
        let newRecordButton = app.navigationBars["Measurements"].buttons["NewRecordButton"]
        newRecordButton.tap()
        
        let titleTextField = app.textFields["TitleTextField"]
        let noteTextField = app.textFields["NoteTextField"]
        let selectUnitButton = app.buttons["SelectUnitButton"]
        let selectTagsButton = app.buttons["SelectTagsButton"]
        let quiTextField = app.textFields["QUITextField"]
        let saveButton = app.buttons["SaveButton"]
        
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(saveButton.isEnabled)
        
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("new")
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        let newCell = app.tables.cells.buttons["RecordCell-new"]
        XCTAssertTrue(newCell.exists)
        
        // new full record
        newRecordButton.tap()
        
        // TEST validation
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeTextLoop("new")
        XCTAssertTrue(saveButton.isEnabled)
        saveButton.tap()
        
        let alert = app.alerts["Measurement with a same title already exists"]
        let renameButton = alert.buttons["Rename"]
        
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        
        // Dismiss alert
        renameButton.tap()
        
        titleTextField.clearText("new")
        XCTAssertFalse(saveButton.isEnabled)
        
        titleTextField.typeTextLoop("full")
        noteTextField.tap()
        noteTextField.typeTextLoop("notes")
        // unit selection
        selectUnitButton.tap()
        
        let selectedUnitCell = app.tables.buttons["UnitCell-unit"]
        selectedUnitCell.tap()
        
        XCTAssertTrue(app.staticTexts["SelectedUnitText"].label == "unit")
        
        app.navigationBars["Select a Unit"].buttons["New Measurement"].tap()
        
        XCTAssertTrue(selectUnitButton.label == "unit")
        
        // tag selection
        selectTagsButton.tap()
        
        let selectedTag1Cell = app.tables.buttons["TagCell-Tag1"]
        let selectedTag2Cell = app.tables.buttons["TagCell-Tag2"]
        selectedTag1Cell.tap()
        
        XCTAssertTrue(app.staticTexts["SelectedTagText"].label == "Tag1")
        
        selectedTag2Cell.tap()
        
        XCTAssertTrue(app.staticTexts["SelectedTagText"].label == "Tag1, Tag2")
        
        app.navigationBars["Select Tags"].buttons["New Measurement"].tap()
        
        XCTAssertTrue(selectTagsButton.label == "Tag1, Tag2")
        
        selectTagsButton.swipeUp(velocity: .fast)
        
        // QUI
        
        quiTextField.tapEndCorner()
        quiTextField.tapEndCorner()
        quiTextField.clearText("0")
        quiTextField.typeText("1")

        saveButton.tap()
        
        let fullTestCell = app.tables.cells.buttons["RecordCell-full"]
        XCTAssertTrue(fullTestCell.exists)
        
        fullTestCell.tap()
        XCTAssertTrue(app.staticTexts["full"].exists)
        XCTAssertTrue(app.staticTexts["TagText"].exists)
        XCTAssertTrue(app.staticTexts["TagText"].label == "Tag1, Tag2")
        XCTAssertTrue(app.staticTexts["ValueDetailText"].label == "Value: None")
        
//        XCTAssertTrue(titleTextField.value as! String == "Full measurements")
//        XCTAssertTrue(selectUnitButton.label == "none")
//        XCTAssertTrue(selectTagsButton.label == "Tag1")
//        XCTAssertTrue(quiTextField.value as! String == "1")
//        XCTAssertFalse(quiTextField.isEnabled)
        
    }
    
    func testEditRecord() {
        
        app = makeApp()
        app.launch()
        
        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        XCTAssertTrue(fullMeasurementCell.exists)
        
        fullMeasurementCell.tap()
        
        let detailNavBar = app.navigationBars["Measurement"]
        XCTAssertTrue(detailNavBar.exists)
        detailNavBar.buttons["MoreBarButton"].tap()
        XCTAssertTrue(detailNavBar.buttons["MoreBarButton"].exists)
        // Action Sheet
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        XCTAssertTrue(actionSheet.scrollViews.otherElements.buttons["Edit"].exists)
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        // Editor
        
        let titleTextField = app.textFields["TitleTextField"]
        let noteTextField = app.textFields["NoteTextField"]
        let selectUnitButton = app.buttons["SelectUnitButton"]
        let selectTagsButton = app.buttons["SelectTagsButton"]
        let quiTextField = app.textFields["QUITextField"]
        let quiText = app.staticTexts["QUIText"]
        let saveButton = app.buttons["SaveButton"]
        
        XCTAssertTrue(saveButton.isEnabled)
        XCTAssertTrue(titleTextField.value as! String == "Full measurement")
        XCTAssertTrue((noteTextField.value as! String).contains("This is a note."))
        XCTAssertTrue(selectUnitButton.label == "unit")
        XCTAssertTrue(selectTagsButton.label == "Tag1, Tag2")
        XCTAssertTrue(quiText.exists)
        XCTAssertTrue(quiText.label == "1")
        
        // test empty title textfield
        titleTextField.tapEndCorner()
        titleTextField.tapEndCorner()
        titleTextField.typeText("s")
        XCTAssertTrue(saveButton.isEnabled)
        
//        noteTextField.tap()
        let previousNote = noteTextField.value as! String
////        noteTextField.clearText(previousNote)
////        noteTextField.typeTextLoop("newnote")
        noteTextField.tapVeryStartCorner()
        noteTextField.tapVeryStartCorner()
        noteTextField.typeTextLoop("a")
        let newNote = "a" + previousNote
        
        // unit
        selectUnitButton.tap()
        
        let selectedUnitCell = app.tables.buttons["UnitCell-none"]
        selectedUnitCell.tap()
        
        XCTAssertTrue(app.staticTexts["SelectedUnitText"].label == "none")
        
        app.navigationBars["Select a Unit"].buttons["Edit Measurement"].tap()
        
        XCTAssertTrue(selectUnitButton.label == "none")
        
        // tag
        selectTagsButton.tap()
        
        let selectedTag2Cell = app.tables.buttons["TagCell-Tag2"]
        selectedTag2Cell.tap()
        
        XCTAssertTrue(app.staticTexts["SelectedTagText"].label == "Tag1")
        
        app.navigationBars["Select Tags"].buttons["Edit Measurement"].tap()
        
        XCTAssertTrue(selectTagsButton.label == "Tag1")
        
        // QUI - editing qui is moved
        XCTAssertTrue(quiText.exists)
        XCTAssertFalse(quiTextField.exists)
//        quiTextField.tap()
//        quiTextField.clearText("1")
//        quiTextField.typeText("2")
        
        saveButton.tap()
        
        XCTAssertFalse(app.staticTexts["Full measurement"].exists)
        XCTAssertTrue(app.staticTexts["Full measurements"].exists)
        XCTAssertTrue(app.staticTexts["NoteText"].exists)
        XCTAssertTrue(app.staticTexts["NoteText"].label == newNote)
        XCTAssertTrue(app.staticTexts["TagText"].exists)
        XCTAssertFalse(app.staticTexts["TagText"].label == "Tag1, Tag2")
        XCTAssertTrue(app.staticTexts["TagText"].label == "Tag1")
        
        // editor
        detailNavBar.buttons["MoreBarButton"].tap()
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        
        XCTAssertTrue(titleTextField.value as! String == "Full measurements")
        XCTAssertTrue(noteTextField.value as! String == newNote)
        XCTAssertTrue(selectUnitButton.label == "none")
        XCTAssertTrue(selectTagsButton.label == "Tag1")
        XCTAssertFalse(quiTextField.exists)
        XCTAssertTrue(quiText.label == "1")
        
        let editMeasurementsNavBar = app.navigationBars["Edit Measurement"]
        XCTAssertTrue(editMeasurementsNavBar.exists)
        editMeasurementsNavBar.swipeToDismiss()
        app.navigationBars["Measurement"].buttons["Measurements"].tap()

        XCTAssertFalse(app.cells.buttons["RecordCell-Full measurement"].exists)
        XCTAssertTrue(app.cells.buttons["RecordCell-Full measurements"].exists)
        
    }
    
    func testRecordSearch() {
        app = makeApp()
        app.launch()
        
        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        let withNoteCell = app.tables.cells.buttons["RecordCell-With note"]
        let emptyCell = app.tables.cells.buttons["RecordCell-Empty measurement"]
        
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertTrue(withNoteCell.exists)
        XCTAssertTrue(emptyCell.exists)
        
        let searchToggleButton = app.buttons["SearchToggleButton"]
        XCTAssertTrue(searchToggleButton.exists)
        searchToggleButton.tap()
        let searchTextField = app.textFields["SearchTextField"]
        let searchClearButton = app.buttons["SearchClearButton"]
        XCTAssertTrue(searchTextField.exists)
        XCTAssertTrue(searchClearButton.exists)
        
        searchToggleButton.tap()
        XCTAssertFalse(searchTextField.exists)
        XCTAssertFalse(searchClearButton.exists)
        searchToggleButton.tap()
        searchTextField.tapEndCorner()
        searchTextField.tapEndCorner()
        searchTextField.typeTextLoop("x")
        XCTAssertFalse(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        searchTextField.clearText("x")
        searchTextField.typeTextLoop("ful")
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        searchToggleButton.tap()
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        searchToggleButton.tap()
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        searchTextField.tapEndCorner()
        searchTextField.tapEndCorner()
        searchTextField.typeTextLoop("1")
        XCTAssertFalse(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertFalse(emptyCell.exists)
        searchClearButton.tap()
        XCTAssertTrue(searchTextField.value as! String == "Search")
        
        searchTextField.typeTextLoop("em")
        XCTAssertTrue(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertTrue(emptyCell.exists)
        searchTextField.typeTextLoop("p")
        XCTAssertFalse(fullMeasurementCell.exists)
        XCTAssertFalse(withNoteCell.exists)
        XCTAssertTrue(emptyCell.exists)
    }
    
    func testSort() {
        app = makeApp()
        app.launch()
        
        let sortButton = app.buttons["SortNavLink"]
        //        let fullMeasurementCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        //        let withNoteCell = app.tables.cells.buttons["RecordCell-With note"]
        //        let emptyCell = app.tables.cells.buttons["RecordCell-Empty measurement"]
        
        sortButton.tap()
        
        let titleSortButton = app.buttons["SortCell-Title"]
        let dateLatestSortButton = app.buttons["SortCell-Date Added (Latest)"]
        let dateOldestSortButton = app.buttons["SortCell-Date Added (Oldest)"]
        let recentSortButton = app.buttons["SortCell-Recently Updated"]
        XCTAssertTrue(titleSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(dateLatestSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(dateOldestSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(recentSortButton.images["CheckmarkImage"].exists)
        dateLatestSortButton.tap()
        XCTAssertFalse(titleSortButton.images["CheckmarkImage"].exists)
        XCTAssertTrue(dateLatestSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(dateOldestSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(recentSortButton.images["CheckmarkImage"].exists)
        dateOldestSortButton.tap()
        XCTAssertFalse(titleSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(dateLatestSortButton.images["CheckmarkImage"].exists)
        XCTAssertTrue(dateOldestSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(recentSortButton.images["CheckmarkImage"].exists)
        recentSortButton.tap()
        XCTAssertFalse(titleSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(dateLatestSortButton.images["CheckmarkImage"].exists)
        XCTAssertFalse(dateOldestSortButton.images["CheckmarkImage"].exists)
        XCTAssertTrue(recentSortButton.images["CheckmarkImage"].exists)
        
        let backButton = app.navigationBars["Sort Options"].buttons["Measurements"]
        
        titleSortButton.tap()
        
        backButton.tap()
        
        /*
         title: empty, full, with
         latest: full, with, empty
         oldest: empty, with, full
         recent: full, with, empty
         */
        XCTAssertTrue(app.navigationBars["Measurements"].waitForExistence(timeout: 3))
        var cellArray = app.tables.cells.allElementsBoundByIndex
        XCTAssertTrue(cellArray[0].staticTexts["RecordCellTitleText"].label == "Empty measurement")
        XCTAssertTrue(cellArray[1].staticTexts["RecordCellTitleText"].label == "Full measurement")
        XCTAssertTrue(cellArray[2].staticTexts["RecordCellTitleText"].label == "With note")
        
        sortButton.tap()
        dateLatestSortButton.tap()
        backButton.tap()
        
        cellArray = app.tables.cells.allElementsBoundByIndex
        XCTAssertTrue(cellArray[0].staticTexts["RecordCellTitleText"].label == "Full measurement")
        XCTAssertTrue(cellArray[1].staticTexts["RecordCellTitleText"].label == "With note")
        XCTAssertTrue(cellArray[2].staticTexts["RecordCellTitleText"].label == "Empty measurement")
        
        sortButton.tap()
        dateOldestSortButton.tap()
        backButton.tap()
        cellArray = app.tables.cells.allElementsBoundByIndex
        XCTAssertTrue(cellArray[0].staticTexts["RecordCellTitleText"].label == "Empty measurement")
        XCTAssertTrue(cellArray[1].staticTexts["RecordCellTitleText"].label == "With note")
        XCTAssertTrue(cellArray[2].staticTexts["RecordCellTitleText"].label == "Full measurement")
        
        sortButton.tap()
        recentSortButton.tap()
        backButton.tap()
        cellArray = app.tables.cells.allElementsBoundByIndex
        XCTAssertTrue(cellArray[0].staticTexts["RecordCellTitleText"].label == "Full measurement")
        XCTAssertTrue(cellArray[1].staticTexts["RecordCellTitleText"].label == "With note")
        XCTAssertTrue(cellArray[2].staticTexts["RecordCellTitleText"].label == "Empty measurement")
        
        let emptyMeasurementCell = app.tables.cells.buttons["RecordCell-Empty measurement"]
        emptyMeasurementCell.tap()
        
        XCTAssertTrue(app.navigationBars["Measurement"].exists)
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        XCTAssertTrue(app.sheets["More options"].scrollViews.otherElements.buttons["Edit"].exists)
        app.sheets["More options"].scrollViews.otherElements.buttons["Edit"].tap()
        
        XCTAssertTrue(app.navigationBars["Edit Measurement"].waitForExistence(timeout: 3))
        
        let noteTextField = app.textFields["NoteTextField"]
        XCTAssertTrue(noteTextField.exists)
        noteTextField.tapEndCorner()
        noteTextField.tapEndCorner()
        noteTextField.typeTextLoop("a")
        
        app.buttons["SaveButton"].tap()
        app.navigationBars["Measurement"].buttons["Measurements"].tap()
        
        cellArray = app.tables.cells.allElementsBoundByIndex
        XCTAssertTrue(cellArray[0].staticTexts["RecordCellTitleText"].label == "Empty measurement")
        XCTAssertTrue(cellArray[1].staticTexts["RecordCellTitleText"].label == "Full measurement")
        XCTAssertTrue(cellArray[2].staticTexts["RecordCellTitleText"].label == "With note")
        
        
    }
}


//        titleTextField.typeTextLoop("Full measurement")
//
//        XCTAssertTrue(saveButton.isEnabled)
//
//        saveButton.tap()
//
//        let alert = app.alerts["Measurement with a same title already exists"]
//        let renameButton = alert.buttons["Rename"]
//
//        XCTAssertTrue(alert.exists)
//        XCTAssertTrue(renameButton.exists)
//
//        // Dismiss alert
//        renameButton.tap()
//
//        titleTextField.tap()
//        var emptyString = String(repeating: XCUIKeyboardKey.delete.rawValue, count: "Full measurement".count)
//        titleTextField.typeTextLoop(emptyString)
//        XCTAssertFalse(saveButton.isEnabled)
//        titleTextField.typeTextLoop("New")
//
//        XCTAssertTrue(saveButton.isEnabled)
//
//        saveButton.tap()
//
//        let newMeasurementCell = app.tables.cells.buttons["RecordCell-New"]
//        XCTAssertTrue(newMeasurementCell.exists)


// new record all



//    func testasd() {
//
//        let app = XCUIApplication()
//        app/*@START_MENU_TOKEN@*/.buttons["SkipButton"]/*[[".buttons[\"Skip\"]",".buttons[\"SkipButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["Measurements"]/*@START_MENU_TOKEN@*/.buttons["NewRecordButton"]/*[[".buttons[\"Add\"]",".buttons[\"NewRecordButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let tablesQuery = app.tables
//        tablesQuery/*@START_MENU_TOKEN@*/.textFields["TitleTextField"]/*[[".cells[\"Title, Error\"]",".textFields[\"Title\"]",".textFields[\"TitleTextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["New Measurement"]/*@START_MENU_TOKEN@*/.buttons["SaveButton"]/*[[".buttons[\"Add\"]",".buttons[\"SaveButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        tablesQuery/*@START_MENU_TOKEN@*/.buttons["RecordCell-asd"]/*[[".cells[\"asd, -, -\"]",".buttons[\"asd, -, -\"]",".buttons[\"RecordCell-asd\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["Measurement"]/*@START_MENU_TOKEN@*/.buttons["MoreBarButton"]/*[[".buttons[\"More\"]",".buttons[\"MoreBarButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.sheets["More options"].scrollViews.otherElements.buttons["Edit"].tap()
//
//        let editMeasurementNavigationBar = app.navigationBars["Edit Measurement"]
//        let editMeasurementStaticText = editMeasurementNavigationBar.staticTexts["Edit Measurement"]
//        editMeasurementStaticText.tap()
//        editMeasurementStaticText.tap()
//        editMeasurementNavigationBar.children(matching: .other).element.swipeDown()
//
//
//    }
//
//    func testASD() {
//
//        let app = XCUIApplication()
//        app.collectionViews/*@START_MENU_TOKEN@*/.cells/*[[".scrollViews.cells",".cells"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .other).element.children(matching: .other).element.tap()
//        app/*@START_MENU_TOKEN@*/.buttons["SkipButton"]/*[[".buttons[\"Skip\"]",".buttons[\"SkipButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let measurementsNavigationBar = app.navigationBars["Measurements"]
//        measurementsNavigationBar/*@START_MENU_TOKEN@*/.buttons["NewRecordButton"]/*[[".buttons[\"Add\"]",".buttons[\"NewRecordButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let tablesQuery = app.tables
//        tablesQuery/*@START_MENU_TOKEN@*/.textFields["TitleTextField"]/*[[".cells[\"Title, Error\"]",".textFields[\"Title\"]",".textFields[\"TitleTextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["New Measurement"]/*@START_MENU_TOKEN@*/.buttons["SaveButton"]/*[[".buttons[\"Add\"]",".buttons[\"SaveButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        tablesQuery/*@START_MENU_TOKEN@*/.buttons["RecordCell-asd"]/*[[".cells[\"asd, -, -\"]",".buttons[\"asd, -, -\"]",".buttons[\"RecordCell-asd\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["Measurement"].buttons["Measurements"].tap()
//        measurementsNavigationBar.staticTexts["Measurements"].tap()
//
//    }

//    func testASD() {
//
//        let app = XCUIApplication()
//        app.windows.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.tap()
//        app.navigationBars["Measurements"]/*@START_MENU_TOKEN@*/.buttons["NewRecordButton"]/*[[".buttons[\"Add\"]",".buttons[\"NewRecordButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let tablesQuery = app.tables
//        tablesQuery/*@START_MENU_TOKEN@*/.textFields["NoteTextField"]/*[[".cells[\"Note\"]",".textFields[\"Note\"]",".textFields[\"NoteTextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let titletextfieldTextField = tablesQuery/*@START_MENU_TOKEN@*/.textFields["TitleTextField"]/*[[".cells[\"Title, Error\"]",".textFields[\"Title\"]",".textFields[\"TitleTextField\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
//        titletextfieldTextField.tap()
//        titletextfieldTextField.tap()
//        titletextfieldTextField.tap()
//        titletextfieldTextField.tap()
//        app.navigationBars["New Measurement"]/*@START_MENU_TOKEN@*/.buttons["SaveButton"]/*[[".buttons[\"Add\"]",".buttons[\"SaveButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        tablesQuery/*@START_MENU_TOKEN@*/.buttons["RecordCell-asd"]/*[[".cells[\"asd, -, -\"]",".buttons[\"asd, -, -\"]",".buttons[\"RecordCell-asd\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//
//        let elementsQuery = app.scrollViews.otherElements
//        let asdStaticText = elementsQuery.staticTexts["asd"]
//        asdStaticText.tap()
//        elementsQuery.staticTexts["No tags"].tap()
//
//        let valueNoneStaticText = elementsQuery.staticTexts["Value: None"]
//        valueNoneStaticText.tap()
//        valueNoneStaticText.tap()
//        elementsQuery.buttons["Add"].tap()
//
//        let newValueTextField = elementsQuery.textFields["New Value"]
//        newValueTextField.tap()
//        newValueTextField.tap()
//        elementsQuery.buttons["none"].tap()
//        app.navigationBars["Select a Unit"].buttons["Measurement"].tap()
//
//        let updateButton = elementsQuery.buttons["Update"]
//        updateButton.tap()
//        updateButton.tap()
//        asdStaticText.tap()
//
//    }

// Action sheet on detail
//        let app = XCUIApplication()
//        app.tables/*@START_MENU_TOKEN@*/.buttons["RecordCell-Full measurement"]/*[[".cells[\"Full measurement, Mar 29, 2022 at 12:50:23 PM, 11 unit\"]",".buttons[\"Full measurement, Mar 29, 2022 at 12:50:23 PM, 11 unit\"]",".buttons[\"RecordCell-Full measurement\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["Measurement"].buttons["More"].tap()
//        app.sheets["More options"].scrollViews.otherElements.buttons["Edit"].tap()
//        app.navigationBars["Edit Measurement"].children(matching: .other).element.swipeDown()
