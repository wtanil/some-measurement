//
//  UnitUITests.swift
//  Some MeasurementUITests
//
//  Created by William Suryadi Tanil on 26/04/22.
//

import XCTest

class UnitUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
    }
    
    override func tearDownWithError() throws {
        app = nil
        app = makeAppForReset()
        
        app.launch()
        
        app = nil
        
        try super.tearDownWithError()
    }
    
    // MARK: UI Test
    func testCreateUnit() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        // Setting
        app.tables.buttons["ManageUnitsCell"].tap()
        // Unit Home
        XCTAssertTrue(app.navigationBars["Manage Units"].waitForExistence(timeout: 3))
        app.navigationBars["Manage Units"].buttons["NewUnitButton"].tap()
        // Unit Editor .create
        XCTAssertTrue(app.navigationBars["New Unit"].waitForExistence(timeout: 3))
        let nameTextField = app.textFields["TitleTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        
        // When in .create mode, save button should exist but not hittable, delete button should not exist
        XCTAssertTrue(saveButton.exists)
        XCTAssertFalse(deleteButton.exists)
        // Save button should be disabled when name is not valid
        XCTAssertFalse(saveButton.isEnabled)
        
        // Input not unique name
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        // Note: Disable auto-correction on test device when testing
        nameTextField.typeTextLoop("none")
        
        // Save button should be enabled when title is valid even when name is not unique
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        // A warning alert should be displayed because name is not unique
        let alert = app.alerts["Unit already exists"]
        let renameButton = alert.buttons["Rename"]
        
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        
        // Dismiss alert
        renameButton.tap()
        
        // Rename name to a valid one
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        // Note: Disable auto-correction on test device when testing
        nameTextField.typeText("s")
        
        saveButton.tap()
        
        let nonesCell = app.tables.cells.buttons["UnitCell-nones"]
        XCTAssertTrue(nonesCell.exists)
        XCTAssertTrue(nonesCell.isEnabled)
    }
    
    func testEditUnit_empty() {
        app = makeApp()
        app.launch()
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        // Setting
        app.tables.buttons["ManageUnitsCell"].tap()
        // Unit Home
        XCTAssertTrue(app.navigationBars["Manage Units"].waitForExistence(timeout: 3))
        let noneCell = app.tables.cells.buttons["UnitCell-none"]
        let nothingCell = app.tables.cells.buttons["UnitCell-nothing"]
        
        XCTAssertTrue(noneCell.exists)
        XCTAssertFalse(noneCell.isEnabled)
        XCTAssertTrue(nothingCell.exists)
        XCTAssertTrue(nothingCell.isEnabled)
        
        nothingCell.tap()
        XCTAssertTrue(app.navigationBars["Edit Unit"].waitForExistence(timeout: 3))
        
        let unitEditorTitle = app.navigationBars["Edit Unit"].staticTexts["Edit Unit"]
        XCTAssertTrue(unitEditorTitle.exists)
        
        let nameTextField = app.textFields["TitleTextField"]
        let saveButton = app.buttons["SaveButton"]
        let deleteButton = app.buttons["DeleteButton"]
        XCTAssertTrue(saveButton.exists)
        XCTAssertTrue(deleteButton.exists)
        // Save button should be disabled when name is not valid
        XCTAssertTrue(saveButton.isEnabled)
        
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        // Note: Disable auto-correction on test device when testing
        var emptyString = String(repeating: XCUIKeyboardKey.delete.rawValue, count: "nothing".count)
        nameTextField.typeTextLoop(emptyString)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("none")
        
        // Save button should be enabled when title is valid even when name is not unique
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        // A warning alert should be displayed because name is not unique
        let alert = app.alerts["Unit already exists"]
        let renameButton = alert.buttons["Rename"]
        
        XCTAssertTrue(alert.exists)
        XCTAssertTrue(renameButton.exists)
        
        // Dismiss alert
        renameButton.tap()
        
        // Rename name to a valid one
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        emptyString = String(repeating: XCUIKeyboardKey.delete.rawValue, count: "none".count)
        nameTextField.typeTextLoop(emptyString)
        XCTAssertFalse(saveButton.isEnabled)
        nameTextField.typeTextLoop("new")
        
        XCTAssertTrue(saveButton.isEnabled)
        
        saveButton.tap()
        
        let newCell = app.tables.cells.buttons["UnitCell-new"]
        XCTAssertFalse(nothingCell.exists)
        XCTAssertTrue(newCell.exists)
        XCTAssertTrue(newCell.isEnabled)
        
        newCell.tap()
        XCTAssertTrue(app.navigationBars["Edit Unit"].waitForExistence(timeout: 3))
        
        let delete2Button = app.buttons["DeleteButton"]
        XCTAssertTrue(delete2Button.exists)
        delete2Button.tap()
        
        let deleteAlert = app.alerts["Warning"]
        let alertCancelButton = deleteAlert.buttons["Cancel"]
        let alertDeleteButton = deleteAlert.buttons["Delete"]
        
        XCTAssertTrue(deleteAlert.exists)
        XCTAssertTrue(alertCancelButton.exists)
        XCTAssertTrue(alertDeleteButton.exists)
        
        alertCancelButton.tap()
        delete2Button.tap()
        alertDeleteButton.tap()
        
        XCTAssertFalse(newCell.exists)
        
    }
    
    func testEditUnit_used() {
        app = makeApp()
        app.launch()
        
        let fullCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        XCTAssertTrue(fullCell.exists)
        XCTAssertTrue(fullCell.staticTexts["ValueText"].label == "11 unit")
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        app.tables.buttons["ManageUnitsCell"].tap()
        XCTAssertTrue(app.navigationBars["Manage Units"].waitForExistence(timeout: 3))
        
        let unitCell = app.tables.cells.buttons["UnitCell-unit"]
        unitCell.tap()
        
        XCTAssertTrue(app.navigationBars["Edit Unit"].waitForExistence(timeout: 3))
        
        let nameTextField = app.textFields["TitleTextField"]
        let saveButton = app.buttons["SaveButton"]
        XCTAssertTrue(saveButton.exists)
        
        nameTextField.tapEndCorner()
        nameTextField.tapEndCorner()
        nameTextField.clearText("t")
        saveButton.tap()
        XCTAssertFalse(app.tables.cells.buttons["UnitCell-unit"].exists)
        XCTAssertTrue(app.tables.cells.buttons["UnitCell-uni"].exists)
        
        app.tabBars["Tab Bar"].buttons["Home"].tap()
        XCTAssertTrue(fullCell.staticTexts["ValueText"].label == "11 uni")
        
        fullCell.tap()
        XCTAssertTrue(app.navigationBars["Measurement"].waitForExistence(timeout: 3))
        
        // check in value in line editor
        let valueText = app.staticTexts["ValueDetailText"]
        let valueToggleButton = app.buttons["ValueToggleButton"]
        //        let selectUnitButton = app.buttons["SelectUnitButton"]
        XCTAssertTrue(valueText.label == "Value: 11 uni")
        valueToggleButton.tap()
        XCTAssertTrue(app.buttons["InlineSelectUnitButton"].label == "uni")
        
        // check in measurement editor
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        XCTAssertTrue(app.buttons["SelectUnitButton"].label == "uni")
        app.navigationBars["Edit Measurement"].swipeToDismiss()
        
        // check in history
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        actionSheet.scrollViews.otherElements.buttons["History"].tap()
        XCTAssertTrue(app.navigationBars["History"].waitForExistence(timeout: 3))
        XCTAssertTrue(app.tables.cells.buttons["ValueCell-11 uni"].waitForExistence(timeout: 3))
        XCTAssertFalse(app.tables.cells.buttons["ValueCell-11 unit"].exists)
        XCTAssertFalse(app.tables.cells.buttons["ValueCell-27 unit"].exists)
        XCTAssertTrue(app.tables.cells.buttons["ValueCell-11 uni"].exists)
        XCTAssertTrue(app.tables.cells.buttons["ValueCell-27 uni"].exists)
        app.tables.cells.buttons["ValueCell-11 uni"].tap()
        XCTAssertTrue(app.buttons["SelectUnitButton"].label == "uni")
        
    }
    
    func testDeleteUnit() {
        app = makeApp()
        app.launch()
        
        let fullCell = app.tables.cells.buttons["RecordCell-Full measurement"]
        XCTAssertTrue(fullCell.exists)
        XCTAssertTrue(fullCell.staticTexts["ValueText"].label == "11 unit")
        
        app.tabBars["Tab Bar"].buttons["Setting"].tap()
        app.tables.buttons["ManageUnitsCell"].tap()
        XCTAssertTrue(app.navigationBars["Manage Units"].waitForExistence(timeout: 3))
        let unitCell = app.tables.cells.buttons["UnitCell-unit"]
        unitCell.tap()
        
        XCTAssertTrue(app.navigationBars["Edit Unit"].waitForExistence(timeout: 3))
        
        XCTAssertTrue(app.buttons["DeleteButton"].exists)
        app.buttons["DeleteButton"].tap()
        app.alerts["Warning"].buttons["Delete"].tap()
        
        XCTAssertFalse(unitCell.exists)
        
        app.tabBars["Tab Bar"].buttons["Home"].tap()
        XCTAssertTrue(fullCell.staticTexts["ValueText"].label == "11")
        
        fullCell.tap()
        XCTAssertTrue(app.navigationBars["Measurement"].waitForExistence(timeout: 3))
        // check in value in line editor
        let valueText = app.staticTexts["ValueDetailText"]
        let valueToggleButton = app.buttons["ValueToggleButton"]
//        let selectUnitButton = app.buttons["SelectUnitButton"]
        XCTAssertTrue(valueText.label == "Value: 11")
        valueToggleButton.tap()
        XCTAssertTrue(app.buttons["InlineSelectUnitButton"].label == "none")
        
        // check in measurement editor
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        
        let actionSheet = app.sheets["More options"]
        XCTAssertTrue(actionSheet.waitForExistence(timeout: 3))
        actionSheet.scrollViews.otherElements.buttons["Edit"].tap()
        XCTAssertTrue(app.navigationBars["Edit Measurement"].waitForExistence(timeout: 3))
        XCTAssertTrue(app.buttons["SelectUnitButton"].label == "none")
        app.navigationBars["Edit Measurement"].swipeToDismiss()
        
        // check in history
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        actionSheet.scrollViews.otherElements.buttons["History"].tap()
        XCTAssertTrue(app.navigationBars["History"].waitForExistence(timeout: 3))
        XCTAssertTrue(app.tables.cells.buttons["ValueCell-11"].waitForExistence(timeout: 3))
        XCTAssertFalse(app.tables.cells.buttons["ValueCell-11 unit"].exists)
        XCTAssertFalse(app.tables.cells.buttons["ValueCell-27 unit"].exists)
        XCTAssertTrue(app.tables.cells.buttons["ValueCell-11"].exists)
        XCTAssertTrue(app.tables.cells.buttons["ValueCell-27"].exists)
        app.tables.cells.buttons["ValueCell-11"].tap()
        XCTAssertTrue(app.buttons["SelectUnitButton"].label == "none")
        
    }
    
    
    func testUnitSelectSearch() {
        app = makeApp()
        app.launch()
        
        let emptyMeasurementCell = app.tables.cells.buttons["RecordCell-Empty measurement"]
        emptyMeasurementCell.tap()
        
        XCTAssertTrue(app.navigationBars["Measurement"].waitForExistence(timeout: 3))
        app.navigationBars["Measurement"].buttons["MoreBarButton"].tap()
        XCTAssertTrue(app.sheets["More options"].scrollViews.otherElements.buttons["Edit"].exists)
        app.sheets["More options"].scrollViews.otherElements.buttons["Edit"].tap()
        
        XCTAssertTrue(app.navigationBars["Edit Measurement"].waitForExistence(timeout: 3))
        
        let selectedUnitButton = app.buttons["SelectUnitButton"]
        XCTAssertTrue(selectedUnitButton.exists)
        selectedUnitButton.tap()
        
        let selectedUnitText = app.staticTexts["SelectedUnitText"]
        let searchTextField = app.textFields["SearchTextField"]
        let noneCell = app.buttons["UnitCell-none"]
        let nothingCell = app.buttons["UnitCell-nothing"]
        let unitCell = app.buttons["UnitCell-unit"]
        
        
        
        XCTAssertTrue(selectedUnitText.label == "none")
        XCTAssertTrue(searchTextField.value as! String == "Search")
        XCTAssertTrue(noneCell.images["CheckmarkImage"].exists)
        XCTAssertFalse(nothingCell.images["CheckmarkImage"].exists)
        XCTAssertFalse(unitCell.images["CheckmarkImage"].exists)
        
        searchTextField.tapEndCorner()
        searchTextField.tapEndCorner()
        searchTextField.typeTextLoop("n")
        XCTAssertTrue(noneCell.exists)
        XCTAssertTrue(nothingCell.exists)
        XCTAssertTrue(unitCell.exists)
        searchTextField.typeTextLoop("o")
        XCTAssertTrue(noneCell.exists)
        XCTAssertTrue(nothingCell.exists)
        XCTAssertFalse(unitCell.exists)
        searchTextField.typeTextLoop("t")
        XCTAssertFalse(noneCell.exists)
        XCTAssertTrue(nothingCell.exists)
        XCTAssertFalse(unitCell.exists)
        searchTextField.clearText("not")
        XCTAssertTrue(noneCell.exists)
        XCTAssertTrue(nothingCell.exists)
        XCTAssertTrue(unitCell.exists)
        
        
    }
    
}
