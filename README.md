# Some Measurements
Tailored for casual users, this intuitive app allows you to seamlessly record and monitor your progress in mere seconds. Whether it's managing weight, height, or any personal metric, stay on top of your goals with the added convenience of customizable tags. 
